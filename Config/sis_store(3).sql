-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 23, 2024 at 11:15 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sis_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm`
--

CREATE TABLE `adm` (
  `id` int NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `auth_token` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `adm`
--

INSERT INTO `adm` (`id`, `username`, `password_hash`, `auth_token`) VALUES
(4, 'sisadmin', '$2y$10$Ie4rCDHsANNtIGfZDd6ZOOLx37Qockx3sf2OWRVg5V7M7z3BA.IiS', '35fbba70e6e9037f4e6a43dba4d29565ad6dd2f2303c87a27eefacc4e5861700'),
(5, 'riniadmin', '$2y$10$nwhVqEsl3jZjCok/yIu3teywoQmmt97o33sBNBAYO8LVXN4BkrGH6', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admcontrol`
--

CREATE TABLE `admcontrol` (
  `secondBanerFirstImg` int DEFAULT NULL,
  `secondBanerSecondImg` int DEFAULT NULL,
  `whiteBanerFirstImg` int DEFAULT NULL,
  `whiteBanerSecondImg` int DEFAULT NULL,
  `cms_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `admcontrol`
--

INSERT INTO `admcontrol` (`secondBanerFirstImg`, `secondBanerSecondImg`, `whiteBanerFirstImg`, `whiteBanerSecondImg`, `cms_id`) VALUES
(55, 17, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int NOT NULL,
  `published_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `main_image` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `content` text COLLATE utf8mb4_general_ci,
  `content1` text COLLATE utf8mb4_general_ci,
  `content2` text COLLATE utf8mb4_general_ci,
  `sub_title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title_content` text COLLATE utf8mb4_general_ci,
  `sub_title_content1` text COLLATE utf8mb4_general_ci,
  `sub_title_content2` text COLLATE utf8mb4_general_ci,
  `image` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title1` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title1_content` text COLLATE utf8mb4_general_ci,
  `sub_title1_content1` text COLLATE utf8mb4_general_ci,
  `sub_title1_content2` text COLLATE utf8mb4_general_ci,
  `sub_title2` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title2_content` text COLLATE utf8mb4_general_ci,
  `sub_title2_content1` text COLLATE utf8mb4_general_ci,
  `sub_title2_content2` text COLLATE utf8mb4_general_ci,
  `sub_title3` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title3_content` text COLLATE utf8mb4_general_ci,
  `sub_title3_content1` text COLLATE utf8mb4_general_ci,
  `sub_title3_content2` text COLLATE utf8mb4_general_ci,
  `sub_title4` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title4_content` text COLLATE utf8mb4_general_ci,
  `sub_title4_content1` text COLLATE utf8mb4_general_ci,
  `sub_title4_content2` text COLLATE utf8mb4_general_ci,
  `sub_title5` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title5_content` text COLLATE utf8mb4_general_ci,
  `sub_title5_content1` text COLLATE utf8mb4_general_ci,
  `sub_title5_content2` text COLLATE utf8mb4_general_ci,
  `sub_title6` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title6_content` text COLLATE utf8mb4_general_ci,
  `sub_title6_content1` text COLLATE utf8mb4_general_ci,
  `sub_title6_content2` text COLLATE utf8mb4_general_ci,
  `sub_title7` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title7_content` text COLLATE utf8mb4_general_ci,
  `sub_title7_content1` text COLLATE utf8mb4_general_ci,
  `sub_title7_content2` text COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `published_at`, `main_image`, `title`, `content`, `content1`, `content2`, `sub_title`, `sub_title_content`, `sub_title_content1`, `sub_title_content2`, `image`, `sub_title1`, `sub_title1_content`, `sub_title1_content1`, `sub_title1_content2`, `sub_title2`, `sub_title2_content`, `sub_title2_content1`, `sub_title2_content2`, `sub_title3`, `sub_title3_content`, `sub_title3_content1`, `sub_title3_content2`, `sub_title4`, `sub_title4_content`, `sub_title4_content1`, `sub_title4_content2`, `sub_title5`, `sub_title5_content`, `sub_title5_content1`, `sub_title5_content2`, `sub_title6`, `sub_title6_content`, `sub_title6_content1`, `sub_title6_content2`, `sub_title7`, `sub_title7_content`, `sub_title7_content1`, `sub_title7_content2`) VALUES
(1, '2023-12-13 10:51:27', 'IMG_0001.jpg', 'Celebrate Your Unique Style with SIS: Where Fashion Meets Empowerment and Lifelong Friendship.', 'Are you in search of a new and stylish clothing brand? Look no further! Welcome to the grand opening of <span class=\"lg:text-2xl rische\">SlS</span>,\n                    founded by two individuals with 25 years of shared experiences. Immerse yourself in a world where fashion\n                    expertise meets makeup artistry, creating a unique blend of style and empowerment. Let\'s explore what sets <span class=\"lg:text-2xl rische\">SlS</span>\n                    apart and why it\'s your new go-to destination for contemporary fashion.', 'Rooted in a lifelong friendship and shared memories, <span class=\"lg:text-2xl rische\">SlS</span> is a manifestation of passion, creativity,\n                    and a deep appreciation for the art of style. Join us as we introduce you to the essence of <span class=\"lg:text-2xl rische\">SlS</span> and the exciting\n                    journey that lies ahead.', NULL, 'A Unique Fusion of Fashion and Friendship <span class=\"stink\">:</span>', 'Two distinct worlds collided 25 years ago to create the universe of SIS. Rezarta and Drita,\n                    originally friends and now considered family, affectionately refer to each other as <span class=\"lg:text-2xl rische\">SlS</span>.\n                    Hailing from the realms of makeup and beauty, fine art, and design, their shared passion for fashion\n                    not only propelled them to excel in their individual domains but also led them to collaboratively\n                    establish their own brand, <span class=\"lg:text-2xl rische\">SlS</span>.', NULL, NULL, 'IMG_0004.jpg', 'Meet the Founders <span class=\"stink\">:</span>', 'Blending their unique talents to create a brand that speaks to the modern, in-the-know individual.', 'One founder, a seasoned fashion expert in the Berlin and London scene, brings an eye for trends and timeless elegance.\n                            The other, a skilled makeup artist, adds a touch of transformative beauty to the mix.\n                            Together, they are excited to present a brand that transcends the boundaries of conventional fashion.', NULL, 'Unveiling the Collections <span class=\"stink\">:</span>', 'With the opening of SIS, we proudly showcase our inaugural collections that capture the spirit of contemporary fashion.\n                    From versatile everyday wear to statement pieces that turn heads, our curated selections are a testament to the founders\'\n                    commitment to offering diverse and distinctive fashion choices. Get ready to discover your new go-to pieces that effortlessly\n                    blend comfort with style.', NULL, NULL, 'The SIS Experience <span class=\"stink\">:</span>', 'More than a clothing store, SIS is a fashion experience. Our focus is on creating evergreen staple pieces qualitatively\n                    that can always style with your old clothes that haven\'t been used for a while, curating timeless styles, and providing\n                    expertise in your purchases. Experience personal styling by our expert Drita, who has extensive fashion experience,\n                    bringing a unique touch to your fashion journey.', 'Explore daily outfits and night looks, including vintage pieces and festival\n                    outfits, and always leave a lasting impression. Immerse yourself in cutting-edge apparel and exquisite jewelry,\n                    transforming your fashion sense and perspective. It\'s not just about what you wear but how you wear it. With us,\n                    you are guided by true trendsetters.', NULL, 'SIS Community', 'More than a clothing store, SIS is a fashion experience. Our focus is on creating evergreen staple pieces qualitatively\n                    that can always style with your old clothes that haven\'t been used for a while, curating timeless styles, and providing\n                    expertise in your purchases. Experience personal styling by our expert Drita, who has extensive fashion experience,\n                    bringing a unique touch to your fashion journey. Explore daily outfits and night looks, including vintage pieces and festival\n                    outfits, and always leave a lasting impression. Immerse yourself in cutting-edge apparel and exquisite jewelry,\n                    transforming your fashion sense and perspective. It\'s not just about what you wear but how you wear it. With us,\n                    you are guided by true trendsetters.', NULL, NULL, 'CONCLUSION', 'As you explore the world of SIS, remember that fashion is a form of self-expression, and everyone has a unique \n                    story to tell. Embrace your individuality, celebrate your journey, and let SIS be your partner in \n                    expressing the beautiful, confident creation that you are. Join us in the pursuit of fashion that empowers \n                    and uplifts - because at SIS, we believe that style is a celebration of you!', 'As SIS opens its doors to the world, we extend a warm welcome to you, our discerning and stylish \n                community. This is just the beginning of an exciting journey, and we invite you to be part of our story. \n                Discover the fusion of fashion, friendship, and individuality at SIS - where every piece tells a story, \n                and everyone is celebrated. Cheers to the start of something beautiful!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'Shirts'),
(2, 'Sweaters'),
(3, 'Hoodies'),
(4, 'T-Shirts'),
(5, 'Jackets'),
(6, 'Blazers'),
(7, 'Pants'),
(8, 'Skirts'),
(9, 'Dresses'),
(10, 'Coats'),
(11, 'Sets');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `answer` text COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'Are you currently offering online shopping through your website?', 'Answer: No, at the moment, we are not processing sales through our website. However, you can explore our collection and find our products available in-store or through our social media in Instagram or Facebook.', '2023-11-16 11:54:01', '2023-11-23 11:09:44'),
(2, 'How can I purchase your products?', 'Answer: You can purchase our products by visiting our physical store or checking with our authorized retailers. We are working to enhance our online shopping experience, and details about online sales will be announced soon.', '2023-11-16 11:54:01', '2023-11-16 11:54:01'),
(3, 'Do you offer international shipping?', 'Answer: Currently, we are focusing on local sales. We do not provide international shipping services. Our primary goal is to ensure a seamless experience for our local customers.', '2023-11-16 11:54:01', '2023-11-16 11:54:01'),
(4, 'What sizes are available for your clothing?', 'Answer: Our clothing comes in a range of sizes to cater to various body types. Please refer to our size chart for accurate measurements and find the perfect fit for you.', '2023-11-16 11:54:01', '2023-11-16 11:54:01'),
(5, 'Can I return or exchange a product?', 'Answer: Yes, we accept returns and exchanges within a specified period. Please review our return policy for detailed information on the process, conditions, and timeframe for returns and exchanges.', '2023-11-16 11:54:01', '2023-11-16 11:54:01'),
(6, 'Are your products sustainable or eco-friendly?', 'Answer: We are committed to sustainability. Many of our products are made using eco-friendly materials, and we continually strive to minimize our environmental impact. Check our product descriptions for details on sustainability features.', '2023-11-16 11:54:01', '2023-11-16 11:54:01'),
(7, 'How can I stay updated on new arrivals and promotions?', 'Answer: Stay in the loop by subscribing to our newsletter! Receive the latest updates on new arrivals, exclusive promotions, and special events. You can also follow us on social media for behind-the-scenes content and sneak peeks.', '2023-11-16 11:54:01', '2023-11-16 11:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `jewelries`
--

CREATE TABLE `jewelries` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int NOT NULL,
  `material` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jewelries`
--

INSERT INTO `jewelries` (`id`, `name`, `description`, `price`, `created_at`, `category_id`, `material`, `color`) VALUES
(2, 'Green & Brown  Gemstones', 'Jewelry ', 30, '2023-12-05 11:55:34', 2, 'Real Gemstones', 'Emerald '),
(3, 'Turquenite Gemstones ', 'Jewelry ', 35, '2023-12-05 12:00:25', 2, 'Real Gemstones', 'Turquoise '),
(4, 'Emerald Real Gemstone ', 'Jewelry ', 30, '2023-12-05 12:03:27', 2, 'Real Gemstones', 'Emerald '),
(5, 'Quartz Gemstones ', 'Jewelry ', 30, '2023-12-05 12:06:19', 2, 'Real Gemstones', 'White '),
(6, 'Amethyst Tumbled Crystals ', 'Jewelry ', 30, '2023-12-05 12:10:34', 2, 'Real Gemstones', 'Purple '),
(7, 'Gorgeus Grey Silver Gemstones ', 'Jewelry ', 30, '2023-12-05 12:14:23', 2, 'Real Gemstones', 'Grey silver '),
(8, 'Onyx Black Gemstones', 'Jewelry ', 30, '2023-12-05 12:19:53', 2, 'Real Gemstones', 'Black'),
(9, 'Tigers Eye Gemstones ', 'Jewelry ', 30, '2023-12-05 12:24:11', 2, 'Real Gemstones', 'Brown '),
(10, 'Black Shiny Gemstones ', 'Jewelry ', 30, '2023-12-05 12:25:13', 2, 'Real Gemstones', 'Black '),
(11, 'Dark Mat Grey Stone ', 'Jewelry ', 30, '2023-12-05 12:27:27', 2, 'Real Gemstones', 'Green Brown Black Dark Grey '),
(12, 'Black & Brown Gemstones ', 'Jewelry ', 30, '2023-12-05 12:28:48', 2, 'Real Gemstones', 'Black Brown ');

-- --------------------------------------------------------

--
-- Table structure for table `jewelry_categories`
--

CREATE TABLE `jewelry_categories` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jewelry_categories`
--

INSERT INTO `jewelry_categories` (`id`, `name`) VALUES
(1, 'Rings'),
(2, 'Necklaces'),
(3, 'Bracelets'),
(4, 'Earings'),
(5, 'Sets');

-- --------------------------------------------------------

--
-- Table structure for table `jewelry_images`
--

CREATE TABLE `jewelry_images` (
  `id` int NOT NULL,
  `jewellry_id` int NOT NULL,
  `main_image` varchar(255) NOT NULL,
  `image1` varchar(255) DEFAULT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `image4` varchar(255) DEFAULT NULL,
  `image5` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jewelry_images`
--

INSERT INTO `jewelry_images` (`id`, `jewellry_id`, `main_image`, `image1`, `image2`, `image3`, `image4`, `image5`) VALUES
(2, 2, 'IMG_4118.jpg', NULL, NULL, NULL, NULL, NULL),
(3, 3, 'IMG_4121.jpg', NULL, NULL, NULL, NULL, NULL),
(4, 4, 'IMG_4127.jpg', NULL, NULL, NULL, NULL, NULL),
(5, 5, 'IMG_4129.jpg', NULL, NULL, NULL, NULL, NULL),
(6, 6, 'IMG_4135.jpg', NULL, NULL, NULL, NULL, NULL),
(7, 7, 'IMG_4161.jpg', NULL, NULL, NULL, NULL, NULL),
(8, 8, 'IMG_4156.jpg', NULL, NULL, NULL, NULL, NULL),
(9, 9, 'IMG_4171.jpg', NULL, NULL, NULL, NULL, NULL),
(10, 10, 'IMG_4170.jpg', NULL, NULL, NULL, NULL, NULL),
(11, 11, 'IMG_4164.JPG', NULL, NULL, NULL, NULL, NULL),
(12, 12, 'IMG_4166.jpg', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `attempt_count` int NOT NULL DEFAULT '0',
  `last_attempt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `username`, `attempt_count`, `last_attempt_time`) VALUES
(1, 'riniMisini', 1, '2023-11-21 17:58:54'),
(2, 'riniMisini', 2, '2023-11-25 12:54:21'),
(3, 'sisadmin', 1, '2023-11-26 12:44:06'),
(4, 'sisadmin', 2, '2023-11-26 12:44:20'),
(5, 'sisadmin', 2, '2023-11-26 12:44:57'),
(6, 'sisadmin', 2, '2023-11-26 12:47:48'),
(7, 'sisadmin', 2, '2023-11-27 09:42:24'),
(8, 'sisadmin', 2, '2023-11-27 09:43:14'),
(9, 'sisadmin', 2, '2023-11-27 09:51:39'),
(10, 'sisadmin', 2, '2023-11-27 09:53:23'),
(11, 'sisadmin', 2, '2023-11-27 09:54:53'),
(12, 'sisadmin', 2, '2023-11-27 09:56:07');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `user_message` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `username`, `user_email`, `user_message`) VALUES
(1, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(2, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(3, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(4, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(5, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(7, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(11, 'sssss', 'ssss@gmail.com', 'Mesazhiiii'),
(12, 'sssss', 'ssss@gmail.com', 'Mesazhiiii'),
(13, 'Rini Misini', 'misinirini@gmail.com', 'Hello sis store'),
(14, 'Rajan', 'rajanmisini1@gmail.com', 'Hello');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`) VALUES
(1, 'misinirini@gmail.com'),
(2, 'misinirini@gmail.com'),
(3, '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `product_description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `in_stock` bit(1) NOT NULL DEFAULT b'1',
  `price` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_description`, `in_stock`, `price`, `created_at`, `category_id`) VALUES
(1, 'Pink Kimono Set', 'Japanese inspired floral shirt and pants', b'1', 80, '2023-11-17 14:59:06', 11),
(4, 'Coral Blue Corduroy Set', 'Beatiful corduroy casual teal blazer and pants', b'1', 160, '2023-11-17 15:01:58', 11),
(6, 'Pastel Pink Dress Skirt', 'Pastel pink colored satin long skirt', b'1', 50, '2023-11-17 15:06:27', 8),
(7, 'Black Sleeveless Blazer Set', 'Black sleeves blazer and pants set ', b'1', 150, '2023-11-17 15:06:27', 11),
(9, 'Basic SIS Tank Top Black', 'Black colored basic tank top', b'1', 20, '2023-11-17 15:09:58', 4),
(10, 'Satin Black Dress Skirt', 'Satin black dress skirt, for when ur feeling fancy', b'1', 50, '2023-11-17 15:09:58', 8),
(12, 'Pastel Blue Satin Dress Skirt', 'Pastel blue satin long dress skirt ', b'1', 50, '2023-11-17 15:09:58', 8),
(13, 'Business Casual Pants', 'Business casual black pants', b'1', 60, '2023-11-17 15:14:45', 7),
(14, 'Black Serious Blazer', 'Black Blazer Serious Style', b'1', 70, '2023-11-17 15:14:45', 6),
(15, 'Jade Green Dress Pants', 'Jade green dress pants ', b'1', 65, '2023-11-17 15:14:45', 7),
(16, 'SIS Quilted Black Jacket', 'Winter jacked with quilted stitching and removable sleeves', b'1', 130, '2023-11-17 15:14:45', 5),
(17, 'Monochrome Patched Sweater', 'Black sweater with patches', b'1', 60, '2023-11-17 15:21:04', 2),
(18, 'Black Cinderella Skirt', 'Black Wide Dress Skirt', b'1', 60, '2023-11-17 15:21:04', 8),
(19, 'Black Patched Bomber Jacket', 'Black Patched Bomber Jacket', b'1', 100, '2023-11-17 15:21:04', 5),
(20, 'Knit Black Long Dress', 'Black long dress, knited stitching', b'1', 50, '2023-11-17 15:21:04', 9),
(21, 'Black Dahlia Kimono Overcoat', 'See through overcoat with flower pattern embroidery ', b'1', 80, '2023-11-17 15:21:04', 10),
(42, 'Oversized Square Blazer', 'Black blazer with oversized look to it', b'1', 70, '2023-11-23 13:50:54', 6),
(45, 'Asymetric Rrez Dress', 'Asymetric Black long dress', b'1', 120, '2023-11-24 14:29:11', 9),
(46, 'SIS Picasso Dress', 'Artistic dress with fabric painted look to elevate your style', b'1', 70, '2023-11-24 15:37:05', 9),
(47, 'SIS Short Sleeved Jacket', 'Short sleeved jacket with a scarf like appearenace, truly unique', b'1', 100, '2023-11-24 15:40:44', 5),
(48, 'Basic SIS Tank Top Gray', 'Steel Gray coloured basic tank top', b'1', 20, '2023-11-24 15:58:39', 4),
(49, 'Chic Sleeveless Vest Set', 'Sleeveless vest with oversized dress pants, hits the spot does it not', b'1', 110, '2023-11-24 21:59:30', 11),
(54, 'Shoulder Pad Top', 't shirt ', b'1', 20, '2023-11-27 17:42:36', 4),
(55, 'Chic pants ', 'Black pants ', b'1', 75, '2023-11-27 20:10:25', 7),
(56, 'Basic SIS Tank Top Gray', 'Steel Gray coloured basic tank top', b'1', 20, '2023-11-27 20:21:26', 4),
(58, 'Funky cotton dress ', 'Cotton long slevless dress', b'1', 50, '2023-11-30 10:59:11', 9);

-- --------------------------------------------------------

--
-- Table structure for table `productsizes`
--

CREATE TABLE `productsizes` (
  `product_id` int NOT NULL,
  `SizeID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `productsizes`
--

INSERT INTO `productsizes` (`product_id`, `SizeID`) VALUES
(1, 2),
(4, 2),
(6, 2),
(7, 2),
(9, 2),
(10, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(42, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(54, 2),
(55, 2),
(56, 2),
(58, 2),
(1, 3),
(4, 3),
(6, 3),
(7, 3),
(9, 3),
(10, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(42, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(54, 3),
(55, 3),
(56, 3),
(58, 3),
(1, 4),
(4, 4),
(6, 4),
(7, 4),
(9, 4),
(10, 4),
(12, 4),
(13, 4),
(14, 4),
(15, 4),
(16, 4),
(17, 4),
(18, 4),
(19, 4),
(20, 4),
(21, 4),
(42, 4),
(45, 4),
(46, 4),
(47, 4),
(48, 4),
(49, 4),
(54, 4),
(55, 4),
(56, 4),
(58, 4),
(1, 5),
(4, 5),
(6, 5),
(7, 5),
(9, 5),
(10, 5),
(12, 5),
(13, 5),
(14, 5),
(15, 5),
(16, 5),
(17, 5),
(18, 5),
(19, 5),
(20, 5),
(21, 5),
(45, 5),
(46, 5),
(47, 5),
(48, 5),
(49, 5),
(55, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `image_id` int NOT NULL,
  `product_id` int NOT NULL,
  `main_image` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `image1` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `image2` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `image3` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `image4` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `image5` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `image6` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `product_id`, `main_image`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`) VALUES
(1, 1, 'sis_secondBanner-2-min.jpg', 'sis_secondBanner-3-min.jpg', 'sis_secondBanner-1-min.jpg', NULL, NULL, NULL, NULL),
(4, 4, 'teal_dress_pants.jpg', 'teal_blazer.jpg', 'tealset.jpg', 'tealset1.jpg', NULL, NULL, NULL),
(6, 6, 'cream_dress_skirt.jpg', 'pinkskrt.jpg', 'pinkskrt2.jpg', NULL, NULL, NULL, NULL),
(7, 7, 'blzSet1.jpg', 'blzSet2.jpg', 'blzSet3.jpg', NULL, NULL, NULL, NULL),
(9, 9, 'black_tanktop.jpg', 'satinBlckDrs1.jpg', NULL, NULL, NULL, NULL, NULL),
(10, 10, 'satin_black_dress.jpg', 'satinBlckDrs.jpg', 'satinBlckDrs1.jpg', NULL, NULL, NULL, NULL),
(12, 12, 'blue_satin_dress.jpg', 'satinBlueDrs1.jpg', 'satinBlueDrs.jpg', NULL, NULL, NULL, NULL),
(13, 13, 'blackPants.jpg', 'blackPants1.jpg', 'grayTnktp.jpg', 'IMG_1872.jpg', 'IMG_1882.jpg', NULL, NULL),
(14, 14, 'seriousBlz.jpg', 'seriousBlz1.jpg', 'black_serious_blazer.jpg', NULL, NULL, NULL, NULL),
(15, 15, 'IMG_1099.jpg', 'jadeGreenPnt1.jpg', 'IMG_0914.jpg', 'IMG_1102.JPG', NULL, NULL, NULL),
(16, 16, 'quilted_jacket.jpg', 'sis-banner-2-min.jpg', 'sis-banner-1-min.jpg', NULL, NULL, NULL, NULL),
(17, 17, 'sis_2img_banner-2-min.jpg', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 18, 'sis_2img_banner-1.jpg', 'cinderellaSkrt1.jpg', 'cinderellaSkrt.jpg', NULL, NULL, NULL, NULL),
(19, 19, 'jaknbomber.jpg', 'jaknbomber2.jpg', 'jaknbomber3.jpg', NULL, NULL, NULL, NULL),
(20, 20, 'knit.jpg', 'knit2.jpg', 'knit3.jpg', 'knit3.jpg', NULL, NULL, NULL),
(21, 21, 'black_overcoat.jpg', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 42, 'Optimized-NUP_9110.jpg', 'NUP_9187 (1).jpg', NULL, NULL, NULL, NULL, NULL),
(44, 45, 'fustan.jpg', 'fustan1.jpg', NULL, NULL, NULL, NULL, NULL),
(45, 46, 'IMG_2652.JPG', 'IMG_2176.jpg', 'IMG_2178 2.jpg', NULL, NULL, NULL, NULL),
(46, 47, 'jacketCut1.jpg', 'jacketCut2.jpg', 'jacketCut.jpg', 'jacketCut3.jpg', 'IMG_1876.jpg', NULL, NULL),
(47, 48, 'grayTnktp.jpg', NULL, NULL, NULL, NULL, NULL, NULL),
(48, 49, 'blackSet2.jpg', 'blackSet1.jpg', 'blackSet.jpg', NULL, NULL, NULL, NULL),
(51, 54, 'IMG_2343.jpg', 'IMG_2337.jpg', NULL, NULL, NULL, NULL, NULL),
(52, 55, 'IMG_0752.JPG', 'IMG_0282.jpg', 'IMG_0751 2.JPG', 'IMG_0750.JPG', NULL, NULL, NULL),
(53, 56, 'IMG_1097.jpg', 'IMG_1094.jpg', NULL, NULL, NULL, NULL, NULL),
(55, 58, 'IMG_0613(1).jpg', 'IMG_0601.jpg', 'IMG_0604.heic', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `SizeID` int NOT NULL,
  `SizeName` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`SizeID`, `SizeName`) VALUES
(6, '2XL'),
(14, '30'),
(15, '31'),
(16, '32'),
(17, '34'),
(18, '36'),
(19, '38'),
(20, '40'),
(4, 'L'),
(3, 'M'),
(2, 'S'),
(5, 'XL'),
(1, 'XS');

-- --------------------------------------------------------

--
-- Table structure for table `vintageproducts`
--

CREATE TABLE `vintageproducts` (
  `vintage_id` int NOT NULL,
  `v_name` varchar(255) NOT NULL,
  `v_description` varchar(255) NOT NULL,
  `v_price` float NOT NULL,
  `in_stock` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vintageproducts`
--

INSERT INTO `vintageproducts` (`vintage_id`, `v_name`, `v_description`, `v_price`, `in_stock`, `created_at`, `category_id`) VALUES
(11, 'Mulberry dress', 'Vintage Dress with scarf like design', 350, '1', '2023-11-29 01:14:47', 9),
(12, 'Vintage BALMAIN dress ', 'Vintage Black dress with floral embroidery patterns', 60, '1', '2023-11-29 01:29:22', 9),
(13, 'Silence Noice ', 'Black Dress', 30, '1', '2023-11-29 11:36:55', 9),
(14, 'Vintage House CB London', 'Pearls Design Dress', 50, '1', '2023-11-29 11:40:02', 9),
(15, 'See Through Top & Skirt ', 'Black see through dress', 100, '1', '2023-11-29 12:00:53', 9),
(16, 'Mulberry Dress', 'Orange Dress', 300, '1', '2023-11-29 12:10:22', 9),
(17, 'Karen Millen Dress', 'Black Dress', 190, '1', '2023-11-29 12:22:49', 9);

-- --------------------------------------------------------

--
-- Table structure for table `vintagesizes`
--

CREATE TABLE `vintagesizes` (
  `vintage_id` int NOT NULL,
  `SizeID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vintagesizes`
--

INSERT INTO `vintagesizes` (`vintage_id`, `SizeID`) VALUES
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 3),
(17, 3),
(11, 4);

-- --------------------------------------------------------

--
-- Table structure for table `vintage_images`
--

CREATE TABLE `vintage_images` (
  `vimage_id` int NOT NULL,
  `vintage_id` int NOT NULL,
  `vmain_image` varchar(255) NOT NULL,
  `vimage1` varchar(255) DEFAULT NULL,
  `vimage2` varchar(255) DEFAULT NULL,
  `vimage3` varchar(255) DEFAULT NULL,
  `vimage4` varchar(255) DEFAULT NULL,
  `vimage5` varchar(255) DEFAULT NULL,
  `vimage6` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vintage_images`
--

INSERT INTO `vintage_images` (`vimage_id`, `vintage_id`, `vmain_image`, `vimage1`, `vimage2`, `vimage3`, `vimage4`, `vimage5`, `vimage6`) VALUES
(10, 11, 'tangerineDress.jpg', 'tangerineDress2.jpg', NULL, NULL, NULL, NULL, NULL),
(11, 12, 'blackFloralDress2.jpg', 'blackFloralDress1.jpg', 'blackFloralDress2.jpg', NULL, NULL, NULL, NULL),
(12, 13, 'stylishDress.jpg', 'stylishDress1.jpg', NULL, NULL, NULL, NULL, NULL),
(13, 14, 'PearlDress2.jpg', 'PearlDress1.jpg', 'PearlDress.jpg', NULL, NULL, NULL, NULL),
(14, 15, 'seeThroughDress.jpg', 'seeThroughDress1.jpg', 'seeThroughDress2.jpg', NULL, NULL, NULL, NULL),
(15, 16, 'tangerineDress3.jpg', 'tangerineDress4.jpg', NULL, NULL, NULL, NULL, NULL),
(16, 17, 'blackvintageDress1.jpg', 'blackvintageDress.jpg', 'blackvintageDress2.jpg', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adm`
--
ALTER TABLE `adm`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `auth_token` (`auth_token`);

--
-- Indexes for table `admcontrol`
--
ALTER TABLE `admcontrol`
  ADD UNIQUE KEY `cms_id` (`cms_id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `jewelries`
--
ALTER TABLE `jewelries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_jewelries_category` (`category_id`);

--
-- Indexes for table `jewelry_categories`
--
ALTER TABLE `jewelry_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jewelry_images`
--
ALTER TABLE `jewelry_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_jewelry_images_jewelries` (`jewellry_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `fk_category` (`category_id`);

--
-- Indexes for table `productsizes`
--
ALTER TABLE `productsizes`
  ADD PRIMARY KEY (`product_id`,`SizeID`),
  ADD KEY `SizeID` (`SizeID`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `fk_product_images_product_id` (`product_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`SizeID`),
  ADD UNIQUE KEY `SizeName` (`SizeName`);

--
-- Indexes for table `vintageproducts`
--
ALTER TABLE `vintageproducts`
  ADD PRIMARY KEY (`vintage_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `vintagesizes`
--
ALTER TABLE `vintagesizes`
  ADD PRIMARY KEY (`vintage_id`,`SizeID`),
  ADD KEY `SizeID` (`SizeID`);

--
-- Indexes for table `vintage_images`
--
ALTER TABLE `vintage_images`
  ADD PRIMARY KEY (`vimage_id`),
  ADD KEY `vintage_images_id_ibfk_1` (`vintage_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adm`
--
ALTER TABLE `adm`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jewelries`
--
ALTER TABLE `jewelries`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `jewelry_categories`
--
ALTER TABLE `jewelry_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jewelry_images`
--
ALTER TABLE `jewelry_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `image_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `SizeID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `vintageproducts`
--
ALTER TABLE `vintageproducts`
  MODIFY `vintage_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `vintage_images`
--
ALTER TABLE `vintage_images`
  MODIFY `vimage_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jewelries`
--
ALTER TABLE `jewelries`
  ADD CONSTRAINT `fk_jewelries_category` FOREIGN KEY (`category_id`) REFERENCES `jewelry_categories` (`id`);

--
-- Constraints for table `jewelry_images`
--
ALTER TABLE `jewelry_images`
  ADD CONSTRAINT `fk_jewelry_images_jewelries` FOREIGN KEY (`jewellry_id`) REFERENCES `jewelries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`);

--
-- Constraints for table `productsizes`
--
ALTER TABLE `productsizes`
  ADD CONSTRAINT `productsizes_ibfk_2` FOREIGN KEY (`SizeID`) REFERENCES `sizes` (`SizeID`),
  ADD CONSTRAINT `productsizes_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `fk_product_images_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `vintageproducts`
--
ALTER TABLE `vintageproducts`
  ADD CONSTRAINT `vintageproducts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`);

--
-- Constraints for table `vintagesizes`
--
ALTER TABLE `vintagesizes`
  ADD CONSTRAINT `vintagesizes_ibfk_1` FOREIGN KEY (`vintage_id`) REFERENCES `vintageproducts` (`vintage_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vintagesizes_ibfk_2` FOREIGN KEY (`SizeID`) REFERENCES `sizes` (`SizeID`);

--
-- Constraints for table `vintage_images`
--
ALTER TABLE `vintage_images`
  ADD CONSTRAINT `vintage_images_id_ibfk_1` FOREIGN KEY (`vintage_id`) REFERENCES `vintageproducts` (`vintage_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

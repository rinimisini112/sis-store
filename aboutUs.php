<?php
include 'public/Components/htmlHead.php';
?>
<body class="bg-sis-white">
    <header class="">
        <nav id="navbar" class="bg-sis-white duration-200 z-40 fixed top-0 left-0 text-sis-grey  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="index.php" class=" relative z-[100]">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <?php 
            include 'public/Components/phoneMenu.php';
            ?>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
            <li  class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                        </p>
                    <div class="absolute shadow-lg left-0 w-full bg-sis-white" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="blog.php" class="relative">Blog
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <main class="w-[95%] md:w-[90%] lg:w-[80%] h-auto mx-auto flex items-center md:justify-center justify-end">
        <div id="firstGalleryParent" class="w-full flex md:flex-row flex-col items-center md:justify-evenly mt-[3rem] md:mt-20 lg:mt-[10rem] relative">
        <h2 id="whoAreWeTitle" class="rische relative md:absolute top-0 md:py-0 py-16 text-center text-6xl md:text-8xl z-30 text-sis-grey">Who we are</h2>    
        <div class=" w-[90%] md:w-2/5 flex flex-col items-start">
                <img src="resources/images/business_casual_pants.jpg" alt="Image of our founder" class=" w-full md:w-[90%]">
                <h3 class="font-semibold angle-font md:text-left text-center text-4xl pt-4">Drita Avdiu</h3>
                <p>Founder of SIS Store</p>
            </div>
            <div class=" w-[90%] md:w-2/5 md:mt-32 mt-16 flex flex-col md:justify-end justify-start items-start">
                <p class="block md:hidden text-5xl font-semibold rische py-3">We are </p>
                <p class="pr-0 md:pr-8 md:px-0 px-4 text-xl pb-12"><span class="md:inline hidden">We're</span> more than a clothing store; we're a fashion experience. Our focus extends beyond trends, curating timeless styles and providing expertise in your purchases</p>
                <img src="resources/images/NUP_8418-min.jpg" alt="Image of our founder" class="w-full md:w-[90%]">
                <h3 class="angle-font md:text-left text-center text-4xl pt-4 font-semibold">Rezarta Galica</h3>
                <p>Founder of SIS Store</p>
            </div>
        </div>
    </main>

    <article id="aboutUsArticle" class="w-[95%] md:w-[90%] h-auto lg:w-[70%] mx-auto">
    <div class="w-full flex md:flex-row flex-col-reverse items-center justify-evenly mt-[10rem] md:mt-20 lg:mt-[10rem] relative">
        <p class="rische absolute top-0 text-right ml-0 md:ml-44 text-6xl md:text-8xl text-sis-grey">About Us</p>    
        <div class=" w-[90%] md:w-2/5 flex flex-col md:-mt-0 -mt-16 items-center justify-center">
                <p class="font-semibold angle-font text-4xl md:text-4xl lg:text-5xl mt-32">From Us to You</p>
                <p class="text-2xl leading-relaxed pt-4">Experience personal styling by our expert, Drita, who has extended styling background both in Berlin and London, bringing a unique touch to your fashion journey. Explore daily outfits and night looks, including vintage pieces and festival outfits, and always leave a lasting impression.</p>
            </div>
            <div class=" w-[90%] md:w-2/5 mt-24 flex flex-col justify-end">
                <img src="resources/images/sis_about_us_sideImg-min.jpg" alt="Co-founder in a tuxedo" class="w-full md:w-[90%] h-[400px] object-cover">
            </div>
        </div>
    </article>
    <article id="aboutUsArticleExtend" class="w-[95%] md:w-[90%] lg:w-[85%] h-auto mx-auto pb-40">
        
    <div class="w-full flex justify-evenly md:flex-row flex-col mt-[5rem] md:mt-24 lg:mt-[10rem] relative">
        <div class=" w-[95%] md:w-3/5 mt-16 h-full flex md:flex-col flex-row justify-end md:justify-center lg:justify-normal">
        <img src="resources/images/sis_about_us_thirdImg-min.jpg" alt="Girl drinking coffee in a cafe" class=" w-[60%] md:w-[90%] md:h-[500px] md:mt-12 lg:mt-0 lg:h-[400px] h-300px object-contain">
        <h3 class="font-semibold -ml-8 angle-font text-3xl md:text-4xl md:hidden block lg:text-5xl mt-32">Immerse yourself in cutting-edge apparel and exquisite jewelry, </h3>
            </div>
        <div class=" w-[90%] md:w-2/5 flex flex-col items-center  md:justify-center justify-end ">
                <h3 class="font-semibold -ml-36 angle-font text-3xl md:text-4xl z-20 relative md:block hidden lg:text-5xl mt-32">Immerse yourself in cutting-edge apparel and exquisite jewelry, </h3>
                <p class="text-2xl md:-ml-0  leading-relaxed pt-4 md:pr-12 pr-0 md:pl-0 pl-4">transforming your fashion sense and perspective. It's not just about what you wear but how you wear it—with us, you're guided by true trendsetters.</p>
            </div>
        </div>
    </article>
    <?php 
    include "public/Classes/Dbh.php";
    include "public/Components/footer.php";
    ?>
    <script src="dist/javascript/aboutUsAnimations.js"></script>
</body>

</html>
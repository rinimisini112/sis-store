<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="dist/output.css">
    <link rel="stylesheet" href="dist/customs.css">
    <script src="dist/javascript/jquery.js"></script>
    <script src="dist/javascript/minified/gsap.min.js"></script>
    <script src="dist/javascript/minified/ScrollTrigger.min.js"></script>
    <script src="dist/javascript/jquery.validate.js"></script>
    <script src="dist/javascript/jquery.validate.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="resources/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/favicon/favicon-16x16.png">
    <link rel="manifest" href="../resources/favicon/site.webmanifest">
    <title>SIS store</title>
    <style>
           .error {
    color: red!important;
    padding-top: 5px!important;
    margin-top: 0!important;
    outline-color: red!important;
}
    .error:focus {
    color: red;
    border-color: red!important;
    outline-color: red!important;

    }
    .error_effect {
        border-color: red!important;
        outline-color: red!important;

    }
    </style>
</head>
<body class="w-full h-[90vh] bg-sis-white overflow-hidden">
<nav id="navbar" class=" duration-200 z-30 text-sis-grey  flex md:px-16 px-8 lg:py-2 py-4 justify-between items-center bg-opacity-10 w-full">
    <a href="index.php" class=" relative z-[100]">
        <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
    </a>
   <a href="index.php" class=" border-sis-grey border text-sis-grey lg:px-16 px-8 py-2 text-xl font-bold hover:bg-sis-grey hover:text-sis-white duration-200">Back</a>
</nav>
<main class="w-full h-full flex items-center justify-center">
    <div class="lg:w-2/5 w-[90%] lg:mx-0 mx-auto h-auto shadow-sm border-sis-grey border flex items-center justify-center">
    <form id="login" action="sstore/auth/verify.php" method="post" class=" text-sis-grey flex flex-col w-full h-full pb-12">
        <legend class="text-center text-4xl py-3 font-bold border-b-2 border-sis-grey bg-sis-grey text-sis-white angle-font">Login</legend>
        <?php 
        if(isset($_SESSION['mssg'])) {
            echo "<div class=' text-center text-red-500 pt-4 text-xl font-semibold'>{$_SESSION['mssg']}</div>";
            unset($_SESSION['mssg']);
        }
        ?>
        <div class="w-[90%] h-full mx-auto px-4 flex flex-col items-center">
            <div class="flex flex-col items-center w-full">
            <label for="username" class="text-2xl font-bold py-1 mt-4">Username :</label>
            <input type="text" name="authUsername" id="authUsername" class="w-4/5 py-2 duration-200 outline-none pl-2 text-xl bg-sis-white border-b-2 border-[#777777]">
            </div>
            <div class="flex flex-col items-center w-full">
            <label for="password" class="text-2xl font-bold py-1 mt-4">Password :</label>
            <input type="password" name="password" id="authPassword" class="w-4/5 py-2 bg-sis-white duration-200 outline-none pl-2 text-xl border-b-2 border-[#777777]"> 
            </div>
            <input type="submit" value="Continue" name="login" class="lg:w-1/2 w-3/5 mx-auto py-2 border border-sis-grey text-xl font-extrabold my-8 cursor-pointer hover:bg-sis-grey hover:text-sis-white duration-200">
            </div>
        </form>
    </div>
    </main>
    <script src="dist/javascript/admVal.js"></script>
</body>
</html>
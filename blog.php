<?php
session_start();
include 'public/Components/htmlHead.php';
include 'public/Classes/Post.php';

$post = new Post();
$posts = $post->fetchAllPosts();
?>

<body class="bg-sis-white">
    <nav id="navbar" class=" duration-200 z-40 fixed top-0 left-0 text-sis-white  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
        <a href="index.php" class=" relative z-[100]">
            <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
        </a>
        <div class='block lg:hidden' id='openPhoneMenuIcon'>
            <button class='relative group z-50'>
                <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                    <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                    <div class='burger_line bg-sis-grey h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                    <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                </div>
            </button>
        </div>
        <?php
        include "public/Components/phoneMenu.php";

        ?>
        <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
            <li class="px-8 w-[30%] relative group">
                <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                        <polyline points="6 9 12 15 18 9"></polyline>
                    </svg>
                </p>
                <div class="absolute shadow-lg left-0 w-full" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                    <ul class="w-full">
                        <a href="collections.php">
                            <li class="hover:backdrop-blur-md backdrop-blur-sm filter duration-200 py-4 pl-4 border-b border-b-sis-grey">New Collections</li>
                        </a>
                        <a href="vintageCollection.php">
                            <li class="hover:backdrop-blur-md backdrop-blur-sm filter duration-200 py-4 pl-4 border-b border-b-sis-grey">Vintage Collection</li>
                        </a>
                        <a href="jewelries.php">
                            <li class="hover:backdrop-blur-md backdrop-blur-sm filter duration-200 py-4 pl-4">Jewelry</li>
                        </a>
                    </ul>
                </div>
            </li>
            <li class="pl-8 w-[30%] relative group">
                <a href="aboutUs.php" class="relative">About Us
                    <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                </a>
            </li>
            <li class="pl-8 w-[30%] relative group">
                <a href="blog.php" class="relative">Blog
                    <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                </a>
            </li>
            <li class="pl-8 w-[30%] relative group">
                <a href="contactUs.php" class="relative">Contact Us
                    <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                </a>
            </li>
        </ul>
    </nav>
    <header class="" id="blogHeader">
        <div class="relative w-full lg:h-[500px] h-[450px] flex items-center justify-center drop-shadow-lg" id="paralaxScenery">
            <h1 class="rische lg:text-9xl text-5xl relative opacity-90 text-sis-white font-extrabold z-30">SlS <span>st<span class="rische lg:text-[6.8rem] text-[2.6rem]">o</span>re</span></h1>
        </div>

    </header>
    <main class="w-[90%] mx-auto  pt-20 pb-28 flex items-center justify-center" id="mainContainer">
        <div>
            <h2 class="tempest underline text-4xl">Welcome to our story</h2>
            <h1 class="rische text-7xl pt-4">SlS Blog</h1>
        </div>
    </main>
    <?php
    $counter = 0;
    foreach ($posts as $p) :
        $timestamp = strtotime($p->published_at);

        // Get month and year separately using DateTime
        $month = date('F', $timestamp); 
        $month = strtoupper($month);
        $year = date('Y', $timestamp);

        $truncatedContent = (strlen($p->content) > 280) ? substr($p->content, 0, 280) . '...' : $p->content;
        $counter++;

        if ($counter % 2 === 1) :
    ?>
            <section class="lg:w-[85%] md:w-[75%] w-[95%] mx-auto mb-16 lg:pb-0 pb-6 flex lg:flex-row flex-col bg-[#f2ede6] shadow-lg">
                <div class="flex flex-col lg:w-3/5 w-full">
                    <div class="w-[90%] shadow-xl shadow-[#00000077] lg:h-[400px] h-[250px] mx-auto object-cover transform -translate-y-8">
                        <img src="resources\images/<?= $p->main_image ?>" alt="" class="w-full h-full object-cover">
                    </div>
                    <div class="flex w-[90%] mx-auto lg:pt-4 justify-between items-center">
                        <p class="stink lg:text-5xl text-3xl text-neutral-400"><?=$month ?> <span style="text-shadow: 1px 1px 1px gray;" class="rische font-extrabold"><?=$year ?></span></p>
                        <div class="flex">
                            <a class="group" href="https://www.instagram.com/sis.store.pr/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="grey" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram group-hover:fill-sis-white group-hover:stroke-sis-grey duration-300">
                                    <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                                    <path class="group-hover:fill-sis-grey duration-200" d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                                    <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                                </svg>
                            </a>
                            <a class="" href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="grey" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook hover:fill-sis-white hover:stroke-sis-grey duration-300">
                                    <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class=" lg:w-2/5 w-full px-4 md:px-8  lg:pr-6 lg:pt-6 pt-4">
                    <h1 class=" lg:text-3xl text-2xl pb-3 font-bold border-b border-b-sis-grey tempest"><?= $p->title ?></h1>
                    <p class="lg:text-xl text-md lg:pt-4 py-4 lg:pr-4 pr-0 libre"><?= $truncatedContent; ?></p>
                    <a href="post.php?nr=<?= $p->id; ?>" class="border group border-sis-grey px-8 py-2 lg:float-right shadow-xl hover:bg-sis-grey hover:bg-opacity-20 font-bold libre duration-300 my-4">View more</a>

                </div>
            </section>
        <?php else : ?>
            <section class="lg:w-[85%] md:w-[75%] w-[95%] mx-auto mb-16  lg:flex-row flex-col-reverse mt-32 flex bg-[#f2ede6] shadow-lg">
                <div class=" lg:w-2/5 lg:pl-6 px-4  md:px-8 pt-6 lg:flex flex-col justify-between">
                    <h1 class=" lg:text-3xl text-2xl pb-3 font-bold border-b border-b-sis-grey tempest"><?= $p->title ?></h1>
                    <p class="lg:text-xl text-lg: lg:pt-4 py-4 lg:pr-4 pr-2 libre"><?= $truncatedContent; ?></p>
                    <a href="post.php?nr=<?= $p->id; ?>" class="border inline-block lg:w-2/5 border-sis-grey px-8 py-2 shadow-xl hover:bg-sis-grey hover:bg-opacity-20 font-bold libre duration-300 mb-2">View more</a>

                </div>
                <div class="flex flex-col lg:w-3/5 w-full">
                    <div class="w-[90%] shadow-xl shadow-[#00000077] lg:h-[400px] h-[250px] mx-auto object-cover transform -translate-y-8">
                        <img src="resources\images/<?= $p->main_image; ?>" alt="" class="w-full h-full object-cover">
                    </div>
                    <div class="flex w-[90%] mx-auto lg:pb-2 justify-between items-center">
                        <p class="stink lg:text-5xl text-3xl text-neutral-400">=<?=$month ?><span style="text-shadow: 1px 1px 1px gray;" class="rische font-extrabold"><?=$year ?></span></p>
                        <div class="flex">
                            <a class="group" href="https://www.instagram.com/sis.store.pr/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="grey" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram group-hover:fill-sis-white group-hover:stroke-sis-grey duration-300">
                                    <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                                    <path class="group-hover:fill-sis-grey duration-200" d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                                    <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                                </svg>
                            </a>
                            <a class="" href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="grey" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook hover:fill-sis-white hover:stroke-sis-grey duration-300">
                                    <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
    <?php
        endif;
    endforeach;
    include 'public/Components/footer.php' ?>
    <script src="dist/javascript/blog.js"></script>
</body>

</html>
<?php
include "public/collections/c_components/head.php";
include 'public/controllers/filteredProducts.php';
include 'dist/pagination/Zebra_Pagination.php';
$pagination = new Zebra_Pagination();
if (isset($_SESSION['filteredCatItems'])) {
    $items = $productsByCategory->sortFilteredCategories($_SESSION['filteredCatItems'], $cName, $sort);
}


$totalRecords = count($items);
$pagination->records($totalRecords);
// Set records per page
$recordsPerPage = 8;  // You can change this value to adjust the number of products per page
$pagination->records_per_page($recordsPerPage);

// Calculate start index for data
$startIndex = ($pagination->get_page() - 1) * $recordsPerPage;
$itemsSubset = array_slice($items, $startIndex, $recordsPerPage);
?>

<body class="bg-sis-white relative">
    <header>
        <nav id="navbar" class="bg-sis-white bg-opacity-60 duration-200 z-30 text-sis-grey  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="../../index.php" class=" relative z-50">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <div class="fixed top-0 left-0 w-[80%] max-w-0 h-full flex-col flex justify-between bg-sis-white text-sis-grey z-40 transition-all duration-100 overflow-hidden pt-16" id="phoneMenu">
                <ul class=" text-3xl pb-6 mt-4 transition-colors duration-300">
                    <li id="collectionsLi" class="py-4 pl-3 border-b-2 border-t-2 border-sis-grey relative"><span class="flex items-center gap-8">Collections
                            <svg id='phoneCollectionsIcon' xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                                <polyline points="6 9 12 15 18 9"></polyline>
                            </svg>
                        </span>
                        <div id="collectionsPhoneDropdown" style="max-height: 0; overflow: hidden; transition: max-height 0.3s ease-out;">
                            <ul class="w-[95%] pt-4">
                                <li class="py-3 pl-3 border-b border-b-sis-grey"><a href="collections.php">New Collections</a></li>
                                <li class="py-3 pl-3"><a href="vintageCollection.php">Vintage Collection</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-grey"><a href="aboutUs.php">About Us</a></li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-grey"><a href="blog.php">Blog</a></li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-grey"><a href="contactUs.php">Contact Us</a></li>
                </ul>
                <ul class=" p-2 md:text-2xl text-xl text-center w-full">
                    <li class="flex flex-col gap-2 items-center justify-between border-b-2 border-sis-grey pb-2">
                        <span class="text-2xl">
                            Mobile
                        </span> <a href="tel:+38343555450">043 555 450
                        </a>
                    </li>
                    <li class="flex flex-col gap-3 mt-2 items-center justify-between">
                        <span class="text-2xl">
                            Whatsapp </span> <a href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
                <li class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </p>
                    <div class="absolute shadow-lg left-0 w-full z-50 bg-sis-white" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="blog.php" class="relative">Blog
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
            </ul>
        </nav>
        <div id="filler" class="duration-200"></div>
    </header>
    <a id="goUpBtn" href="#navbar" class="duration-300">
        <div class="fixed bottom-6 left-6 md:w-16 w-14 h-14 md:h-16 lg:animate-bounce bg-opacity-80  text-xl font-bold bg-vintage-black hover:bg-opacity-100 duration-200 text-vintage-white rounded-full z-40 flex flex-col items-center justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-up">
                <polyline points="17 11 12 6 7 11"></polyline>
                <polyline points="17 18 12 13 7 18"></polyline>
            </svg>
            <p>UP</p>
        </div>
    </a>
    <main class="w-full">
        <div class="py-6 w-[90%] mx-auto relative">
            <h1 class="text-5xl flex justify-between md:text-left text-center"><span class="rische md:inline hidden">Collections</span> <span class="rische">View our <a href="../../vintageCollection.php" class="rische pl-4 underline decoration-sis-grey underline-offset-4">Vintage Collection</a></span></h1>
            <p class="pt-6 text-center md:hidden block text-3xl rische">SIS Main Collection</p>
            <p class="text-2xl text-center md:pt-8 pt-2 pb-4">Your daily dose of everyday essentials</p>
        </div>
        <section>
            <div id="productNavMb" class="w-full md:px-4 px-2 py-4 flex items-center justify-between text-xl sticky top-0 bg-sis-white z-10 left-0">
                <div class="flex">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-down-left cursor-pointer pt-1">
                        <polyline points="9 10 4 15 9 20"></polyline>
                        <path d="M20 4v7a4 4 0 0 1-4 4H4"></path>
                    </svg>
                    <p class="pr-2 pl-1 lg:text-xl text-lg"><a href='collections.php'>Back</a></p> / <a class="px-2 font-bold" href="collections.php">All</a> / <p class="pl-2 cursor-pointer font-bold"><?= $categoryInfo['category_name'] ?></p>
                </div>
                <p class="hidden lg:flex items-center gap-2 cursor-pointer" onclick="toggleFilterLg()">
                    <svg id="lgFilterIcon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders cursor-pointer">
                        <line x1="4" y1="21" x2="4" y2="14"></line>
                        <line x1="4" y1="10" x2="4" y2="3"></line>
                        <line x1="12" y1="21" x2="12" y2="12"></line>
                        <line x1="12" y1="8" x2="12" y2="3"></line>
                        <line x1="20" y1="21" x2="20" y2="16"></line>
                        <line x1="20" y1="12" x2="20" y2="3"></line>
                        <line x1="1" y1="14" x2="7" y2="14"></line>
                        <line x1="9" y1="8" x2="15" y2="8"></line>
                        <line x1="17" y1="16" x2="23" y2="16"></line>
                    </svg>
                    Filter
                </p>
                <div id="filterMenu" class=" absolute top-14 left-0 z-20 w-full bg-sis-white">
                    <div class="grid justify-items-center overflow-hidden pt-2 w-full">
                        <form action="" method="post" class=" w-full flex justify-between items-center pb-16 px-16">
                            <div class="flex w-auto items-center h-auto justify-center gap-3">
                                <p class="text-xl pt-1">Price </p>
                                <div class="py-1 relative min-w-full">
                                    <input type="range" id="priceInput" name="price" style="accent-color: #000000;" class="w-full h-1" min="0" max="200" step="3" value="<?php echo isset($_SESSION['categoriesPriceRange']) ? $_SESSION['categoriesPriceRange'] : 100; ?>" oninput="updatePriceRange(this.value)">
                                    <div id="tooltipLeft" class="absolute text-gray-800 -ml-1 bottom-0 left-0 -mb-6">$ 0</div>
                                    <div id="tooltipRight" class="absolute text-gray-800 -mr-1 bottom-0 right-0 -mb-6">$ 100</div>
                                </div>
                            </div>
                            <div class="pt-2">
                                <p class="pt-6 text-center">Sizes :</p>
                                <?php
                                include "public/Classes/ProductHandler.php";
                                $PrHandler = new ProductHandler(new Dbh);
                                $allSizes = $PrHandler->fetchAllSizesForCategories();

                                // Separate number sizes and letter sizes
                                $numberSizes = [];
                                $letterSizes = [];

                                foreach ($allSizes as $size) {
                                    if (ctype_digit(substr($size['SizeName'], 1, 1))) {
                                        // Number sizes
                                        $numberSizes[] = $size;
                                    } else {
                                        // Letter sizes
                                        $letterSizes[] = $size;
                                    }
                                }

                                // Function to generate checkboxes for a given size group
                                function generateCheckboxes($sizes, $sessionData)
                                {
                                    foreach ($sizes as $size) {
                                        $checkboxName = 'size' . $size['SizeID'];
                                        $checked = isset($sessionData[$checkboxName]) ? 'checked' : '';
                                ?>
                                        <label for="<?= $checkboxName ?>"><?= $size['SizeName'] ?></label>
                                        <input type="checkbox" name="<?= $checkboxName ?>" class="pr-3" id="<?= $checkboxName ?>" style="accent-color: #000000;" value="<?= $size['SizeID'] ?>" <?= $checked ?>>
                                <?php
                                    }
                                }

                                // Generate checkboxes for number sizes
                                ?>
                                <div class="pt-2">
                                    <?php generateCheckboxes($numberSizes, isset($_SESSION['categoriesSelectedSizes']) ? $_SESSION['categoriesSelectedSizes'] : ''); ?>
                                </div>
                                <?php

                                // Generate checkboxes for letter sizes
                                ?>
                                <div class="pt-2">
                                    <?php generateCheckboxes($letterSizes, isset($_SESSION['categoriesSelectedSizes']) ? $_SESSION['categoriesSelectedSizes'] : ''); ?>
                                </div>
                            </div>
                            <input type="hidden" name="cg" value="<?= isset($cName) ? $cName : '' ?>">
                            <input type="hidden" name="sort" value="<?= isset($_GET['sort']) ? $_GET['sort'] : '' ?>">
                            <div class="flex flex-col">
                                <input type="submit" value="Remove Filters" name="removefilter" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey px-8 py-1.5 mt-4">
                                <input type="submit" value="Filter" name="filterClothes" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey px-8 py-1.5 mt-4">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="relative w-1/5">
                    <p class="hidden lg:block text-right cursor-pointer font-normal duration-200" id="sortDropdownTrigger">Sort
                        <b class="px-2">
                            [<span><?= $sortMessage ?></span>]
                        </b>
                    <div id="sortDropdown" class=" absolute w-full z-50 left-0 text-right bg-sis-white shadow-sm shadow-sis-grey">
                    <a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2 border-b border-b-sis-grey" href="?<?= isset($_GET['price']) ? 'price=' . $_GET['price'] . '&' : '' ?><?= isset($_GET['size']) ? http_build_query(array('size' => $_GET['size'])) . '&' : '' ?>cg=<?= $cName ?>&sort=price_desc">Sort by Price (High to Low)</a>

<a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2 border-b border-b-sis-grey" href="?<?= isset($_GET['price']) ? 'price=' . $_GET['price'] . '&' : '' ?><?= isset($_GET['size']) ? http_build_query(array('size' => $_GET['size'])) . '&' : '' ?>cg=<?= $cName ?>&sort=price_asc">Sort by Price (Low to High)</a>

<a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2 border-b border-b-sis-grey" href="?<?= isset($_GET['price']) ? 'price=' . $_GET['price'] . '&' : '' ?><?= isset($_GET['size']) ? http_build_query(array('size' => $_GET['size'])) . '&' : '' ?>cg=<?= $cName ?>&sort=newest">Sort by Newest</a>

<a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2" href="?<?= isset($_GET['price']) ? 'price=' . $_GET['price'] . '&' : '' ?><?= isset($_GET['size']) ? http_build_query(array('size' => $_GET['size'])) . '&' : '' ?>cg=<?= $cName ?>&sort=oldest">Sort by Oldest</a> </div>
                    </p>
                </div>
                <svg id="togglePhoneFilter" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders block lg:hidden cursor-pointer">
                    <line x1="4" y1="21" x2="4" y2="14"></line>
                    <line x1="4" y1="10" x2="4" y2="3"></line>
                    <line x1="12" y1="21" x2="12" y2="12"></line>
                    <line x1="12" y1="8" x2="12" y2="3"></line>
                    <line x1="20" y1="21" x2="20" y2="16"></line>
                    <line x1="20" y1="12" x2="20" y2="3"></line>
                    <line x1="1" y1="14" x2="7" y2="14"></line>
                    <line x1="9" y1="8" x2="15" y2="8"></line>
                    <line x1="17" y1="16" x2="23" y2="16"></line>
                </svg>
            </div>
            <div class="flex w-full">
                <div class="lg:w-[75%] w-full lg:pl-8 pl-0 h-auto mx-auto flex  flex-wrap relative">
                    <?php
                    include 'public/controllers/filterCategories.php';
                    echo $pagination->render();
                    ?>
                </div>
                <div class="lg:w-[25%] h-auto w-0 lg:overflow-visible overflow-hidden lg:z-0 z-50 bg-sis-white ">
                    <ul id="categoryList" class="lg:sticky bg-sis-white lg:h-auto h-screen lg:z-0 z-50 lg:top-20 text-center lg:w-full w-0 py-4 lg:overflow-y-scroll duration-300 fixed top-0 left-0 overflow-hidden">
                        <h1 class="text-3xl pb-6 angle-font font-semibold hidden lg:block">Categories</h1>
                        <h1 id='categoriesToggle' class="text-3xl pb-4 font-bold lg:hidden block">Categories</h1>
                        <div id='categoriesPhoneMenu' class="lg:block grid-cols-3 align-items-center lg:pb-0 pb-4 px-2">
                            <li class='py-1.5 lg:text-xl text-md font-bold text-sis-grey'>
                                <a href='../../collections.php' class='relative group rische'>
                                    View All <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-grey'></span>
                                </a>
                                <?php
                                include 'public/controllers/clickedCategory.php';
                                ?>
                        </div>
                        <div class=" grid h-auto justify-items-center pt-6 pb-4 lg:border-none border-t-2 border-b-2 border-sis-grey">
                            <p class="lg:hidden flex items-center gap-2 text-3xl font-bold filter-toggle cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders cursor-pointer">
                                    <line x1="4" y1="21" x2="4" y2="14"></line>
                                    <line x1="4" y1="10" x2="4" y2="3"></line>
                                    <line x1="12" y1="21" x2="12" y2="12"></line>
                                    <line x1="12" y1="8" x2="12" y2="3"></line>
                                    <line x1="20" y1="21" x2="20" y2="16"></line>
                                    <line x1="20" y1="12" x2="20" y2="3"></line>
                                    <line x1="1" y1="14" x2="7" y2="14"></line>
                                    <line x1="9" y1="8" x2="15" y2="8"></line>
                                    <line x1="17" y1="16" x2="23" y2="16"></line>
                                </svg>
                                Filter
                            </p>
                            <div class="grid justify-items-center overflow-hidden pt-2 w-full px-2 filter-content">
                                <form action="" method="post" class=" w-full px-4">
                                    <p class="text-xl pt-1">Price :</p>
                                    <div class="flex w-full m-auto items-center h-auto justify-center">
                                        <div class="py-1 relative min-w-full">
                                            <input type="range" id="priceInput" name="price" style="accent-color: #000000;" class="w-full h-1" min="0" max="200" step="3" value="<?php echo isset($_SESSION['categoriesPriceRange']) ? $_SESSION['categoriesPriceRange'] : 100; ?>" oninput="updatePriceRange(this.value)">
                                            <div id="tooltipLeft" class="absolute text-gray-800 -ml-1 bottom-0 left-0 -mb-6">$ 0</div>
                                            <div id="phoneTooltipRight" class="absolute text-gray-800 -mr-1 bottom-0 right-0 -mb-6">$ 100</div>
                                        </div>
                                    </div>
                                   <p class="pt-4">Sizes :</p>
                                    <div class="pt-2">
                                        <?php
                                        $allSizes = $PrHandler->fetchAllSizesForCategories();

                                        // Separate number sizes and letter sizes
                                        $numberSizes = [];
                                        $letterSizes = [];

                                        foreach ($allSizes as $size) {
                                            if (ctype_digit(substr($size['SizeName'], 1, 1))) {
                                                // Number sizes
                                                $numberSizes[] = $size;
                                            } else {
                                                // Letter sizes
                                                $letterSizes[] = $size;
                                            }
                                        }

                                        ?>
                                    </div>
                                    <p class="pt-4">Clothes Sizes :</p>
                                    <div class="pt-2">
                                    <?php generateCheckboxes($letterSizes, isset($_SESSION['categoriesSelectedSizes']) ? $_SESSION['categoriesSelectedSizes'] : ''); ?>
                                    </div>
                                    <p class="pt-4">Pant Sizes :</p>
                                    <div class="pt-2">
                                        <?php generateCheckboxes($numberSizes, isset($_SESSION['categoriesSelectedSizes']) ? $_SESSION['categoriesSelectedSizes'] : ''); ?>
                                    </div>
                                    <input type="hidden" name="sort" value="<? $_GET['sort'] ?>">
                                    <div class="flex items-center justify-center w-full gap-2 mb-4">
                                        <input type="submit" value="Remove Filters" name="removefilter" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey w-[45%] py-1.5 mt-4">
                                        <input type="submit" value="Filter" name="filterClothes" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey w-[45%] py-1.5 mt-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="lg:hidden grid h-auto justify-items-center pt-6 pb-4 border-b-2 border-sis-grey">
                            <p class=" flex items-center gap-2 text-4xl font-bold sort-toggle cursor-pointer">
                                Sort <b class="px-2 text-2xl">[<span><?= $sortMessage ?></span>]</b>
                            </p>
                            <div class="flex flex-col overflow-hidden pt-4 pb-2 w-full sort-content">
                                <a class="block text-xl font-bold my-1 underline" href="?cg=<?= $cName ?>&sort=price_desc">Sort by Price (High to Low)</a>
                                <a class="block text-xl font-bold my-1 underline" href="?cg=<?= $cName ?>&sort=price_asc">Sort by Price (Low to High)</a>
                                <a class="block text-xl font-bold my-1 underline" href="?cg=<?= $cName ?>&sort=newest">Sort by Newest</a>
                                <a class="block text-xl font-bold my-1 underline" href="?cg=<?= $cName ?>&sort=oldest">Sort by Oldest</a>
                            </div>
                        </div>
                    </ul>

                </div>
            </div>
        </section>
    </main>
    <script src="dist/javascript/collectionsPhoneMenu.js"></script>
    <script src="dist/javascript/filterSort.js"></script>

</body>

</html>
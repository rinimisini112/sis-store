<?php
include "public/controllers/productHandler.php";
include 'public/Components/htmlHead.php';
require 'dist/pagination/Zebra_Pagination.php'; // Include Composer autoloader
// Create an instance of Zebra_Pagination
$pagination = new Zebra_Pagination();
if (isset($_SESSION['filteredItems'])) {
    $items = $_SESSION['filteredItems'];
} else {
    $items = $products->fetchProducts($sort);
}
if (isset($_GET['filtered'])) {
    $items = $products->sortFilteredItems($items,$sort);
}


// Set the total number of records
$totalRecords = count($items);
$pagination->records($totalRecords);
// Set records per page
$recordsPerPage = 10;  // You can change this value to adjust the number of products per page
$pagination->records_per_page($recordsPerPage);

// Calculate start index for data
$startIndex = ($pagination->get_page() - 1) * $recordsPerPage;
$itemsSubset = array_slice($items, $startIndex, $recordsPerPage);

?>

<body class="bg-sis-white relative">
    <header>
        <nav id="navbar" class="bg-sis-white bg-opacity-60 duration-200 z-30 text-sis-grey  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="index.php" class=" relative z-50">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <?php
            include "public/Components/phoneMenu.php";
            ?>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
            <li  class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                        </p>
                    <div class="absolute shadow-lg left-0 w-full z-50 bg-sis-white" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="blog.php" class="relative">Blog
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <?php 

            if(isset($_SESSION['vMssg'])) {
                echo "<div id='vintageCollsNotif' class='fixed md:w-[35%] w-[90%] h-[150px] flex items-center justify-center z-30 bg-sis-white border border-[rgb(178,163,156)] bg-opacity-60 shadow-2xl bottom-4 right-4'>";
                echo "<p class='text-center text-2xl font-extrabold px-4'>{$_SESSION['vMssg']}</p>";
                echo "</div>";
                unset($_SESSION['vMssg']);
            }
            ?>
                <a id="goUpBtn" href="#navbar" class="duration-300">
        <div class="fixed bottom-6 left-6 md:w-16 w-14 h-14 md:h-16 lg:animate-bounce bg-opacity-80  text-xl font-bold bg-vintage-black hover:bg-opacity-100 duration-200 text-vintage-white rounded-full z-40 flex flex-col items-center justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-up">
                <polyline points="17 11 12 6 7 11"></polyline>
                <polyline points="17 18 12 13 7 18"></polyline>
            </svg>
            <p>UP</p>
        </div>
    </a>
    <main class="w-full relative">
            
        <div class="py-6 w-[90%] mx-auto relative">
            <h1 class="text-5xl flex justify-between md:text-left text-center"><span class="rische md:inline hidden">Collections</span> <span class="rische">View our <a href="vintageCollection.php" class="rische pl-4 underline decoration-sis-grey underline-offset-4">Vintage Collection</a></span></h1>
            <p class="pt-6 text-center md:hidden block text-3xl rische">SIS Main Collection</p>
            <p class="text-2xl text-center md:pt-8 pt-2 pb-4">Your daily dose of everyday essentials</p>
        </div>
        <section>
            <div id="productNavMb" class="w-full px-6 py-4 flex items-center justify-between lg:text-xl text-lg sticky -top-0.5 bg-sis-white z-10 left-0">
                <div class="flex items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-down-right">
                        <polyline points="15 10 20 15 15 20"></polyline>
                        <path d="M4 4v7a4 4 0 0 0 4 4h12"></path>
                    </svg>
                    <p class="pr-2">Viewing</p> / <a href='collections.php' class="pl-2 font-bold">All</a>
                </div>
                <p id="lgFilterBtn" class="hidden lg:flex items-center gap-2 cursor-pointer font-normal" onclick="toggleFilterLg()">
                    <svg id="lgFilterIcon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders cursor-pointer">
                        <line x1="4" y1="21" x2="4" y2="14"></line>
                        <line x1="4" y1="10" x2="4" y2="3"></line>
                        <line x1="12" y1="21" x2="12" y2="12"></line>
                        <line x1="12" y1="8" x2="12" y2="3"></line>
                        <line x1="20" y1="21" x2="20" y2="16"></line>
                        <line x1="20" y1="12" x2="20" y2="3"></line>
                        <line x1="1" y1="14" x2="7" y2="14"></line>
                        <line x1="9" y1="8" x2="15" y2="8"></line>
                        <line x1="17" y1="16" x2="23" y2="16"></line>
                    </svg>
                    Filter
                </p>
                <div id="filterMenu" class=" absolute top-14 left-0 z-20 w-full bg-sis-white">
                    <div class="grid justify-items-center overflow-hidden pt-2 w-full">
                        <form action="public/controllers/filteredItems.php" method="post" class=" w-full flex justify-between items-center pb-16 px-16">
                            <div class="flex w-auto items-center h-auto justify-center gap-3">
                                <p class="text-xl pt-1">Price </p>
                                <div class="py-1 relative min-w-full">
                                    <input type="range" id="priceInput" name="priceRange" style="accent-color: #000000;" class="w-full h-1" min="0" max="200" step="3" value="<?php echo isset($_SESSION['priceRange']) ? $_SESSION['priceRange'] : 100; ?>" oninput="updatePriceRange(this.value)">
                                    <div id="tooltipLeft" class="absolute text-gray-800 -ml-1 bottom-0 left-0 -mb-6">$ 0</div>
                                    <div id="tooltipRight" class="absolute text-gray-800 -mr-1 bottom-0 right-0 -mb-6">$ 100</div>
                                </div>
                            </div>
                            <div class="pt-2">
                                <p class="pt-6 text-center">Sizes :</p>
                                <?php
                                $sizes = ['XS', 'S', 'M', 'L', 'XL', '2XL'];
                                foreach ($sizes as $size) {
                                    $checkboxName = 'size' . $size;
                                    $checked = isset($_SESSION['selectedSizes'][$checkboxName]) ? 'checked' : '';
                                ?>
                                    <label for="<?= $checkboxName ?>"><?= $size ?></label>
                                    <input type="checkbox" name="<?= $checkboxName ?>" class="pr-3" id="<?= $checkboxName ?>" style="accent-color: #000000;" <?= $checked ?>>
                                <?php } ?>
                            </div>
                            <div class="pt-2">
                                <p class="pt-6 text-center">Pant Sizes :</p>
                                <?php
                                $pantSizes = ['30', '31', '32', '34', '36', '38', '40'];
                                foreach ($pantSizes as $size) {
                                    $checkboxName = 'size' . $size;
                                    $checked = isset($_SESSION['selectedSizes'][$checkboxName]) ? 'checked' : '';
                                ?>
                                    <label for="<?= $checkboxName ?>"><?= $size ?></label>
                                    <input type="checkbox" name="<?= $checkboxName ?>" class="pr-3" id="<?= $checkboxName ?>" style="accent-color: #000000;" <?= $checked ?>>
                                <?php } ?>
                            </div>
                            <div class="flex flex-col">
                                <input type="submit" value="Remove Filters" name="removefilter" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey px-8 py-1.5 mt-4">
                                <input type="submit" value="Filter" name="filterClothes" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey px-8 py-1.5 mt-4">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="relative w-1/5">
                    <p class="hidden lg:block text-right cursor-pointer font-normal duration-200" id="sortDropdownTrigger">Sort
                        <b class="px-2">
                            [<span><?= $sortMessage ?></span>]
                        </b>
                    <div id="sortDropdown" class="absolute w-full z-50 left-0 text-right bg-sis-white shadow-sm shadow-sis-grey">
                        <a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2 border-b border-b-sis-grey" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_desc">Sort by Price (High to Low)</a>
                        <a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2 border-b border-b-sis-grey" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_asc">Sort by Price (Low to High)</a>
                        <a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2 border-b border-b-sis-grey" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=newest">Sort by Newest</a>
                        <a class="block text-xl duration-200 hover:bg-[rgb(215,215,215)] font-bold pr-2 py-2" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=oldest">Sort by Oldest</a>
                    </div>
                    </p>
                </div>

                <svg id="togglePhoneFilter" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders block lg:hidden cursor-pointer">
                    <line x1="4" y1="21" x2="4" y2="14"></line>
                    <line x1="4" y1="10" x2="4" y2="3"></line>
                    <line x1="12" y1="21" x2="12" y2="12"></line>
                    <line x1="12" y1="8" x2="12" y2="3"></line>
                    <line x1="20" y1="21" x2="20" y2="16"></line>
                    <line x1="20" y1="12" x2="20" y2="3"></line>
                    <line x1="1" y1="14" x2="7" y2="14"></line>
                    <line x1="9" y1="8" x2="15" y2="8"></line>
                    <line x1="17" y1="16" x2="23" y2="16"></line>
                </svg>
            </div>
            <div class="flex w-full h-auto relative">
                <div class="lg:w-[75%] w-full lg:pl-8 pl-0 h-auto mx-auto flex  flex-wrap relative">
                    <?php
                    include "public/controllers/displayProducts.php";
                    ?>
                    <?php
                    echo $pagination->render();

                    ?>
                </div>
                <div class="lg:w-[25%] h-auto w-0 lg:overflow-visible overflow-hidden lg:z-0 z-50 bg-sis-white ">
                    <ul id="categoryList" class="lg:sticky bg-sis-white lg:h-auto h-screen lg:z-0 z-50 lg:top-12 text-center lg:w-full w-0 py-4 lg:overflow-y-scroll duration-300 fixed top-0 left-0 overflow-hidden">
                        <h1 class="text-3xl pb-6 angle-font font-semibold hidden lg:block">Categories</h1>
                        <h1 id='categoriesToggle' class="text-3xl cursor-poiner pb-4 font-bold lg:hidden block">Categories</h1>
                        <div id='categoriesPhoneMenu' class="lg:block grid-cols-3 lg:pb-0 pb-2 lg:px-0 px-2">
                            <li class='py-1.5 col-span-1 lg:text-xl text-lg font-bold text-sis-grey'>
                                <a href='#' class='relative group rische'>
                                    All <span class=' absolute bottom-0 left-0 duration-200 w-full h-px group-hover:w-full bg-sis-grey'></span>
                                </a>
                            </li>

                            <?php
                            include "public/controllers/categoryList.php";
                            ?>
                        </div>
                        <div class=" grid h-auto w-full justify-items-center pt-4 pb-4 lg:border-none border-t-2 border-b-2 border-sis-grey">
                            <p class="lg:hidden flex items-center gap-2 text-4xl font-bold filter-toggle cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders cursor-pointer">
                                    <line x1="4" y1="21" x2="4" y2="14"></line>
                                    <line x1="4" y1="10" x2="4" y2="3"></line>
                                    <line x1="12" y1="21" x2="12" y2="12"></line>
                                    <line x1="12" y1="8" x2="12" y2="3"></line>
                                    <line x1="20" y1="21" x2="20" y2="16"></line>
                                    <line x1="20" y1="12" x2="20" y2="3"></line>
                                    <line x1="1" y1="14" x2="7" y2="14"></line>
                                    <line x1="9" y1="8" x2="15" y2="8"></line>
                                    <line x1="17" y1="16" x2="23" y2="16"></line>
                                </svg>
                                Filter
                            </p>
                            <div class=" overflow-hidden pt-2 px-2 w-full filter-content">
                                <form action="public/controllers/filteredItems.php" method="post" class="mx-auto w-[95%]">
                                    <p class="text-xl pt-1">Price :</p>
                                    <div class="flex w-full m-auto items-center h-auto justify-center">
                                        <div class="py-1 relative min-w-full">
                                            <input type="range" id="priceInput" name="priceRange" style="accent-color: #000000;" class="w-full h-1" min="0" max="200" step="3" value="<?php echo isset($_SESSION['priceRange']) ? $_SESSION['priceRange'] : 100; ?>" oninput="updatePriceRange(this.value)">
                                            <div id="tooltipLeft" class="absolute text-gray-800 -ml-1 bottom-0 left-0 -mb-6">$ 0</div>
                                            <div id="phoneTooltipRight" class="absolute text-gray-800 -mr-1 bottom-0 right-0 -mb-6">$ 100</div>
                                        </div>
                                    </div>
                                    <p class="pt-4">Sizes :</p>

                                    <div class="pt-2">
                                        <?php
                                        $sizes = ['XS', 'S', 'M', 'L', 'XL', '2XL'];
                                        foreach ($sizes as $size) {
                                            $checkboxName = 'size' . $size;
                                            $checked = isset($_SESSION['selectedSizes'][$checkboxName]) ? 'checked' : '';
                                        ?>
                                            <label class='text-lg' for="<?= $checkboxName ?>"><?= $size ?></label>
                                            <input type="checkbox" name="<?= $checkboxName ?>" class="pr-3" id="<?= $checkboxName ?>" style="accent-color: #000000;" <?= $checked ?>>
                                        <?php } ?>
                                    </div>
                                    <p class="pt-2 text-lg">Pant Sizes :</p>
                                    <div class="pt-2">
                                        <?php
                                        $pantSizes = ['30', '31', '32', '34', '36', '38', '40'];
                                        foreach ($pantSizes as $size) {
                                            $checkboxName = 'size' . $size;
                                            $checked = isset($_SESSION['selectedSizes'][$checkboxName]) ? 'checked' : '';
                                        ?>
                                            <label class='text-md' for="<?= $checkboxName ?>"><?= $size ?></label>
                                            <input type="checkbox" name="<?= $checkboxName ?>" class="pr-3" id="<?= $checkboxName ?>" style="accent-color: #000000;" <?= $checked ?>>
                                        <?php } ?>
                                    </div>
                                    <div class="flex items-center w-full justify-center gap-2 mb-4">
                                        <input type="submit" value="Remove Filters" name="removefilter" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey w-[45%] py-1.5 mt-4">
                                        <input type="submit" value="Filter" name="filterClothes" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey w-[45%] py-1.5 mt-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="lg:hidden grid h-auto justify-items-center pt-6 pb-4 border-b-2 border-sis-grey">
                            <p class=" flex items-center gap-2 text-4xl font-bold sort-toggle cursor-pointer">
                                Sort
                                <b class="px-2 text-2xl">
                                    [<span><?= $sortMessage ?></span>]
                                </b>
                            </p>
                            <div class="flex flex-col overflow-hidden pt-4 pb-2 w-full sort-content">
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_desc">Sort by Price (High to Low)</a>
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_asc">Sort by Price (Low to High)</a>
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=newest">Sort by Newest</a>
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=oldest">Sort by Oldest</a>

                            </div>
                        </div>
                    </ul>

                </div>
        </section>
    </main>
    <script src="dist/javascript/collectionsPhoneMenu.js"></script>
    <script src="dist/javascript/filterSort.js"></script>
    <script>
        $(document).ready(function() {
    // Wait for the document to be ready
    setTimeout(function() {
        $('#vintageCollsNotif').fadeOut('slow'); // Hide the element with a fade-out effect
    }, 4000); // 5000 milliseconds = 5 seconds
});
  
    </script>
</body>

</html>
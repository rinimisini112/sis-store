<?php
session_start();
include 'public/Components/htmlHead.php';
?>
<body class="bg-sis-white">
    <header class="">
        <nav id="navbar" class="bg-sis-white duration-200 z-40 fixed top-0 left-0 text-sis-grey  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="index.php" class=" relative z-[100]">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <?php 
            include "public/Components/phoneMenu.php";
            
            ?>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
            <li  class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                        </p>
                    <div class="absolute shadow-lg left-0 w-full bg-sis-white" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:bg-[rgb(216,216,216)] hover:text-sis-grey duration-200 py-4 pl-4">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="blog.php" class="relative">Blog
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="w-full py-8 block "></div>
    </header>

    <div class="pt-12">
        <h1 class="lg:text-7xl text-5xl lg:text-left text-center rische pl-0 lg:pl-32">The Contact zone</h1>
    </div>
    <main class="lg:w-[70%] w-[90%] bg-[#f2ede6] mb-16 h-auto justify-center flex lg:flex-row flex-col mx-auto mt-[5rem] text-sis-grey">
        <div class="w-full lg:w-[50%] lg:p-8 p-4 relative group">
            <div class="lg:block hidden w-full h-1 group-hover:h-0 duration-200 absolute top-[20%] left-0 bg-[#f2ede6] z-20"></div>
            <div class="lg:block hidden w-full h-1 group-hover:h-0 duration-200 absolute top-[50%] left-0 bg-[#f2ede6] z-20"></div>
            <div class="lg:block hidden w-full h-1 group-hover:h-0 duration-200 absolute top-[80%] left-0 bg-[#f2ede6] z-20"></div>
            <img src="resources/images/NUP_8672-min.jpg" alt="A photo of the founders together" class="w-full h-[400px] object-contain">
        </div>
        <div class=" lg:w-3/5 w-full flex flex-col gap-2 lg:ml-16 ml-0 justify-between">
            <h1 class="rische lg:text-5xl text-4xl lg:-ml-24 -ml-0 lg:pt-6 py-6 lg:text-left text-center">Write us an letter.</h1>

            <div class="flex lg:flex-row flex-col border-l-2 border-t-2 border-sis-grey h-[70%] relative">

                <?php
                if (isset($_SESSION['message'])) {
                    echo "<div id='messagePopUp' class='absolute top-0 left-0 w-full h-full duration-200 bg-[#f2ede6] flex flex-col justify-center items-center z-20 text-2xl'>";
                    echo "<p class='text-3xl px-8 py-4'>" . $_SESSION['message'] . "</p>";
                    echo "<p id='closeMessagePopUp' class='mt-2 py-2 px-8 bg-sis-grey text-sis-white cursor-pointer '>Close</p>";
                    echo "</div>";
                    session_unset();
                }
                ?>
                <form action="public/controllers/messageHandler.php" id="messageForm" method="post" class=" w-full lg:w-3/5 lg:h-[80%] flex flex-col lg:gap-6 gap-16 pt-6 items-center">
                    <div class="flex flex-col w-[75%] h-[20%]">
                        <label class='cursor-pointer' for="fullName">Full Name</label>
                        <input class=" bg-[#f2ede6] py-2 duration-300 border-b-2 placeholder:text-red-500 border-gray-400 focus:border-sis-grey outline-none" type="text" name="fullName" id="fullName">
                    </div>
                    <div class="flex flex-col w-[75%] h-[20%]">
                        <label class='cursor-pointer' for="email">E-mail</label>
                        <input class=" bg-[#f2ede6] py-2 duration-300 border-b-2 placeholder:text-red-500 border-gray-400 focus:border-sis-grey outline-none" type="email" name="email" id="email">
                    </div>
                    <div class="flex flex-col w-[75%] h-[20%]">
                        <label class='cursor-pointer' for="message">Message</label>
                        <input class=" bg-[#f2ede6] py-2 duration-300 border-b-2 placeholder:text-red-500 border-gray-400 focus:border-sis-grey outline-none" type="text" name="message" id="message">
                    </div>
                    <input type="submit" value="Contact Us" class="w-[75%] hover:outline hover:outline-1 hover:outline-sis-grey uppercase mt-3 cursor-pointer hover:bg-sis-white hover:text-sis-grey duration-200 pb-2 pt-3 bg-sis-grey text-sis-white relative angle-font text-xl font-semibold">
                </form>
                <div class="lg:w-2/5 w-full flex flex-col justify-between items-center py-12">
                    <div class="text-xl">
                        <h3 class=" font-bold"><b>Contact</b></h3>
                        <a class="lg:mb-0 mb-4" href="mailto:sisxstore@gmail.com">sisxstore@gmail.com</a>
                        <h3 class="font-bold">Based in</h3>
                        <p class="lg:pb-0 pb-4">Prishtina, Kosovo</p>
                    </div>
                    <div class="flex gap-6">
                        <a href="https://www.instagram.com/sis.store.pr/" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram hover:fill-sis-grey hover:stroke-white duration-200">
                                <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                                <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                                <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                            </svg>
                        </a>
                        <a href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook  hover:fill-sis-grey hover:stroke-white duration-200">
                                <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                            </svg>
                        </a>
                        <a href="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter  hover:fill-sis-grey hover:stroke-white duration-200">
                                <path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div>
        <h2 class="text-4xl lg:text-left text-center lg:text-6xl angle-font font-semibold lg:pl-32 py-16">Where to find us</h2>
    </div>
    <article class=" flex w-full md:flex-row flex-col h-auto lg:h-[400px] justify-between items-center mb-32 ">
        <div class="flex lg:flex-row flex-col lg:w-[50%] w-full lg:h-[400px] h-auto">
            <ul class="lg:w-[50%] w-full lg:h-[400px] h-[250px] bg-sis-grey text-sis-white flex flex-col items-center justify-center">
                <p class="text-2xl font-bold  pb-4">Call us</p>
                <li class="w-[60%]"><a class="flex py-1 items-center lg:justify-between justify-evenly" href="tel:+38343555450"><img src="resources/SVG/whitePhone.svg" alt=""><span class="underline hover:decoration-sis-white duration-300 decoration-sis-grey">043 555 450</span></a></li>
                <li class="w-[60%]"><a class="flex py-1 items-center lg:justify-between justify-evenly" href="https://wa.me/+38343555450" target="_blank"><img src="resources/SVG/whiteWhp.svg" alt=""><span class="underline hover:decoration-sis-white duration-300 decoration-sis-grey">+383 43 555 450</span></a></li>
            </ul>
            <ul class="lg:w-[50%] w-full lg:h-[400px] h-[250px] bg-[#f2ede6] text-sis-grey flex flex-col items-center justify-center">
                <p class="text-2xl font-bold  pb-4">Write us in social media</p>
                <li class="text-xl py-1 "><a href="https://www.instagram.com/sis.store.pr/" target="_blank">on <span class="font-bold underline hover:decoration-sis-grey duration-300 decoration-sis-white">Instagram</span></a></li>
                <li class="text-xl py-1 "><a href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">on <span class="font-bold underline hover:decoration-sis-grey duration-300 decoration-sis-white">Facebook</span></a></li>
            </ul>
        </div>
        <div class="w-full lg:w-[40%]">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1116.8567404501289!2d21.17610582499726!3d42.656124041851484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13549fa5ca5d1b63%3A0x81049182679f074e!2sRezarta%20Galica%20Beauty%20Pros!5e0!3m2!1sen!2s!4v1700068683479!5m2!1sen!2s" class="w-full h-[400px]" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
        <div class="lg:flex hidden w-[10%]  items-center justify-center h-[400px] relative bg-sis-grey">
            <h1 class=" text-4xl font-bold text-sis-white horizontal-txt">We are located at</h1>
        </div>
    </article>
    <article class="lg:w-[70%] w-[90%] mx-auto h-auto pb-32" id="FAQ">
        <h1 class=" lg:text-6xl text-4xl text-right lg:pr-16 pr-0 py-16 angle-font font-semibold">Frequently asked questions</h1>
        <?php include 'public/controllers/faqAccordion.php' ?>
    </article>
    <footer id="footer" class="w-full flex md:flex-row flex-col-reverse bg-sis-grey text-sis-white relative">
    <div class="md:w-1/2 w-full py-8 md:block flex flex-col items-center justify-center">
        <img src="resources/images/Logo_Sis_Store_1_BW_edited.jpg" class=" w-[85%] md:-ml-16 -ml-0 object-cover" alt="company-logo">
        <form action="" class="lg:py-6 md:py-10 py-6 lg:w-[70%] md:w-[90%] w-full px-8">
            <p class=" text-2xl">Subsribe to our Newsletter,
                stay informed on sales and new relases.
            </p>
            <span class="relative">
                <input class="w-full py-2 bg-sis-grey border-b-2 pl-1 border-white my-3 outline-none placeholder:text-sis-white" type="email" name="email" id="email" placeholder="Enter your email">
                <input type="submit" class=" absolute right-2 top-0" value="Subscribe">
            </span>
        </form>
        <div class="lg:w-[70%] md:w-[90%] w-full px-8 flex pb-16 md:pb-0 flex-col lg:py-0 md:py-4 py-0">
            <p class=" text-center text-3xl py-2">Follow us on social media.</p>
            <div class="flex items-center justify-center gap-8 py-6">
                <span class="text-xl">
                    <a href="https://www.instagram.com/sis.store.pr/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram hover:fill-sis-white hover:stroke-sis-grey duration-300">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                    </a>
                </span>
                <span class="text-xl">
                    <a href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook hover:fill-sis-white hover:stroke-sis-grey duration-300">
                            <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                        </svg>
                    </a>
                </span>
            </div>
        </div>
        <div class="w-full py-8"></div>
    </div>
    <div class=" grid md:mt-28 mt-16 md:grid-cols-2 grid-cols-1 grid-rows-2 md:gap-x-16 gap-x-0 gap-y-0 md:gap-y-16 md:m-auto mx-auto md:w-1/2 w-[90%]">
        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Browse Clothes</p>
            <?php
            include 'public/controllers/footerContactUs.php';
            ?>
        </ul>
        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto md:mt-0 mt-4">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Company</p>
            <li class="text-lg py-2">
                <a class="group relative" href="aboutUs.php">About Us
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="aboutUs.php">Story
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="https://www.instagram.com/sis.store.pr/">Collab with us
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
                </a>
            </li>
        </ul>

        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Contact Us</p>
            <li class="text-lg py-2">
                <a class="group relative" href="tel:+38343555450">043 555 450
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>

                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>

                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="mailto:sisxstore@gmail.com">sisxstore@gmail.com
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>

                </a>
            </li>
        </ul>
        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto md:mt-0 mt-4">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Help</p>
            <li class="text-lg py-2">
            <a class="group relative" href="contactUs.php#FAQ">
                FAQ
                <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
            </a>
            </li>
            <li class="text-lg py-2">
            <a class="group relative" href="terms&conditions.php">
                Terms of Service
                <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
            </a>
            </li>
            <li class="text-lg py-2 cursor-pointer">No online shopping for the moment</li>
        </ul>
    </div>
    <div class=" absolute bottom-0 left-0 w-full bg-sis-white text-sis-grey flex md:flex-row flex-col py-2 items-center justify-center md:justify-evenly">
        <p class="lg:py-0 md:py-8 py-0 md:px-8 px-0 pt-6 md:text-xl text-lg rische">Copyright 2023 - SIS Store&copy;</p>
        <p class=" md:px-8 px-0 md:text-xl text-lg  rische">Website by : <a href="" class="angle-font font-semibold border-b border-sis-grey">Rini Misini</a></p>
    </div>
</footer>
    <script src="dist/javascript/aboutUsAnimations.js">
    </script>
    <script>
 $(document).ready(function () {
        // Use jQuery click method to add a click event listener
        $('#closeMessagePopUp').click(function () {
            // Use jQuery hide method to hide the popup
            $('#messagePopUp').hide();
        });
    });
        $(document).ready(function() {
            $.validator.addMethod("alphabetsOnly", function(value, element) {
                return /^[a-zA-Z\s]+$/.test(value);
            }, "Enter letters only.");

            $("#messageForm").validate({
                rules: {
                    email: {
                        email: true,
                        required: true,
                    },
                    message: {
                        required: true,
                    },
                    fullName: {
                        required: true,
                    },
                },
                messages: {
                    email: {
                        required: "Please enter your email",
                        email: "Enter a valid email address",
                    },
                    message: {
                        required: "Message should not be blank",
                    },
                    fullName: {
                        required: 'This field should not be blank',
                    },
                },
                errorPlacement: function (error, element) {
                // Set the error message as the placeholder
                element.attr("placeholder", error.text());
            },
        });
    });
        document.addEventListener('DOMContentLoaded', function() {
            const sections = document.querySelectorAll('.accordion-section');

            sections.forEach((section) => {
                section.addEventListener('click', function() {
                    const content = this.querySelector('.accordion-content');
                    const isOpen = content.style.maxHeight !== '0px';
                    const chevronImg = this.querySelector('.accordion-img');
                    const box = this.querySelector('.accordion-box'); // Get the specific box associated with the clicked section

                    // Close all sections
                    sections.forEach((s) => {
                        const c = s.querySelector('.accordion-content');
                        const img = s.querySelector('.accordion-img');
                        const b = s.querySelector('.accordion-box'); // Get the box associated with each section
                        if (s !== section && c.style.maxHeight !== '0px') {
                            gsap.to(c, {
                                maxHeight: 0,
                                duration: 0.1
                            });
                            gsap.to(img, {
                                rotate: 0,
                                duration: 0.1
                            });
                            gsap.to(b, {
                                backgroundColor: '',
                                duration: 0.1
                            }); // Reset background color
                        }
                    });

                    // Toggle the selected section
                    gsap.to(content, {
                        maxHeight: isOpen ? 0 : content.scrollHeight,
                        duration: 0.1
                    });
                    gsap.to(chevronImg, {
                        rotate: isOpen ? 0 : 180,
                        duration: 0.1
                    });

                    // Change background color of clicked section
                    gsap.to(box, {
                        backgroundColor: isOpen ? '' : '#000', // Change the color as needed
                        duration: 0.1,
                    });
                });
            });
        });
    </script>
</body>
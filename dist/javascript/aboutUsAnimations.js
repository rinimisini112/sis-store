const phoneMenuIcon = document.getElementById("openPhoneMenuIcon");
  const navbar = document.getElementById("navbar");
  const footer = document.getElementById('footer');
    // Set initial styles when the DOM is loaded

var lines = document.querySelectorAll(".burger_line");
const whatsappSVG = document.getElementById('whatsappSVG');

var phoneMenu = document.getElementById('phoneMenu');
var isOpen = false;

function togglePhoneMenu() {
  const duration = 0.2; // Adjust the duration as needed

  if (!isOpen) {
      // Using GSAP to animate the opening of the menu
      gsap.to(phoneMenu, {
          duration,
          maxWidth: '100%',
          onComplete: () => {
              phoneMenu.classList.add('duration-200');

              // Animate each menu item to become visible
          },
      });
  } else {
      // Using GSAP to animate the closing of the menu
      gsap.to(phoneMenu, {
          duration,
          maxWidth: 0,
          onComplete: () => {
              phoneMenu.classList.remove('duration-200');
          },
      });
  }

  isOpen = !isOpen;
}

phoneMenuIcon.addEventListener('click', togglePhoneMenu);
var open = false;

function toggleMenu() {
  const menuItems = document.querySelectorAll("#phoneMenu > *");
  const listMenuItems = document.querySelectorAll("#phoneMenu > ul > *");
    let lines = document.querySelectorAll('.burger_line');

    gsap.to(lines[0], { duration: 0.05, rotate: open ? 0 : 45 });
    gsap.to(lines[1], { duration: 0.2, x: open ? 0 : 45, opacity: open ? 1 : 5 });
    gsap.to(lines[2], { duration: 0.05, rotate: open ? 0 : -45 });
    open = !open;
    gsap.fromTo(menuItems, { opacity: 0, x: -30 }, { opacity: 1, x: 0, duration: 0.2, stagger: 0.1 });
    gsap.fromTo(listMenuItems, { opacity: 0, x: -30 }, { opacity: 1, x: 0, duration: 0.2, stagger: 0.1 });
}
document.getElementById('openPhoneMenuIcon').addEventListener('click', toggleMenu);

  // Function to set the initial styles
    
  
  
  // Function to animate the navbar on enter
  const animateNavbarOnEnter = () => {
    gsap.to(navbar, { opacity: 0, duration: 0.3 });
  };
  
  // Function to animate the navbar on leave
  const animateNavbarOnLeave = () => {
    gsap.to(navbar, { opacity: 1, duration: 0.3 });
  };
  
  // ScrollTrigger for entering the footer section
  ScrollTrigger.create({
    trigger: footer,
    start: "top 60%", // Adjust the start position as needed
    onEnter: animateNavbarOnEnter,
    onLeave: animateNavbarOnLeave,
    onEnterBack: animateNavbarOnEnter,
    onLeaveBack: animateNavbarOnLeave,
  });
  
  const setNavbarBoxShadow = (boxShadow) => {
    gsap.to(navbar, { boxShadow, duration: 0.3 });
  };
  
  // ScrollTrigger for scrolling down from the top
  ScrollTrigger.create({
    trigger: navbar,
    start: 'bottom 5%',
    end: 'bottom', // Adjust the end position as needed
    onEnter: () => setNavbarBoxShadow('0 4px 4px rgba(50, 50, 50, 0.2)'),
    onLeaveBack: () => setNavbarBoxShadow('none'), // Reset box shadow when scrolling leaves the section
  });

  var collectionsDropdown = document.getElementById('collectionsDropdown');
  var chevronIcon = document.getElementById('chevronIcon');
  var collectionsParagraph = document.getElementById('toggleCollections');
  
  function isCollectionsVisible() {
      return parseFloat(window.getComputedStyle(collectionsDropdown).opacity) > 0;
  }
  
  function toggleCollections() {
      var isVisible = isCollectionsVisible();
  
      gsap.to(collectionsDropdown, {
          opacity: isVisible ? 0 : 1,
          zIndex: isVisible ? -50 : 20,
          visibility: isVisible ? 'hidden' : 'visible',
          duration: 0.2,
          onComplete: function () {
              if (isVisible) {
                  gsap.set(collectionsDropdown, { visibility: 'hidden' });
              }
          },
      });
      gsap.to(chevronIcon, { rotation: isVisible ? 0 : 180, duration: 0.2,x: isVisible ? 5 : 10 });
  }
  
  // Toggle collections on click
  collectionsParagraph.addEventListener('click', toggleCollections);
  
  // Close collections dropdown when clicking outside

  var isDropdownVisible = false;

var collectionsLi = document.getElementById('collectionsLi');
var collectionsPhoneDropdown = document.getElementById('collectionsPhoneDropdown');
var collectionsPhoneIcon = document.getElementById('phoneCollectionsIcon');
collectionsLi.addEventListener('click', function() {
  isDropdownVisible = !isDropdownVisible;

  gsap.to(collectionsPhoneDropdown, {
    maxHeight: isDropdownVisible ? collectionsPhoneDropdown.scrollHeight + 'px' : 0,
    ease: 'power2.inOut',
    duration: 0.2,
    onComplete: function() {
      if (!isDropdownVisible) {
        collectionsPhoneDropdown.style.overflow = 'hidden';
      } 
    }
  });
  gsap.to(collectionsPhoneIcon, {
    rotation: isDropdownVisible ? 180 : 0,
    duration: 0.3
  })
});
window.onclick = function (event) {
  var button = document.getElementById('openPhoneMenuIcon');

  if (open && !button.contains(event.target) && !phoneMenu.contains(event.target)) {
      togglePhoneMenu();
      toggleMenu();
  } else if (isCollectionsVisible() && !collectionsDropdown.contains(event.target)) {
      toggleCollections();
  }
}
$(document).ready(function() {
    $.validator.addMethod("alphabetsOnly", function(value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");

    $("#login").validate({
        rules: {
            username: {
                required: true,
            },
            password: {
                required: true,
            },
        },
        messages: {
            username: {
                required: "This field should not be blank",
            },
            password: {
                required: "This field should not be blank",
            },
                },
        errorPlacement: function(error, element) {
            // Place the error message below the input element
            error.insertAfter(element);

            // Add a class to the input when there is an error
            element.addClass("error_effect");
        },
        success: function(label, element) {
            // Remove the error class when the input is valid
            $(element).removeClass("error_effect");
        }
    });
});
$(document).ready(function() {

    $.validator.addMethod("alphabetsOnly", function(value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
return arg !== value;
}, "Value must not equal arg.");
    $("#productForm").validate({
        rules: {
            productName: {
                required: true,
            },
            productDesc: {
                required: true,
            },
            productPrice: {
                required: true,
            },
            main_image: {
                required: true,
            },
            category: { 
            valueNotEquals: "0",
            },
        },
        messages: {
            productName: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            productDesc: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            productPrice: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            main_image: {
                required: "Minimumi kete foto me vendos se i prishet pamja te collections",
            },
            category: {
                valueNotEquals: "Ju lutem zgjedhni nje kategori",
            },
        },
    errorPlacement: function(error, element) {
        // Place the error message below the input element
        error.insertAfter(element);

        // Add a class to the input when there is an error
        element.addClass("error_effect");
    },
    success: function(label, element) {
        // Remove the error class when the input is valid
        $(element).removeClass("error_effect");
    }
    ,
    errorPlacement: function (error, element) {
            if (element.hasClass("checkbox")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
});
});
const blogPictureInput = document.getElementById('main_image');
const image1Input = document.getElementById('image1');
const image2Input = document.getElementById('image2');
const image3Input = document.getElementById('image3');
const image4Input = document.getElementById('image4');
const image5Input = document.getElementById('image5');
const image6Input = document.getElementById('image6');
const imagePreview = document.getElementById('mainImg');
const img1 = document.getElementById('img1');
const img2 = document.getElementById('img2');
const img3 = document.getElementById('img3');
const img4 = document.getElementById('img4');
const img5 = document.getElementById('img5');
const img6 = document.getElementById('img6');

function showImg(input, previewContainer) {
    input.addEventListener('change', function(event) {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                const image = document.createElement('img');
                image.src = e.target.result;
                previewContainer.innerHTML = '';
                image.className = 'w-full h-full object-cover';
                previewContainer.appendChild(image);
            };
            reader.readAsDataURL(file);
        } else {
            previewContainer.innerHTML = '';
        }
    });
}
showImg(blogPictureInput, imagePreview);
showImg(image1Input, img1);
showImg(image2Input, img2);
showImg(image3Input, img3);
showImg(image4Input, img4);
showImg(image5Input, img5);
showImg(image6Input, img6);

$(document).ready(function() {
    $.validator.addMethod("alphabetsOnly", function(value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");

    $("#editProductForm").validate({
        rules: {
            productName: {
                required: true,
            },
            productDesc: {
                required: true,
            },
            productPrice: {
                required: true,
            },
        },
        messages: {
            productName: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            productDesc: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            productPrice: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },

        },
        errorPlacement: function(error, element) {
            // Place the error message below the input element
            error.insertAfter(element);

            // Add a class to the input when there is an error
            element.addClass("error_effect");
        },
        success: function(label, element) {
            // Remove the error class when the input is valid
            $(element).removeClass("error_effect");
        }
    });
});

$(document).ready(function() {
    // Wait for the document to be ready
    setTimeout(function() {
        $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
    }, 5000); // 5000 milliseconds = 5 seconds
});
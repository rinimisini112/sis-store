var navbar = document.getElementById('navbar');
var NavbarTrigger = document.getElementById('mainContainer');

var buttonTimeline = gsap.timeline({
  scrollTrigger: {
    trigger: NavbarTrigger,
    start: "top 45%",
    end: "+=100%",
    scrub: true,
  },
});

gsap.set(navbar, {
  'text-shadow': '0 0 2px gray',
  backgroundColor: 'transparent', // Set the initial background color
});

buttonTimeline.to(navbar, {
  'text-shadow': 'none', // Remove text shadow
  backgroundColor: '#E3E3E3ff', // Change background color to white
  duration: 0.5,
  onComplete: function() {
    // Add the class 'shadow-lg' after the animation has completed
    navbar.classList.add('shadow-lg');
    navbar.classList.remove('text-sis-white');
    navbar.classList.add('text-sis-grey');
    collectionsDropdownChildren.forEach(function (child) {
        child.classList.add('bg-sis-white');
        child.classList.add('hover:bg-[rgb(216,216,216)]');
        child.classList.remove('hover:backdrop-blur-md');
    }
)},
  onReverseComplete: function() {
    // Remove the class 'shadow-lg' after the reverse animation has completed
    navbar.classList.remove('shadow-lg');
    navbar.classList.add('text-sis-white');
    navbar.classList.remove('text-sis-grey');
    collectionsDropdownChildren.forEach(function (child){
        child.classList.remove('bg-sis-white');
        child.classList.remove('hover:bg-[rgb(216,216,216)]');
        child.classList.add('hover:backdrop-blur-md');
    }
)},
});
var collectionsDropdown = document.getElementById("collectionsDropdown");
var collectionsDropdownChildren = document.querySelectorAll(
  "#collectionsDropdown ul li"
);
var chevronIcon = document.getElementById("chevronIcon");
var collectionsParagraph = document.getElementById("toggleCollections");

function isCollectionsVisible() {
  return parseFloat(window.getComputedStyle(collectionsDropdown).opacity) > 0;
}

function toggleCollections() {
  var isVisible = isCollectionsVisible();

  gsap.to(collectionsDropdown, {
    opacity: isVisible ? 0 : 1,
    zIndex: isVisible ? -50 : 20,
    visibility: isVisible ? "hidden" : "visible",
    duration: 0.2,
    onComplete: function () {
      if (isVisible) {
        gsap.set(collectionsDropdown, { visibility: "hidden" });
      }
    },
  });
  gsap.to(chevronIcon, {
    rotation: isVisible ? 0 : 180,
    duration: 0.2,
    x: isVisible ? 5 : 10,
  });
}

// Toggle collections on click
collectionsParagraph.addEventListener("click", toggleCollections);
var isDropdownVisible = false;

var collectionsLi = document.getElementById('collectionsLi');
var collectionsPhoneDropdown = document.getElementById('collectionsPhoneDropdown');
var collectionsPhoneIcon = document.getElementById('phoneCollectionsIcon');
collectionsLi.addEventListener('click', function() {
  isDropdownVisible = !isDropdownVisible;

  gsap.to(collectionsPhoneDropdown, {
    maxHeight: isDropdownVisible ? collectionsPhoneDropdown.scrollHeight + 'px' : 0,
    ease: 'power2.inOut',
    duration: 0.2,
    onComplete: function() {
      if (!isDropdownVisible) {
        collectionsPhoneDropdown.style.overflow = 'hidden';
      } 
    }
  });
  gsap.to(collectionsPhoneIcon, {
    rotation: isDropdownVisible ? 180 : 0,
    duration: 0.3
  })
})
let lines = document.querySelectorAll(".burger_line");
const whatsappSVG = document.getElementById("whatsappSVG");

let phoneMenu = document.getElementById("phoneMenu");
const phoneMenuIcon = document.getElementById("openPhoneMenuIcon");
let phoneMenuChildren = document.querySelectorAll("#phoneMenu ul li");
let isOpen = false;

function togglePhoneMenu() {
  const duration = 0.2; // Adjust the duration as needed

  if (!isOpen) {
    // Using GSAP to animate the opening of the menu
    gsap.to(phoneMenu, {
      duration,
      maxWidth: "100%",
      onComplete: () => {
        phoneMenu.classList.add("duration-200");

        // Animate each menu item to become visible
      },
    });
  } else {
    // Using GSAP to animate the closing of the menu
    gsap.to(phoneMenu, {
      duration,
      maxWidth: 0,
      onComplete: () => {},
    });
  }

  isOpen = !isOpen;
}

phoneMenuIcon.addEventListener("click", togglePhoneMenu);
let open = false;

function toggleMenu() {
  const menuItems = document.querySelectorAll("#phoneMenu > *");
  const listMenuItems = document.querySelectorAll("#phoneMenu > ul > *");
  let lines = document.querySelectorAll(".burger_line");

  gsap.to(lines[0], { duration: 0.05, rotate: open ? 0 : 45 });
  gsap.to(lines[1], {
    duration: 0.2,
    x: open ? 0 : 45,
    opacity: open ? 1 : 5,
  });
  gsap.to(lines[2], { duration: 0.05, rotate: open ? 0 : -45 });
  open = !open;
  gsap.fromTo(
    menuItems,
    { opacity: 0, x: -30 },
    { opacity: 1, x: 0, duration: 0.3, stagger: 0.2 }
  );
  gsap.fromTo(
    listMenuItems,
    { opacity: 0, x: -30 },
    { opacity: 1, x: 0, duration: 0.2, stagger: 0.1 }
  );
}
document
  .getElementById("openPhoneMenuIcon")
  .addEventListener("click", toggleMenu);

window.onclick = function (event) {
  let phoneMenu = document.getElementById("phoneMenu");
  let button = document.getElementById("openPhoneMenuIcon");

  if (
    !phoneMenu.contains(event.target) &&
    !button.contains(event.target) &&
    open
  ) {
    togglePhoneMenu();
    toggleMenu();
  } else if (
    !collectionsDropdown.contains(event.target) &&
    isCollectionsVisible()
  ) {
    toggleCollections();
  }
};


const phoneMenuIcon = document.getElementById("openPhoneMenuIcon");
const navbar = document.getElementById("navbar");
const footer = document.getElementById('footer');

var phoneMenu = document.getElementById('phoneMenu');
var isOpen = false;
var filterMenuIsOpen = false;

function togglePhoneMenu() {
const duration = 0.2; // Adjust the duration as needed

if (!isOpen) {
    if(filterMenuIsOpen) {
        toggleFilterMenu();
        filterMenuIsOpen = false;
    }
    // Using GSAP to animate the opening of the menu
    gsap.to(phoneMenu, {
        duration,
        maxWidth: '100%',
        onComplete: () => {
            phoneMenu.classList.add('duration-200');
            navbar.classList.add('fixed');
            phoneMenuIsOpen = true;
        },
    });
} else {
    // Using GSAP to animate the closing of the menu
    gsap.to(phoneMenu, {
        duration,
        maxWidth: 0,
        onComplete: () => {
            phoneMenu.classList.remove('duration-200');
            navbar.classList.remove('fixed');
            phoneMenuIsOpen = false;
        },
    });
}

isOpen = !isOpen;
}

phoneMenuIcon.addEventListener('click', togglePhoneMenu);
var open = false;

function toggleMenu() {
var menuItems = document.querySelectorAll("#phoneMenu > *");
var listMenuItems = document.querySelectorAll("#phoneMenu > ul > *");
  var lines = document.querySelectorAll('.burger_line');

  gsap.to(lines[0], { duration: 0.05, rotate: open ? 0 : 45 });
  gsap.to(lines[1], { duration: 0.2, x: open ? 0 : 45, opacity: open ? 1 : 5 });
  gsap.to(lines[2], { duration: 0.05, rotate: open ? 0 : -45 });
  open = !open;
  gsap.fromTo(menuItems, { opacity: 0, x: -30 }, { opacity: 1, x: 0, duration: 0.3, stagger: 0.2 });
  gsap.fromTo(listMenuItems, { opacity: 0, x: -30 }, { opacity: 1, x: 0, duration: 0.2, stagger: 0.1 });
}
document.getElementById('openPhoneMenuIcon').addEventListener('click', toggleMenu);


  
// Function to set the initial styles
var categoryList = document.getElementById('categoryList');
var svg = document.getElementById('togglePhoneFilter');
var filterLine = document.querySelectorAll('#togglePhoneFilter line');

// Initial state (collapsed)
var isExpanded = false;

function toggleFilterMenu() {
    isExpanded = !isExpanded;
    filterMenuIsOpen = true;
    // Use GSAP to animate the width
  gsap.to(categoryList, {
  width: isExpanded ? '80%' : '0%',
  ease: 'power2.inOut',
  duration: 0.2
});
  
    // Add your additional animations or logic if needed
  
    gsap.to(filterLine[0], { duration: 0.2, y: isExpanded ? 20 : 0, transformOrigin: 'center center ' });
    gsap.to(filterLine[1], { duration: 0.2, rotation: isExpanded ? 0 : 0, transformOrigin: ' center center' });
    gsap.to(filterLine[2], { duration: 0.2, rotation: isExpanded ? 0 : 0, transformOrigin: 'center center' });
    gsap.to(filterLine[3], { duration: 0.2, y: isExpanded ? 30 : 0, transformOrigin: 'center' });
    gsap.to(filterLine[4], { duration: 0.2, y: isExpanded ? 20 : 0, transformOrigin: 'top' });
    gsap.to(filterLine[5], { duration: 0.2, y: isExpanded ? 0 : 0, transformOrigin: 'center center ' });
    gsap.to(filterLine[6], { duration: 0.2, rotation: isExpanded ? 90 : 0, transformOrigin: 'center center' });
    gsap.to(filterLine[7], { duration: 0.2, rotation: isExpanded ? 90 : 0, transformOrigin: 'center center' });
    gsap.to(filterLine[8], { duration: 0.2, rotation: isExpanded ? 90 : 0, transformOrigin: 'bottom ' });
  }
  

  svg.addEventListener('click',toggleFilterMenu);

    
    var pcScreen = window.innerWidth >= 1025;

    if(pcScreen) {
    
var productNav = document.getElementById('productNav');

var setProductNavBoxShadow = (boxShadow) => {
  gsap.to(productNav, { boxShadow, duration: 0.3 });
};

ScrollTrigger.create({
  trigger: productNav,
  start: 'bottom 5%', // Adjust the start position as needed
  end: 'bottom 5%', // Adjust the end position as needed
  onEnter: () => setProductNavBoxShadow('0 4px 4px rgba(50, 50, 50, 0.4)'),
  onLeaveBack: () => setProductNavBoxShadow('none'),
});
    }

    var collectionsDropdown = document.getElementById('collectionsDropdown');
    var chevronIcon = document.getElementById('chevronIcon');
    var collectionsParagraph = document.getElementById('toggleCollections');
    
    function isCollectionsVisible() {
        return parseFloat(window.getComputedStyle(collectionsDropdown).opacity) > 0;
    }
    
    function toggleCollections() {
        var isVisible = isCollectionsVisible();
    
        gsap.to(collectionsDropdown, {
            opacity: isVisible ? 0 : 1,
            zIndex: isVisible ? -50 : 20,
            visibility: isVisible ? 'hidden' : 'visible',
            duration: 0.2,
            onComplete: function () {
                if (isVisible) {
                    gsap.set(collectionsDropdown, { visibility: 'hidden' });
                }
            },
        });
        gsap.to(chevronIcon, { rotation: isVisible ? 0 : 180, duration: 0.2,x: isVisible ? 5 : 10 });
    }
    
    // Toggle collections on click
    collectionsParagraph.addEventListener('click', toggleCollections);
    
    // Close collections dropdown when clicking outside
 
    var isDropdownVisible = false;

var collectionsLi = document.getElementById('collectionsLi');
var collectionsPhoneDropdown = document.getElementById('collectionsPhoneDropdown');
var collectionsPhoneIcon = document.getElementById('phoneCollectionsIcon');
collectionsLi.addEventListener('click', function() {
  isDropdownVisible = !isDropdownVisible;

  gsap.to(collectionsPhoneDropdown, {
    maxHeight: isDropdownVisible ? collectionsPhoneDropdown.scrollHeight + 'px' : 0,
    ease: 'power2.inOut',
    duration: 0.2,
    onComplete: function() {
      if (!isDropdownVisible) {
        collectionsPhoneDropdown.style.overflow = 'hidden';
      } 
    }
  });
  gsap.to(collectionsPhoneIcon, {
    rotation: isDropdownVisible ? 180 : 0,
    duration: 0.3
  })
}); 
    var goUpBtn = document.getElementById('goUpBtn');
        var productNavbarTrigger = document.getElementById('productNavMb');
        
        var buttonTimeline = gsap.timeline({
    scrollTrigger: {
      trigger: productNavbarTrigger,
      start: "top 100%",
      end: "+=100%",
      scrub: true,
    },
  });
  gsap.set(goUpBtn, {
    opacity : 0 ,
      display: "hidden",
      zIndex: -30,
  });

  buttonTimeline.to(goUpBtn, {
    opacity: 1,
      zIndex: 0,
        display: "block",
    duration: 0.5,
  });
  window.onclick = function (event) {
    var filterMenuButton = document.getElementById('togglePhoneFilter'); // replace 'togglePhoneFilter' with the actual ID
    var button = document.getElementById('openPhoneMenuIcon');
    
    if (!categoryList.contains(event.target) && isExpanded && event.target !== filterMenuButton) {
      toggleFilterMenu();
      return;
    } else if (open && !button.contains(event.target) && !phoneMenu.contains(event.target)) {
        togglePhoneMenu();
          toggleMenu();
        return;
        } else if (!collectionsDropdown.contains(event.target) && isCollectionsVisible()) {
            toggleCollections();
        }
    }
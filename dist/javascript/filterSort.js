const priceInput = document.getElementById("priceInput");
const tooltipLeft = document.getElementById("tooltipLeft");
const tooltipRight = document.getElementById("tooltipRight");
const phoneTooltipRight = document.getElementById("phoneTooltipRight");

// Function to update the price range based on user input
function updatePriceRange(value) {
  // Update the tooltip values
  const minPrice = 0;
  const maxPrice = 100; // Adjusted to match the range input's max value

  const currentPrice = Math.round(
    (value / 100) * (maxPrice - minPrice) + minPrice
  );

  tooltipLeft.textContent = `$ ${minPrice}`;
  tooltipRight.textContent = `$ ${currentPrice}`;
  phoneTooltipRight.textContent = `$ ${currentPrice}`;
}

// Initialize the price range with the initial value
updatePriceRange(priceInput.value);

document.addEventListener("DOMContentLoaded", function () {
  let filterBtn = document.getElementById("lgFilterBtn");
  // Get the elements
  var sortToggle = document.querySelector(".sort-toggle");
  var sortContent = document.querySelector(".sort-content");

  // Set the initial state
  gsap.set(sortContent, {
    height: 0,
    opacity: 0,
    display: "none",
  });

  // Function to toggle the sort content
  function toggleSortContent() {
    gsap.to(sortContent, {
      height: sortContent.clientHeight === 0 ? "auto" : 0,
      opacity: sortContent.clientHeight === 0 ? 1 : 0,
      display: sortContent.clientHeight === 0 ? "block" : "none",
      duration: 0.5,
      ease: "power2.inOut",
    });
  }

  // Event listener for the sort toggle
  sortToggle.addEventListener("click", toggleSortContent);
  // Get the elements
  var filterToggle = document.querySelector(".filter-toggle");
  var filterContent = document.querySelector(".filter-content");

  // Set the initial state
  gsap.set(filterContent, {
    height: 0,
    opacity: 0,
    display: "none",
  });

var isMobileScreenForCtg = window.innerWidth <= 1025;

if(isMobileScreenForCtg) {
  var categoriesMenu = document.getElementById("categoriesPhoneMenu");
   var categoriesToggle = document.getElementById('categoriesToggle');  // Set the initial state
    gsap.set(categoriesMenu, {
      height: 0,
      opacity: 0,
      display: "none",
    });
  
    // Function to toggle the sort content
    function toggleCategoriesContent() {
      gsap.to(categoriesMenu, {
        height: categoriesMenu.clientHeight === 0 ? "auto" : 0,
        opacity: categoriesMenu.clientHeight === 0 ? 1 : 0,
        display: categoriesMenu.clientHeight === 0 ? "grid" : "none",
        duration: 0.5,
        ease: "power2.inOut",
      });
    }
  
    // Event listener for the sort toggle
    categoriesToggle.addEventListener("click", toggleCategoriesContent);
}  
    // Get the elements
    var filterToggle = document.querySelector(".filter-toggle");
    var filterContent = document.querySelector(".filter-content");
  
    // Set the initial state
    gsap.set(filterContent, {
      height: 0,
      opacity: 0,
      display: "none",
    });


  // Function to toggle the filter content
  function toggleFilterContent() {
    gsap.to(filterContent, {
      height: filterContent.clientHeight === 0 ? "auto" : 0,
      opacity: filterContent.clientHeight === 0 ? 1 : 0,
      display: filterContent.clientHeight === 0 ? "block" : "none",
      duration: 0.2,
      ease: "power2.inOut",
    });
  }

  // Event listener for the filter toggle
  filterToggle.addEventListener("click", toggleFilterContent);
});
// Use GSAP to set the initial state of the filter menu
gsap.set("#filterMenu", { maxHeight: 0, overflow: "hidden" });

let isExpand = false;

function toggleFilterLg() {
  const filterMenu = document.getElementById("filterMenu");
  const lgFilterIcon = document.getElementById("lgFilterIcon");

  // Toggle the filter menu's visibility using GSAP
  gsap.to(filterMenu, {
    maxHeight:
      filterMenu.style.maxHeight === "0px"
        ? filterMenu.scrollHeight + "px"
        : "0px",
    duration: 0.3,
    ease: "power2.inOut",
  });

  // Toggle the filter icon animation
  toggleFilterIcon();
}

function toggleFilterIcon() {
  const lgFilterLine = document.querySelectorAll("#lgFilterIcon line");
  isExpand = !isExpand;

  gsap.to(lgFilterLine[0], {
    duration: 0.2,
    y: isExpand ? 20 : 0,
    transformOrigin: "center center ",
  });
  gsap.to(lgFilterLine[1], {
    duration: 0.2,
    rotation: isExpand ? 0 : 0,
    transformOrigin: " center center",
  });
  gsap.to(lgFilterLine[2], {
    duration: 0.2,
    rotation: isExpand ? 0 : 0,
    transformOrigin: "center center",
  });
  gsap.to(lgFilterLine[3], {
    duration: 0.2,
    y: isExpand ? 30 : 0,
    transformOrigin: "center",
  });
  gsap.to(lgFilterLine[4], {
    duration: 0.2,
    y: isExpand ? 20 : 0,
    transformOrigin: "top",
  });
  gsap.to(lgFilterLine[5], {
    duration: 0.2,
    y: isExpand ? 0 : 0,
    transformOrigin: "center center ",
  });
  gsap.to(lgFilterLine[6], {
    duration: 0.2,
    rotation: isExpand ? 90 : 0,
    transformOrigin: "center center",
  });
  gsap.to(lgFilterLine[7], {
    duration: 0.2,
    rotation: isExpand ? 90 : 0,
    transformOrigin: "center center",
  });
  gsap.to(lgFilterLine[8], {
    duration: 0.2,
    rotation: isExpand ? 90 : 0,
    transformOrigin: "bottom ",
  });
}

gsap.set('#sortDropdown', {
    display: 'none',
    maxHeight: 'none',
    overflow: 'hidden'
});

const sortDropdownTrigger = document.getElementById('sortDropdownTrigger');
const sortDropdown = document.getElementById('sortDropdown');

// GSAP function to toggle the visibility of the dropdown on click
function toggleSortDropdown() {
    const duration = 0.1;
    if(sortDropdownTrigger.classList.contains('font-normal')) {
    sortDropdownTrigger.classList.add('font-bold');
    sortDropdownTrigger.classList.remove('font-normal');
    } else {
        sortDropdownTrigger.classList.remove('font-bold');
    sortDropdownTrigger.classList.add('font-normal');
    }
    // Toggle the display and max height of the dropdown
    gsap.to(sortDropdown, {
        display: sortDropdown.style.display === 'none' ? 'block' : 'none',
        maxHeight: sortDropdown.style.maxHeight === 'none' ? 'auto' : 'none',
        duration,
        ease: 'power2.inOut',
    });
}

// Event listener for click on trigger
sortDropdownTrigger.addEventListener('click', toggleSortDropdown);

// Event listener for click outside of the dropdown
document.addEventListener('click', function(event) {
    // Check if the click occurred outside of the dropdown and trigger
    if (!sortDropdown.contains(event.target) && event.target !== sortDropdownTrigger) {
        // Close the dropdown if it's open
        if (sortDropdown.style.display === 'block') {
            toggleSortDropdown();
        }
    }
});
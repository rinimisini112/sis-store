document.addEventListener("DOMContentLoaded", function () {
  const track = document.getElementById("track");
  const items = document.querySelectorAll(".carousel-item");
  const itemWidth = items[0].offsetWidth;
  const cloneCount = 3; // Adjust the number of clones as needed

  for (let i = 0; i < cloneCount; i++) {
    items.forEach((item) => {
      const clone = item.cloneNode(true);
      track.appendChild(clone);
    });
  }

  let currentIndex = 0;

  function updateCarousel() {
    track.style.transform = `translateX(-${currentIndex * itemWidth}px)`;
  }

  function nextSlide() {
    currentIndex++;
    updateCarousel();
  }

  function prevSlide() {
    currentIndex--;
    updateCarousel();
  }

  setInterval(nextSlide, 4000);
});

document.addEventListener("DOMContentLoaded", function () {
  const navbar = document.getElementById("navbar");
  const banner_one = document.getElementById("banner1");
  const banner_two = document.getElementById("banner2");
  const white_banner = document.getElementById("white_banner");
  const scrollPosition = localStorage.getItem("scrollPosition");
  const endTextQuote = document.getElementById("instaQuote");
  const phoneMenuIcon = document.getElementById("openPhoneMenuIcon");
  let lines = document.querySelectorAll(".burger_line");
  const whatsappSVG = document.getElementById("whatsappSVG");

  let phoneMenu = document.getElementById("phoneMenu");
  let phoneMenuChildren = document.querySelectorAll("#phoneMenu ul li");
  let isOpen = false;

  function togglePhoneMenu() {
    const duration = 0.2; // Adjust the duration as needed

    if (!isOpen) {
      // Using GSAP to animate the opening of the menu
      gsap.to(phoneMenu, {
        duration,
        maxWidth: "100%",
        onComplete: () => {
          phoneMenu.classList.add("duration-200");

          // Animate each menu item to become visible
        },
      });
    } else {
      // Using GSAP to animate the closing of the menu
      gsap.to(phoneMenu, {
        duration,
        maxWidth: 0,
        onComplete: () => {},
      });
    }

    isOpen = !isOpen;
  }

  phoneMenuIcon.addEventListener("click", togglePhoneMenu);
  let open = false;

  function toggleMenu() {
    const menuItems = document.querySelectorAll("#phoneMenu > *");
    const listMenuItems = document.querySelectorAll("#phoneMenu > ul > *");
    let lines = document.querySelectorAll(".burger_line");

    gsap.to(lines[0], { duration: 0.05, rotate: open ? 0 : 45 });
    gsap.to(lines[1], {
      duration: 0.2,
      x: open ? 0 : 45,
      opacity: open ? 1 : 5,
    });
    gsap.to(lines[2], { duration: 0.05, rotate: open ? 0 : -45 });
    open = !open;
    gsap.fromTo(
      menuItems,
      { opacity: 0, x: -30 },
      { opacity: 1, x: 0, duration: 0.3, stagger: 0.2 }
    );
    gsap.fromTo(
      listMenuItems,
      { opacity: 0, x: -30 },
      { opacity: 1, x: 0, duration: 0.2, stagger: 0.1 }
    );
  }
  document
    .getElementById("openPhoneMenuIcon")
    .addEventListener("click", toggleMenu);

  window.onclick = function (event) {
    let phoneMenu = document.getElementById("phoneMenu");
    let button = document.getElementById("openPhoneMenuIcon");

    if (
      !phoneMenu.contains(event.target) &&
      !button.contains(event.target) &&
      open
    ) {
      togglePhoneMenu();
      toggleMenu();
    } else if (
      !collectionsDropdown.contains(event.target) &&
      isCollectionsVisible()
    ) {
      toggleCollections();
    }
  };
  const setNavbarStyles = () => {
    if (window.scrollY >= white_banner.offsetTop) {
      navbar.style.backgroundColor = "#E3E3E3ff";
      phoneMenu.style.backgroundColor = "#E3E3E3ff";
      collectionsDropdown.style.backgroundColor = "#E3E3E3ff";
      collectionsDropdownChildren.forEach(function (child) {
        child.style.borderColor = "#1e1e1e";
        child.classList.remove("hover:bg-[#1e1e1ebc]");
        child.classList.add("hover:bg-[rgb(216,216,216)]");
        child.classList.remove("hover:bg-[#303030bd]");
      });
      phoneMenuChildren.forEach(function (phoneChild) {
        phoneChild.style.borderColor = "#1e1e1e";
      });

      navbar.style.color = "#1e1e1e";
      collectionsDropdown.style.color = "#1e1e1e";
    } else if (window.scrollY > banner_one.offsetTop) {
      navbar.style.backgroundColor = "#1e1e1e";
      phoneMenu.style.backgroundColor = "#1e1e1e";
      collectionsDropdown.style.backgroundColor = "#1e1e1e";
      collectionsDropdownChildren.forEach(function (child) {
        child.style.borderColor = "#E3E3E3ff";
        child.classList.remove("hover:bg-[rgb(216,216,216)]");
        child.classList.add("hover:bg-[#303030bd]");
        child.classList.remove("hover:bg-[#1e1e1ebc]");
      });
      phoneMenuChildren.forEach(function (phoneChild) {
        phoneChild.style.borderColor = "#E3E3E3ff";
      });
      navbar.style.color = "#E3E3E3ff";
      collectionsDropdown.style.color = "#E3E3E3ff";
    } else {
      navbar.style.backgroundColor = "rgba(30, 30, 30, 0.6)";
      phoneMenu.style.backgroundColor = "rgba(30, 30, 30, 0.9)";
      collectionsDropdownChildren.forEach(function (child) {
        child.classList.remove("hover:bg-[rgb(216,216,216)]");
        child.classList.add("hover:bg-[#1e1e1ebc]");
        child.classList.remove("hover:bg-[#303030bd]");
      });
      phoneMenuChildren.forEach(function (phoneChild) {
        phoneChild.style.borderColor = "#E3E3E3ff";
      });
      collectionsDropdown.style.backgroundColor = "rgba(30, 30, 30, 0.6)";
      navbar.style.color = "#E3E3E3ff";
      collectionsDropdown.style.color = "#E3E3E3ff";
    }
  };

  // Apply styles on initial load based on stored scroll position
  if (scrollPosition) {
    window.scrollTo(0, parseInt(scrollPosition, 10));
    setNavbarStyles();
  }

  window.addEventListener("scroll", setNavbarStyles);

  const navbarTimeline = gsap.timeline({
    scrollTrigger: {
      trigger: banner_one,
      start: "top",
      end: "+=100%",
      scrub: true,
    },
  });
  gsap.set(navbar, {
    backgroundColor: "rgba(30, 30, 30, 0.8)",
    color: "#E3E3E3ff",
  });

  navbarTimeline.to(navbar, {
    backgroundColor: "#1e1e1e",
    color: "#E3E3E3ff",
  });
  const setNavbarBoxShadow = (boxShadow) => {
    gsap.to(navbar, { boxShadow, duration: 0.3 });
  };
  ScrollTrigger.create({
    trigger: navbar,
    start: "bottom 5%",
    end: "bottom", // Adjust the end position as needed
    onEnter: () => setNavbarBoxShadow("0 4px 4px rgba(50, 50, 50, 0.2)"),
    onLeaveBack: () => setNavbarBoxShadow("none"), // Reset box shadow when scrolling leaves the section
  });
  const sections = document.querySelectorAll("section");

  sections.forEach((section, index) => {
    gsap.to(section, {
      scrollTrigger: {
        trigger: banner_two,
        start: "bottom 10%",
        end: "bottom",
        onEnter: () => {
          navbar.classList.add("opacity-0");
        },
        onLeaveBack: () => {
          navbar.classList.remove("opacity-0");
        },
      },
    });
    gsap.to(navbar, {
      scrollTrigger: {
        trigger: endTextQuote,
        start: "bottom ",
        end: "bottom",
        onEnter: () => {
          navbar.classList.add("opacity-0");
          phoneMenu.classList.add("opacity-0");
        },
        onLeaveBack: () => {
          phoneMenu.classList.remove("opacity-0");
          navbar.classList.remove("opacity-0");
        },
      },
    });
    // Add a point where the navbar color changes at the end of the parallax section
    gsap.to(navbar, {
      scrollTrigger: {
        trigger: white_banner,
        start: "top  ",
        end: "bottom 0%",
        onEnter: () => {
          navbar.classList.remove("opacity-0");
          navbar.style.backgroundColor = "#E3E3E3ff";
          navbar.style.color = "#1e1e1e";
          phoneMenu.classList.remove("bg-sis-grey");
          phoneMenu.classList.remove("text-sis-white");
          phoneMenu.classList.add("bg-sis-white");
          phoneMenu.classList.add("text-sis-grey");
          lines.forEach((line) => {
            line.classList.remove("bg-sis-white");
            line.classList.add("bg-sis-grey");
          });
        },
        onLeaveBack: () => {
          navbar.classList.add("opacity-0");
          navbar.style.backgroundColor = "#1e1e1e";
          navbar.style.color = "#E3E3E3ff";
          phoneMenu.classList.add("bg-sis-grey");
          phoneMenu.classList.add("text-sis-white");
          phoneMenu.classList.remove("bg-sis-white");
          phoneMenu.classList.remove("text-sis-grey");
          lines.forEach((line) => {
            line.classList.add("bg-sis-white");
            line.classList.remove("bg-sis-grey");
          });
        },
      },
    });
  });

  // Save the scroll position to local storage when the user leaves or refreshes the page
  window.addEventListener("beforeunload", () => {
    localStorage.setItem("scrollPosition", window.scrollY.toString());
  });
});
var collectionsDropdown = document.getElementById("collectionsDropdown");
var collectionsDropdownChildren = document.querySelectorAll(
  "#collectionsDropdown ul li"
);
var chevronIcon = document.getElementById("chevronIcon");
var collectionsParagraph = document.getElementById("toggleCollections");

function isCollectionsVisible() {
  return parseFloat(window.getComputedStyle(collectionsDropdown).opacity) > 0;
}

function toggleCollections() {
  var isVisible = isCollectionsVisible();

  gsap.to(collectionsDropdown, {
    opacity: isVisible ? 0 : 1,
    zIndex: isVisible ? -50 : 20,
    visibility: isVisible ? "hidden" : "visible",
    duration: 0.2,
    onComplete: function () {
      if (isVisible) {
        gsap.set(collectionsDropdown, { visibility: "hidden" });
      }
    },
  });
  gsap.to(chevronIcon, {
    rotation: isVisible ? 0 : 180,
    duration: 0.2,
    x: isVisible ? 5 : 10,
  });
}

// Toggle collections on click
collectionsParagraph.addEventListener("click", toggleCollections);
var isDropdownVisible = false;

var collectionsLi = document.getElementById('collectionsLi');
var collectionsPhoneDropdown = document.getElementById('collectionsPhoneDropdown');
var collectionsPhoneIcon = document.getElementById('phoneCollectionsIcon');
collectionsLi.addEventListener('click', function() {
  isDropdownVisible = !isDropdownVisible;

  gsap.to(collectionsPhoneDropdown, {
    maxHeight: isDropdownVisible ? collectionsPhoneDropdown.scrollHeight + 'px' : 0,
    ease: 'power2.inOut',
    duration: 0.2,
    onComplete: function() {
      if (!isDropdownVisible) {
        collectionsPhoneDropdown.style.overflow = 'hidden';
      } 
    }
  });
  gsap.to(collectionsPhoneIcon, {
    rotation: isDropdownVisible ? 180 : 0,
    duration: 0.3
  })
});
<?php
include 'public/Components/htmlHead.php';
session_start();
?>
<body class=" bg-sis-grey">
    <header class="bg-sis-grey">
        <?php 
        include 'public/Components/navbar.php';
        include 'public/Components/firstBanner.php';
        include 'public/Components/secondBanner.php';
        ?>
    </header>
    <section>
        <div class="w-full md:h-[105vh] h-[110vh]" id="parallax"></div>
    </section>
    <article class="w-full bg-sis-white">
       <?php 
       include "public/Components/whiteBanner.php";
       include "public/Components/whiteCarousel.php";
       ?>
        
    </article>
    <div id="instaQuote" class=" bg-sis-white flex flex-col gap-16 items-center justify-center py-24 relative">
        <h2 class=" md:-ml-32 -ml-0 md:text-6xl text-3xl"><a href="https://www.instagram.com/sis.store.pr/" target="_blank">@<span class="rische">SIS</span>st<span class="rische md:text-[3.3rem] text-[1.55rem]">o</span>re</a> on Instagram</h2>
        <p class=" md:ml-32 ml-0 md:mx-0 mx-4 md:text-4xl text-xl">#ElevateYourShoppingExperience</p>
    </div>
   <?php 
   include "public/Components/footer.php";
    ?>
    <script src="dist/javascript/gsapAnimations.js"></script>
</body>

</html>
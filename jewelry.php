<?php
include "public/collections/c_components/head.php";
include 'public/jewelriesControllers/singleJewelry.php';
$upperCaseMaterial = ucfirst($item->material);
?>
<head>
    <style>
        #paralaxBannerJewelry {
            background-image: url(resources/images/imageedit_3_2465562673.gif);
            background-attachment: fixed;
        }
    </style>
</head>
<body class="bg-sis-lemon">
    <header>
        <nav id="navbar" class="bg-sis-lemon duration-200 z-30 text-sis-blue  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="index.php" class=" relative z-50">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-blue h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-blue h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-blue h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <div class="fixed top-0 left-0 w-[80%] max-w-0 h-full flex-col flex justify-between bg-sis-lemon text-sis-blue z-40 transition-all duration-100 overflow-hidden pt-16" id="phoneMenu">
                <ul class=" text-3xl pb-6 mt-4 transition-colors duration-300">
                    <li id="collectionsLi" class="py-4 pl-3 border-b-2 border-t-2 border-sis-sandy relative"><span class="flex items-center gap-8">Collections
                            <svg id='phoneCollectionsIcon' xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                                <polyline points="6 9 12 15 18 9"></polyline>
                            </svg>
                        </span>
                        <div id="collectionsPhoneDropdown" style="max-height: 0; overflow: hidden; transition: max-height 0.3s ease-out;">
                            <ul class="w-[95%] pt-4">
                                <li class="py-3 pl-3 border-b border-b-sis-sandy"><a href="collections.php">New Collections</a></li>
                                <li class="py-3 pl-3 border-b border-b-sis-sandy"><a href="vintageCollection.php">Vintage Collection</a></li>
                                <li class="py-3 pl-3"><a href="jewelries.php">Jewelry</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-sandy"><a href="aboutUs.php">About Us</a></li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-sandy"><a href="contactUs.php">Contact Us</a></li>
                </ul>
                <ul class=" p-2 md:text-2xl text-xl text-center w-full">
                    <li class="flex flex-col gap-2 items-center justify-between border-b-2 border-sis-grey pb-2">
                        <span class="text-2xl">
                            Mobile
                        </span> <a href="tel:+38343555450">043 555 450
                        </a>
                    </li>
                    <li class="flex flex-col gap-3 mt-2 items-center justify-between">
                        <span class="text-2xl">
                            Whatsapp </span> <a href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
                <li class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </p>
                    <div class="absolute shadow-lg left-0 w-full bg-sis-lemon" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:bg-[#c4ced5] hover:text-sis-blue duration-200 py-4 pl-2 border-b border-b-sis-sandy">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:bg-[#c4ced5] hover:text-sis-blue duration-200 py-4 pl-2 border-b border-b-sis-sandy">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:bg-[#c4ced5] hover:text-sis-blue duration-200 py-4 pl-2">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </a>
                </li>
            </ul>
        </nav>
        <div id="filler" class="duration-200"></div>
    </header>
    <main class="w-full h-auto">
        <section>
            <div class="w-full md:px-6 px-2 pt-3 pb-1 flex items-center justify-between text-xl sticky top-0 bg-sis-sandy text-sis-blue shadow-lg z-20 left-0">
                <div class="flex md:flex-row flex-col items-center">
                    <div class=" flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-down-left cursor-pointer">
                            <polyline points="9 10 4 15 9 20"></polyline>
                            <path d="M20 4v7a4 4 0 0 1-4 4H4"></path>
                        </svg>

                        <p class="pr-2"><a href='javascript:history.back()'>Back</a></p>
                        </p> / <a class="px-2 font-bold flex" href="jewelries.php">All</a> / <a href="jewelryCategories.php?cid=<?= $item->category_id ?>" class="px-2 font-bold"><?= $item->category_name ?></a>
                        /
                    </div>
                    <span class=" font-bold md:pl-2 pl-8 lg:py-0 py-2 cursor-pointer block w-full"><?= $item->name ?></span>
                </div>

                <?php
                $redirectToPrevious = true;

                if ($redirectToPrevious) {
                    echo "<a class='md:inline hidden bg-sis-blue border border-sis-blue text-sis-sandy py-1.5 md:px-12 px-4 font-semibold hover:text-sis-blue hover:bg-sis-lemon duration-300' href='javascript:history.back()'><span class='md:inline hidden'>Go</span> Back</a>";
                } else {
                    echo '<a href="collections.php">Go Back</a>';
                }
                ?>
            </div>
            <div class="flex lg:flex-row flex-col-reverse w-full relative">
                <div id="paralaxBannerJewelry" class="lg:w-[55%] w-full  lg:px-0 px-4 h-auto mx-auto flex  flex-wrap relative">
                    <div class=" lg:w-[80%] w-full mx-auto">
                        <img src="resources/images/<?= $item->main_image ?>" alt="" class="pb-3 object-cover">
                        <?php
                        if (isset($item->image1)) {
                            echo "<img src='resources/images/" . $item->image1 . "' alt='Image of our store's clothes' class='pb-3 object-cover'>";
                        }
                        if (isset($item->image2)) {
                            echo "<img src='resources/images/" . $item->image2 . "' alt='Image of our store's clothes' class='pb-3 object-cover'>";
                        }
                        if (isset($item->image3)) {
                            echo "<img src='resources/images/" . $item->image3 . "' alt='Image of our store's clothes' class='pb-3 object-cover'>";
                        }
                        if (isset($item->image4)) {
                            echo "<img src='resources/images/" . $item->image4 . "' alt='Image of our store's clothes' class='pb-3 object-cover'>";
                        }
                        if (isset($item->image5)) {
                            echo "<img src='resources/images/" . $item->image5 . "' alt='Image of our store's clothes' class='pb-3 object-cover'>";
                        }
                        if (isset($item->image6)) {
                            echo "<img src='resources/images/" . $item->image6 . "' alt='Image of our store's clothes' class='pb-3 object-cover'>";
                        }
                        ?>
                    </div>
                </div>
                <div class="lg:w-[45%] bg-sis-yellow shadow-xl text-sis-blue w-[90%] mx-auto h-auto relative lg:pb-0 pb-4">
                <div class="sticky top-14  flex items-start justify-center flex-col">
                        <h1 class="text-5xl shadow-md bg-sis-blue text-sis-sandy w-full py-6 rische text-center font-bold"><?= $item->name ?></h1>
                          <p class="text-2xl pb-16 text-center pt-4 pl-8"><?= $item->description ?></p>
                        <p class="text-2xl pb-4 pl-8">Price - <?= $item->price ?>€</p>
                        <p class="text-xl pl-8">Material : <?= $upperCaseMaterial ?></p>
                        <p class="text-xl pl-8">Color : <?= $item->color ?></p>
                        <p class=" pt-20 lg:px-10 px-4 text-lg"><b>Disclaimer!</b> Due to the business being fairly new, We currently are not accepting orders from our website.
                            You can order on our social media's <a class="font-bold" href="https://www.instagram.com/sis.store.pr/">@sis.store.pr</a> on Instagram or at our Physical store located at Rezarta Galica Beauty Pro's.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <script>
        const phoneMenuIcon = document.getElementById("openPhoneMenuIcon");
        const navbar = document.getElementById("navbar");
        const footer = document.getElementById('footer');

        var phoneMenu = document.getElementById('phoneMenu');
        var isOpen = false;
        var filterMenuIsOpen = false;

        function togglePhoneMenu() {
            const duration = 0.2; // Adjust the duration as needed

            if (!isOpen) {
                if (filterMenuIsOpen) {
                    toggleFilterMenu();
                    filterMenuIsOpen = false;
                }
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration,
                    maxWidth: '100%',
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                        navbar.classList.add('fixed');
                        phoneMenuIsOpen = true;
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration,
                    maxWidth: 0,
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                        navbar.classList.remove('fixed');
                        phoneMenuIsOpen = false;
                    },
                });
            }

            isOpen = !isOpen;
        }

        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        var open = false;

        function toggleMenu() {
            var menuItems = document.querySelectorAll("#phoneMenu > *");
            var listMenuItems = document.querySelectorAll("#phoneMenu > ul > *");
            var lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
            gsap.fromTo(menuItems, {
                opacity: 0,
                x: -30
            }, {
                opacity: 1,
                x: 0,
                duration: 0.3,
                stagger: 0.2
            });
            gsap.fromTo(listMenuItems, {
                opacity: 0,
                x: -30
            }, {
                opacity: 1,
                x: 0,
                duration: 0.2,
                stagger: 0.1
            });
        }
        document.getElementById('openPhoneMenuIcon').addEventListener('click', toggleMenu);


        var collectionsDropdown = document.getElementById('collectionsDropdown');
        var chevronIcon = document.getElementById('chevronIcon');
        var collectionsParagraph = document.getElementById('toggleCollections');

        function isCollectionsVisible() {
            return parseFloat(window.getComputedStyle(collectionsDropdown).opacity) > 0;
        }

        function toggleCollections() {
            var isVisible = isCollectionsVisible();

            gsap.to(collectionsDropdown, {
                opacity: isVisible ? 0 : 1,
                zIndex: isVisible ? -50 : 30,
                visibility: isVisible ? 'hidden' : 'visible',
                duration: 0.2,
                onComplete: function() {
                    if (isVisible) {
                        gsap.set(collectionsDropdown, {
                            visibility: 'hidden'
                        });
                    }
                },
            });
            gsap.to(chevronIcon, {
                rotation: isVisible ? 0 : 180,
                duration: 0.2,
                x: isVisible ? 5 : 10
            });
        }

        // Toggle collections on click
        collectionsParagraph.addEventListener('click', toggleCollections);

        // Close collections dropdown when clicking outside

        var isDropdownVisible = false;

        var collectionsLi = document.getElementById('collectionsLi');
        var collectionsPhoneDropdown = document.getElementById('collectionsPhoneDropdown');
        var collectionsPhoneIcon = document.getElementById('phoneCollectionsIcon');
        collectionsLi.addEventListener('click', function() {
            isDropdownVisible = !isDropdownVisible;

            gsap.to(collectionsPhoneDropdown, {
                maxHeight: isDropdownVisible ? collectionsPhoneDropdown.scrollHeight + 'px' : 0,
                ease: 'power2.inOut',
                duration: 0.2,
                onComplete: function() {
                    if (!isDropdownVisible) {
                        collectionsPhoneDropdown.style.overflow = 'hidden';
                    }
                }
            });
            gsap.to(collectionsPhoneIcon, {
                rotation: isDropdownVisible ? 180 : 0,
                duration: 0.3
            })
        });
        window.onclick = function(event) {
            var button = document.getElementById('openPhoneMenuIcon');

            if (open && !button.contains(event.target) && !phoneMenu.contains(event.target)) {
                togglePhoneMenu();
                toggleMenu();
            } else if (isCollectionsVisible() && !collectionsDropdown.contains(event.target)) {
                toggleCollections();
            }
        }
    </script>
</body>

</html>
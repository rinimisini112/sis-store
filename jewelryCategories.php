<?php
include 'public/Components/htmlHead.php';
include "public/jewelriesControllers/jewelryCategoryHandler.php";
require 'dist/pagination/Zebra_Pagination.php'; // Include Composer autoloader
if(isset($_SESSION['filterJewelryResults'])) {
unset($_SESSION['filterJewelryResults']);
unset($_SESSION['jewelryPrice']);
unset($_SESSION['selectedMaterials']);
}
$pagination = new Zebra_Pagination();
// Set the total number of records
$totalRecords = count($jewelries);
$pagination->records($totalRecords);
// Set records per page
$recordsPerPage = 9;  // You can change this value to adjust the number of products per page
$pagination->records_per_page($recordsPerPage);

// Calculate start index for data
$startIndex = ($pagination->get_page() - 1) * $recordsPerPage;
$itemsSubset = array_slice($jewelries, $startIndex, $recordsPerPage);

?>

<head>
    <style>
        .moving-text-container {
            overflow: hidden;
            position: relative;
        }

        .moving-text {
            white-space: nowrap;
            position: absolute;
            top: 15px;
            left: 0;
        }
        .Zebra_Pagination {
    padding-bottom: 30px;
    clear: both;
    width: 100%;
    overflow: hidden;
}

.Zebra_Pagination ol {
    position: relative;
    left: 50%;
    list-style-type: none;
    margin: 0;
    padding: 0;
    float: left
}

.Zebra_Pagination li {
    position: relative;
    float: left;
    right: 50%
}

.Zebra_Pagination .pagination {
    display: inline-block;
}

.Zebra_Pagination li {
    display: inline;
}

.Zebra_Pagination a,
.Zebra_Pagination span {
    font-family: 'angle-font',sans-serif;
    font-size: large;
    font-weight: 700;
    padding: 8px 14px;
    color: #B49594;
    text-decoration: none;
    background-color: #051923;
    border: 1px solid #051923;
    display: block;
    float: left;
    position: relative;
    margin-left: -1px;
    transition: 0.3s ease;
}

.Zebra_Pagination li.active a {
    color: #B49594;
    cursor: default;
    background-color: #051923;
    border-color: #1e1e1e;
}

.Zebra_Pagination li a:hover,
.Zebra_Pagination li span:hover {
    color: #B49594;
    background-color: #051923;
    border-color: #051923;
}
    </style>
</head>

<body class="bg-sis-lemon relative">
    <header>
        <nav id="navbar" class=" shadow-xl duration-200 z-30 text-sis-blue bg-sis-lemon flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="index.php" class=" relative z-50">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-blue h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-blue h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-blue h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <div class="fixed top-0 left-0 w-[80%] max-w-0 h-full flex-col flex justify-between shadow-md bg-sis-lemon text-sis-blue z-40 transition-all duration-100 overflow-hidden pt-16" id="phoneMenu">
                <ul class=" text-3xl pb-6 mt-4 transition-colors px-8 duration-300">
                    <li id="collectionsLi" class="py-4 pl-3 border-b-2 border-t-2 border-sis-sandy relative"><span class="flex items-center gap-8">Collections
                            <svg id='phoneCollectionsIcon' xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                                <polyline points="6 9 12 15 18 9"></polyline>
                            </svg>
                        </span>
                        <div id="collectionsPhoneDropdown" style="max-height: 0; overflow: hidden; transition: max-height 0.3s ease-out;">
                            <ul class="w-[95%] pt-4">
                                <li class="py-3 pl-3 border-b border-b-sis-sandy"><a href="collections.php">New Collections</a></li>
                                <li class="py-3 pl-3 border-b border-b-sis-sandy"><a href="vintageCollection.php">Vintage Collection</a></li>
                                <li class="py-3 pl-3"><a href="jewelries.php">Jewelry</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-sandy"><a href="aboutUs.php">About Us</a></li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-sandy"><a href="blog.php">Blog</a></li>
                    <li class="py-4 pl-3 border-b-2 border-b-sis-sandy"><a href="contactUs.php">Contact Us</a></li>
                </ul>
                <ul class=" p-2 md:text-2xl text-xl text-center w-full">
                    <li class="flex flex-col gap-2 items-center justify-between border-b-2 border-sis-sandy pb-2">
                        <span class="text-2xl">
                            Mobile
                        </span> <a href="tel:+38343555450">043 555 450
                        </a>
                    </li>
                    <li class="flex flex-col gap-3 mt-2 items-center justify-between">
                        <span class="text-2xl">
                            Whatsapp </span> <a href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
                <li class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </p>
                    <div class="absolute shadow-lg left-0 w-full z-50 bg-sis-lemon" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:bg-[#c4ced5] duration-200 py-4 pl-4 border-b border-b-sis-sandy">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:bg-[#c4ced5] duration-200 py-4 pl-4 border-b border-b-sis-sandy">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:bg-[#c4ced5] duration-200 py-4 pl-4">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="blog.php" class="relative">Blog
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-sandy"></span>
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <?php

    if (isset($_SESSION['catFilterMssgJewelry'])) {
        echo "<div id='vintageCollsNotif' class='fixed md:w-[35%] w-[90%] h-[150px] flex items-center justify-center z-30 bg-sis-sandy border border-sis-violet bg-opacity-60 shadow-2xl bottom-4 right-4'>";
        echo "<p class='text-center text-2xl font-extrabold px-4'>{$_SESSION['catFilterMssgJewelry']}</p>";
        echo "</div>";
    }
    ?>
    <a id="goUpBtn" href="#navbar" class=" duration-300">
        <div class="fixed bottom-6 left-6 md:w-16 w-14 h-14 md:h-16 lg:animate-bounce  text-xl font-bold bg-sis-blue duration-200 text-sis-yellow rounded-full z-40 flex flex-col items-center justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-up">
                <polyline points="17 11 12 6 7 11"></polyline>
                <polyline points="17 18 12 13 7 18"></polyline>
            </svg>
            <p>UP</p>
        </div>
    </a>
    <main class="w-full relative">

        <div class="py-6 w-full md:px-12 px-4 mx-auto relative text-sis-blue">
            <h1 class="text-5xl flex justify-between md:text-left text-center"><span class="rische md:flex items-center gap-4 hidden relative">Jewelries <img src="resources/images/logosvg.png" class="w-[100px] right-0" alt=""></span> <span class="rische">View our <a href="vintageCollection.php" class="rische pl-4 underline  decoration-sis-sandy underline-offset-4">New Collections</a></span></h1>
            <p class="pt-6 text-center md:hidden block text-3xl rische">SIS Jewelries</p>
            <p class="text-2xl text-center md:pt-8 pt-2 pb-4">"Style in Every Sparkle."</p>
        </div>
        <section>

            <div id="productNavMb" class="w-full shadow-sm shadow-black bg-sis-yellow text-sis-blue px-6 py-4 flex items-center justify-between lg:text-xl text-lg sticky -top-0.5 z-10 left-0">
                <div class="flex items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-down-right">
                        <polyline points="15 10 20 15 15 20"></polyline>
                        <path d="M4 4v7a4 4 0 0 0 4 4h12"></path>
                    </svg>
                    <p class="pr-2 pl-1 lg:text-xl text-lg"><a href='jewelries.php'>Back</a></p> / <a class="px-2 font-bold" href="jewelries.php">All</a> / <p class="pl-2 cursor-pointer font-bold"><?= $jewelryCategory->name ?></p>
                </div>
                <p id="lgFilterBtn" class="hidden lg:flex items-center gap-2 cursor-pointer font-normal" onclick="toggleFilterLg()">
                    <svg id="lgFilterIcon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders cursor-pointer">
                        <line x1="4" y1="21" x2="4" y2="14"></line>
                        <line x1="4" y1="10" x2="4" y2="3"></line>
                        <line x1="12" y1="21" x2="12" y2="12"></line>
                        <line x1="12" y1="8" x2="12" y2="3"></line>
                        <line x1="20" y1="21" x2="20" y2="16"></line>
                        <line x1="20" y1="12" x2="20" y2="3"></line>
                        <line x1="1" y1="14" x2="7" y2="14"></line>
                        <line x1="9" y1="8" x2="15" y2="8"></line>
                        <line x1="17" y1="16" x2="23" y2="16"></line>
                    </svg>
                    Filter
                </p>
                <div id="filterMenu" class=" absolute top-14 left-0 z-30 w-full bg-sis-yellow ">
                    <div class="grid justify-items-center overflow-hidden pt-2 w-full">
                        <form action="" method="post" class=" w-full flex justify-between items-center pb-16 px-16">
                            <div class="flex w-auto items-center h-auto justify-center gap-3">
                                <p class="text-xl pt-1">Price </p>
                                <div class="py-1 relative min-w-full">
                                    <input type="range" id="priceInput" name="priceRange" style="accent-color: #000000;" class="w-full h-1" min="0" max="200" step="3" value="<?php echo isset($_SESSION['jewelryCatPrice']) ? $_SESSION['jewelryCatPrice'] : 100; ?>" oninput="updatePriceRange(this.value)">
                                    <div id="tooltipLeft" class="absolute text-gray-800 -ml-1 bottom-0 left-0 -mb-6">$ 0</div>
                                    <div id="tooltipRight" class="absolute text-gray-800 -mr-1 bottom-0 right-0 -mb-6">$ 100</div>
                                </div>
                            </div>

                            <div class="flex items-center flex-col">
                                <p>Material :</p>
                                <div class="flex gap-3">
                                    <?php
                                    $materials = isset($_POST['material']) ? $_POST['material'] : [];
                                    $selectedMaterials = isset($_SESSION['selectedCatMaterials']) ? $_SESSION['selectedCatMaterials'] : [];

                                    $materialOptions = ['gold', 'silver', 'platinum', 'stainless Steel', 'pearl'];

                                    foreach ($materialOptions as $material) {
                                        $checked = in_array($material, $selectedMaterials) ? 'checked' : '';
                                    ?>
                                        <label class="shadow-md">
                                            <input class="accent-sis-violet" type="checkbox" name="material[]" value="<?php echo $material; ?>" <?php echo $checked; ?>>
                                            <?php echo ucfirst($material); ?>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                            <input type="hidden" name="sortVal" value="<?= isset($_GET['sort']) ? $_GET['sort'] : ''; ?>">
                            <div class="flex flex-col">
                                <input type="submit" value="Remove Filters" name="removefilter" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-violet hover:text-sis-white duration-300 cursor-pointer outline-sis-grey px-8 py-1.5 mt-4">
                                <input type="submit" value="Filter" name="filterJewelry" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-violet hover:text-sis-white duration-300 cursor-pointer outline-sis-grey px-8 py-1.5 mt-4">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="relative w-1/5">
                    <p class="hidden lg:block text-right cursor-pointer font-normal duration-200" id="sortDropdownTrigger">Sort
                        <b class="px-2">
                            [<span><?= $sortMessage  ?></span>]
                        </b>
                    <div id="sortDropdown" class="absolute w-full z-50 left-0 text-right text-sis-blue bg-sis-yellow shadow-xl ">
                        <a class="block text-xl duration-200 hover:bg-[#e0c6d3] font-bold pr-2 py-2 border-b border-b-sis-blue" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_desc">Sort by Price (High to Low)</a>
                        <a class="block text-xl duration-200 hover:bg-[#e0c6d3] font-bold pr-2 py-2 border-b border-b-sis-blue" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_asc">Sort by Price (Low to High)</a>
                        <a class="block text-xl duration-200 hover:bg-[#e0c6d3] font-bold pr-2 py-2 border-b border-b-sis-blue" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=newest">Sort by Newest</a>
                        <a class="block text-xl duration-200 hover:bg-[#e0c6d3] font-bold pr-2 py-2" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['filtered']) ? 'filtered=' . $_GET['filtered'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=oldest">Sort by Oldest</a>
                    </div>
                    </p>
                </div>

                <svg id="togglePhoneFilter" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders block lg:hidden cursor-pointer">
                    <line x1="4" y1="21" x2="4" y2="14"></line>
                    <line x1="4" y1="10" x2="4" y2="3"></line>
                    <line x1="12" y1="21" x2="12" y2="12"></line>
                    <line x1="12" y1="8" x2="12" y2="3"></line>
                    <line x1="20" y1="21" x2="20" y2="16"></line>
                    <line x1="20" y1="12" x2="20" y2="3"></line>
                    <line x1="1" y1="14" x2="7" y2="14"></line>
                    <line x1="9" y1="8" x2="15" y2="8"></line>
                    <line x1="17" y1="16" x2="23" y2="16"></line>
                </svg>
            </div>
            <div class="flex flex-col items-center w-full h-auto relative">
                <ul class="lg:flex w-full lg:text-2xl bg-sis-blue shadow-xl text-md items-center sticky z-[5] top-14 justify-evenly py-3 hidden">
                    <li class='py-1.5 font-semibold text-sis-yellow'>
                        <a href='jewelries.php' class='relative group rische'>
                            All
                            <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-[80%] bg-sis-yellow'></span>
                        </a>
                    </li>
                    <?php include "public/jewelriesControllers/clickedJewelryNavbar.php" ?>
                </ul>
                <div class="lg:w-full bg-sis-pink mt-16 w-full h-auto mx-auto gap-4 flex flex-wrap relative z-0">
                    <div class="lg:w-[90%] w-full mx-auto flex flex-wrap gap-6">
                        <?php
                        include "public/jewelriesControllers/fetchCategoriesJewelry.php";
                        echo $pagination->render();
                        ?>
                    </div>
                    <!-- ============== The Full screen Banner ============= -->
                    <div class="moving-text-container bg-sis-blue text-sis-sandy w-full py-8 text-3xl">
                        <div class="moving-text" id="movingText">
                            <p class="rische">#Elevate your shopping experience. #Elevate your shopping experience. #Elevate your shopping experience.</p>
                        </div>
                    </div>
                    <!-- ============== Preferences part ================= -->
                    <?php include "public/jewelriesControllers/preferences.php"?>
                <!------------------- mobile product menu ------------------>
                
        </section>
    </main>
    <div class="lg:w-[25%] lg:hidden block bg-sis-yellow h-auto w-0 lg:overflow-visible shadow-2xl overflow-hidden lg:z-0 z-50 ">
                    <ul id="categoryList" class="lg:sticky shadow-md lg:h-auto h-screen lg:z-0 text-sis-blue bg-sis-yellow z-50 lg:top-12 text-center lg:w-full w-0 py-4 lg:overflow-y-scroll duration-300 fixed top-0 left-0 overflow-hidden">
                        <h1 class="text-3xl pb-6 angle-font font-semibold hidden lg:block">Categories</h1>
                        <h1 id='categoriesToggle' class="text-3xl cursor-poiner pb-4 font-bold lg:hidden block">Categories</h1>
                        <div id='categoriesPhoneMenu' class="lg:block grid-cols-3 lg:pb-0 pb-2 lg:px-0 px-2">
                            <li class='py-1.5 col-span-1 lg:text-xl text-lg font-bold'>
                                <a href='#' class='relative group rische'>
                                    All <span class=' absolute bottom-0 left-0 duration-200 w-full h-px group-hover:w-full bg-sis-grey'></span>
                                </a>
                            </li>

                            <?php
                            include "public/jewelriesControllers/clickedJewelryCateogories.php";
                            ?>
                        </div>
                        <div class=" grid h-auto w-full justify-items-center pt-4 pb-4 lg:border-none border-t-2 border-b-2 border-sis-grey">
                            <p class="lg:hidden flex items-center gap-2 text-4xl font-bold filter-toggle cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders cursor-pointer">
                                    <line x1="4" y1="21" x2="4" y2="14"></line>
                                    <line x1="4" y1="10" x2="4" y2="3"></line>
                                    <line x1="12" y1="21" x2="12" y2="12"></line>
                                    <line x1="12" y1="8" x2="12" y2="3"></line>
                                    <line x1="20" y1="21" x2="20" y2="16"></line>
                                    <line x1="20" y1="12" x2="20" y2="3"></line>
                                    <line x1="1" y1="14" x2="7" y2="14"></line>
                                    <line x1="9" y1="8" x2="15" y2="8"></line>
                                    <line x1="17" y1="16" x2="23" y2="16"></line>
                                </svg>
                                Filter
                            </p>
                            <div class=" overflow-hidden pt-2 px-2 w-full filter-content">
                                <form action="" method="post" class="mx-auto w-[95%]">
                                    <p class="text-xl pt-1">Price :</p>
                                    <div class="flex w-full m-auto items-center h-auto justify-center">
                                        <div class="py-1 relative min-w-full">
                                            <input type="range" id="priceInput" name="priceRange" style="accent-color: #000000;" class="w-full h-1" min="0" max="200" step="3" value="<?php echo isset($_SESSION['jewelryCatPrice']) ? $_SESSION['jewelryCatPrice'] : 100; ?>" oninput="updatePriceRange(this.value)">
                                            <div id="tooltipLeft" class="absolute text-gray-800 -ml-1 bottom-0 left-0 -mb-6">$ 0</div>
                                            <div id="phoneTooltipRight" class="absolute text-gray-800 -mr-1 bottom-0 right-0 -mb-6">$ 100</div>
                                        </div>
                                    </div>
                                    <div class="my-6">
                                    <p>Material :</p>
                                <div class="flex gap-3">
                                    <?php
                                    $materials = isset($_POST['material']) ? $_POST['material'] : [];
                                    $selectedMaterials = isset($_SESSION['selectedCatMaterials']) ? $_SESSION['selectedCatMaterials'] : [];

                                    $materialOptions = ['gold', 'silver', 'platinum', 'stainless Steel', 'pearl'];

                                    foreach ($materialOptions as $material) {
                                        $checked = in_array($material, $selectedMaterials) ? 'checked' : '';
                                    ?>
                                        <label class="shadow-md">
                                            <input class="accent-sis-violet" type="checkbox" name="material[]" value="<?php echo $material; ?>" <?php echo $checked; ?>>
                                            <?php echo ucfirst($material); ?>
                                        </label>
                                    <?php } ?>
                                </div>
                                    </div>
                                    <div class="flex items-center w-full justify-center gap-2 mb-4">
                                        <input type="submit" value="Remove Filters" name="removefilter" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey w-[45%] py-1.5 mt-4">
                                        <input type="submit" value="Filter" name="filterJewelry" class="bg-sis-white  text-sis-grey outline-1 outline hover:bg-sis-grey hover:text-sis-white duration-300 cursor-pointer outline-sis-grey w-[45%] py-1.5 mt-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="lg:hidden grid h-auto justify-items-center pt-6 pb-4 border-b-2 border-sis-grey">
                            <p class=" flex items-center gap-2 text-4xl font-bold sort-toggle cursor-pointer">
                                Sort
                                <b class="px-2 text-2xl">
                                    [<span><?= $sortMessage  ?></span>]
                                </b>
                            </p>
                            <div class="flex flex-col overflow-hidden pt-4 pb-2 w-full sort-content">
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_desc">Sort by Price (High to Low)</a>
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=price_asc">Sort by Price (Low to High)</a>
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=newest">Sort by Newest</a>
                                <a class="block text-xl font-bold my-1 underline" href="?<?= isset($_GET['cid']) ? 'cid=' . $_GET['cid'] . "&" : '' ?><?= isset($_GET['page']) ? 'page=' . $_GET['page'] . "&" : '' ?>sort=oldest">Sort by Oldest</a>

                            </div>
                        </div>
                    </ul>

                </div>
    <?php include "public/Components/footer.php" ?>
    <script src="dist/javascript/collectionsPhoneMenu.js"></script>
    <script src="dist/javascript/filterSort.js"></script>
    <script>
        const movingText = document.getElementById('movingText');

        // Calculate the duration based on the text length and container width
        const duration = (movingText.clientWidth / window.innerWidth) * 15;

        gsap.fromTo(movingText, {
            x: window.innerWidth
        }, {
            x: -movingText.clientWidth,
            duration: duration,
            ease: 'linear',
            repeat: -1,
            onComplete: () => {
                gsap.set(movingText, {
                    x: window.innerWidth
                });
            }
        });
        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#vintageCollsNotif').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 4000); // 5000 milliseconds = 5 seconds
        });
    </script>
</body>

</html>
<?php
session_start();
include 'public/Components/htmlHead.php';
include 'public/Classes/Post.php';

if(isset($_GET['nr'])) {
    $postId = $_GET['nr'];
    $post = new Post();
    $post = $post->fetchPostWithId($postId);
    $timestamp = strtotime($post->published_at);

    // Get month and year separately using DateTime
    $month = date('F', $timestamp); 
    $month = strtoupper($month);
    $year = date('Y', $timestamp);
} else {
    header ('blog.php');
}
?>

<body class="bg-neutral-200">
<nav id="navbar" style=" text-shadow: 0 0 2px gray;" class="bg-sis-white duration-200 z-40 fixed top-0 left-0 text-sis-grey  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center w-full">
            <a href="index.php" class=" relative z-[100]">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class='block lg:hidden' id='openPhoneMenuIcon'>
                <button class='relative group z-50'>
                    <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                        <div class='burger_line bg-sis-grey h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
                    </div>
                </button>
            </div>
            <?php
            include "public/Components/phoneMenu.php";

            ?>
            <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
                <li class="px-8 w-[30%] relative group">
                    <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </p>
                    <div class="absolute shadow-lg left-0 w-full" id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                        <ul class="w-full">
                            <a href="collections.php">
                                <li class="hover:backdrop-blur-md backdrop-blur-sm filter hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">New Collections</li>
                            </a>
                            <a href="vintageCollection.php">
                                <li class="hover:backdrop-blur-md backdrop-blur-sm filter hover:text-sis-grey duration-200 py-4 pl-4 border-b border-b-sis-grey">Vintage Collection</li>
                            </a>
                            <a href="jewelries.php">
                                <li class="hover:backdrop-blur-md backdrop-blur-sm filter hover:text-sis-grey duration-200 py-4 pl-4">Jewelry</li>
                            </a>
                        </ul>
                    </div>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="aboutUs.php" class="relative">About Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="blog.php" class="relative">Blog
                        <span class="duration-200 w-full h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
                <li class="pl-8 w-[30%] relative group">
                    <a href="contactUs.php" class="relative">Contact Us
                        <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-grey"></span>
                    </a>
                </li>
            </ul>
        </nav>
    <header class="h-[500px] flex items-center justify-center" id="blogHeader">
            <h1 class="rische lg:text-9xl text-5xl relative opacity-90 text-sis-white font-extrabold z-30">SlS <span>st<span class="rische lg:text-[6.8rem] text-[2.6rem]">o</span>re</span></h1>
       
    </header>
    <main class="lg:w-[75%] md:w-[75%] w-[90%] mx-auto lg:pb-16 pb-28 bg-[#f2ede6] shadow-lg">
        <div class="flex flex-col w-full">
            <div class="w-[90%] shadow-xl shadow-[#00000077] lg:h-[450px] h-[250px] mx-auto object-cover transform -translate-y-8">
                <img src="resources\images\<?= $post->main_image; ?>" alt="" class="w-full h-full object-cover">
            </div>
        </div>
        <div class=" w-full px-4 md:px-8  lg:pr-6">
            <div class="flex w-[90%] mx-auto lg:pb-2 pb-0 justify-between items-center">
                <p class="stink lg:text-5xl text-3xl text-neutral-400"><?=$month; ?> <span style="text-shadow: 1px 1px 1px gray;" class="tempest text-6xl font-extrabold"><?=$year; ?></span></p>
                <div class="flex gap-4">
                    <a class="group" href="https://www.instagram.com/sis.store.pr/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="grey" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram group-hover:fill-sis-white group-hover:stroke-sis-grey duration-300">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path class="group-hover:fill-sis-grey duration-200" d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                    </a>
                    <a class="" href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="grey" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook hover:fill-sis-white hover:stroke-sis-grey duration-300">
                            <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="lg:px-16 px-0">
                <h1 class=" lg:text-5xl text-3xl lg:px-4 px-2 py-6 border-b border-b-sis-grey tempest"><?= $post->title?></h1>
                <p class="lg:text-xl text-md lg:pt-4 pt-6 pb-4 lg:px-14 px-4  libre"> • <?=$post->content; ?></p>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4  libre">• <?=$post->content1; ?></p>
                <h3 class=" lg:text-4xl text-3xl lg:px-4 py-6  tempest underline decoration-sis-sandy"><?=$post->sub_title?></h3>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4  libre"><?=$post->sub_title_content; ?></p>
                <div class="flex lg:px-4 py-6 items-center gap-4">
                    <span class="w-[5px] rounded-full h-[40px] bg-sis-sandy">.</span>
                    <h2 class=" lg:text-5xl text-4xl  tempest"><?=$post->sub_title1?></h2>
                </div>
                <div class="w-full lg:pt-4 py-4 lg:pr-14 flex">
                    <img src="resources/images/<?=$post->image?>" alt="" class="object-cover w-1/2 h-[450px] grayscale">
                    <div class="w-1/2 pl-4 lg:pl-12">
                        <p class=" lg:text-xl text-md md:text-lg   libre pt-4">• <?= $post->sub_title1_content?></p>
                        <p class="lg:text-xl md:text-lg text-md lg:pt-4 pb-4 pt-8 libre">• <?= $post->sub_title1_content1?></p>
                    </div>
                </div>
                <h3 class=" lg:text-4xl text-3xl lg:px-4 py-6 underline decoration-sis-sandy  tempest"><?=$post->sub_title2?></h3>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4 libre">• <?= $post->sub_title2_content?></p>
                <h3 class=" lg:text-4xl text-3xl lg:px-4 py-6 underline decoration-sis-sandy tempest"><?=$post->sub_title3?></h3>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4 libre">• <?= $post->sub_title3_content?></p>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4 libre">• <?= $post->sub_title3_content1?></p>
                <h3 class=" lg:text-4xl text-3xl lg:px-4 py-6 underline decoration-sis-sandy tempest"><?= $post->sub_title4?><span class="stink">:</span></h3>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4 libre">• <?= $post->sub_title4_content?></p>
                <div class="flex px-4 py-6 items-center gap-4">
                    <span class="w-[5px] rounded-full h-[40px] bg-sis-sandy">.</span>
                    <h2 class=" lg:text-6xl text-4xl  tempest"><?=$post->sub_title5?> <span class="stink">:</span></h2>
                </div>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4 libre">• <?=$post->sub_title5_content?></p>
                <p class="lg:text-xl text-md lg:pt-4 py-4 lg:px-14 px-4 libre">• <?=$post->sub_title5_content1?></p>
                <p class="lg:text-2xl text-lg lg:pt-8 py-4 text-right lg:px-14 px-4 angle-font">
                Start your journey . View our 
                <a href="" class="ml-2 border hover:bg-neutral-300 duration-200 border-sis-grey px-6 py-1.5 rische ">Collections</a>
                </p>
            </div>
        </div>
    </main>
    <?php include 'public/Components/footer.php' ?>
    <script src="dist/javascript/blog.js"></script>
</body>

</html>
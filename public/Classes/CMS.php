<?php 
include 'ProductHandler.php';

class CMS extends ProductHandler
{

    public function fetchCmsID()
    {
        try {
            $query = "SELECT * FROM admcontrol";

            $stmt = $this->conn->query($query);
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            error_log("Could not get admin view: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function updateSecondBanner($firstImgId,$secondImgId)
    {
        try {
            $query = "UPDATE admcontrol SET secondBanerFirstImg = :secondBanerFirstImg,
            secondBanerSecondImg = :secondBanerSecondImg WHERE cms_id = 1";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':secondBanerFirstImg', $firstImgId, PDO::PARAM_INT);
            $stmt->bindParam(':secondBanerSecondImg', $secondImgId, PDO::PARAM_INT);
            $result = $stmt->execute();
            if($result) {
            return true;
            } else {
                throw new Exception('Ndryshimi nuk mundet te kryhet, provoni prap');
            }
        } catch (Exception $e) {
            error_log("Mesazhi: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function updateWhiteBanner($firstImgId)
    {
        try {
            $query = "UPDATE admcontrol SET whiteBanerFirstImg = :whiteBanerFirstImg WHERE cms_id = 1";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':whiteBanerFirstImg', $firstImgId, PDO::PARAM_INT);
            $stmt->bindParam(':whiteBanerSecondImg', $secondImgId, PDO::PARAM_INT);
            $result = $stmt->execute();
            if($result) {
            return true;
            } else {
                throw new Exception('Ndryshimi nuk mundet te kryhet, provoni prap');
            }
        } catch (Exception $e) {
            error_log("Mesazhi: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchSecondBannerProducts($productId1, $productId2) {
        try {
            $query = "SELECT p1.product_name as product_name1, p1.product_id as pr_id1, p2.product_id as pr_id2,
            p2.product_name as product_name2, pi1.main_image as image_path1, pi2.main_image as image_path2
                      FROM products p1
                      INNER JOIN product_images pi1 ON p1.product_id = pi1.product_id
                      INNER JOIN products p2 ON p1.product_id != p2.product_id
                      INNER JOIN product_images pi2 ON p2.product_id = pi2.product_id
                      WHERE p1.product_id = :productId1 AND p2.product_id = :productId2
                      LIMIT 1";  // Add LIMIT 1 to get only one row
    
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productId1', $productId1, PDO::PARAM_INT);
            $stmt->bindParam(':productId2', $productId2, PDO::PARAM_INT);
            $stmt->execute();
    
            $result = $stmt->fetch(PDO::FETCH_ASSOC);  // Use fetch instead of fetchAll
    
            return $result;
        } catch (Exception $e) {
            error_log("Could not fetch second banner products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchWhiteBannerProducts($productId1) {
        try {
            $query = "SELECT p.product_id,p.product_name, im.main_image, im.image1, im.image2 FROM products p INNER JOIN product_images im 
            ON p.product_id = im.product_id WHERE p.product_id = :productId1";  // Add LIMIT 1 to get only one row
    
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productId1', $productId1, PDO::PARAM_INT);
            $stmt->execute();
    
            $result = $stmt->fetch(PDO::FETCH_ASSOC);  // Use fetch instead of fetchAll
            if($result) {
            return $result;
            } else {
                throw new Exception('Spo bon');
            }
        } catch (Exception $e) {
            error_log("Could not fetch second banner products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }

    public function fetchProductWithId($productId) 
    {
        $query = "SELECT p.product_id,p.product_name, im.main_image FROM products p 
        INNER JOIN product_images im ON p.product_id = im.product_id
        WHERE p.product_id = :productId";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':productId',$productId,PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if($result) {
            return $result;
        } else {
            echo "Something went wrong";
        }
    }
}
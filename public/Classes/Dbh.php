<?php 
$config = require __DIR__ . "/../../Config/config.php"; // Use __DIR__ for the correct path

class Dbh {
    private $host;
    private $username;
    private $password;
    private $dbname;
    private $config;

    public function __construct() {
        $this->config = require __DIR__ . "/../../Config/config.php";
        $this->host = $this->config['host'];
        $this->username = $this->config['username'];
        $this->password = $this->config['password'];
        $this->dbname = $this->config['database'];
    }
    
    public function connect() {
        $dsn = "mysql:host=$this->host;dbname=$this->dbname;charset=utf8mb4";

        try {
            $pdo = new PDO($dsn, $this->username, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            throw new Exception("Connection failed: " . $e->getMessage());
        }
    }
    protected function prepare($sql)
    {
        return $this->connect()->prepare($sql);
    }
}
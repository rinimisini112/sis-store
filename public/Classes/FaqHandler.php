<?php
class FaqHandler
{
    private $question;
    private $answer;
    private $conn;

    public function __construct(Dbh $conn)
    {
        $this->conn = $conn->connect();
    }

    public function readFAQ()
    {
        try {
            $query = "SELECT * FROM faq ORDER BY faq_id ASC";

            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        } catch (Exception $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function readSingleFAQ($faqID)
    {
        try {
            $query = "SELECT * FROM faq WHERE faq_id = :faqID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':faqID',$faqID,PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result;
        } catch (Exception $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function insertFAQ($question,$answer)
    {
        try {
            $query = "INSERT INTO faq(question,answer) VALUES(:question,:answer)";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':question',$question,PDO::PARAM_STR);
            $stmt->bindParam(':answer',$answer,PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result) {
                return true;
            } else {
                throw new Exception('Nuk mundemi te fusim pyetjen per momentin');
            }
 
        } catch (Exception $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function updateFAQ($faqID,$question,$answer)
    {
        try {
            $query = "UPDATE faq set question = :question, answer = :answer WHERE faq_id = :faqId";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':question',$question,PDO::PARAM_STR);
            $stmt->bindParam(':answer',$answer,PDO::PARAM_STR);
            $stmt->bindParam(':faqId',$faqID,PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result) {
                return true;
            } else {
                throw new Exception('Nuk mundemi te ndryshojm pyetjen per momentin');
            }
 
        } catch (Exception $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function deleteFAQ($faqID)
    {
        try {
            $query = "DELETE FROM faq WHERE faq_id = :faqId";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':faqId',$faqID,PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result) {
                return true;
            } else {
                throw new Exception('Nuk mundemi te fshijm pyetjen per momentin');
            }
 
        } catch (Exception $e) {
            echo 'Error' . $e->getMessage();
        }
    }
}

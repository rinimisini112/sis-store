<?php
include "Dbh.php";

class Jewelery extends Dbh
{
    public function fetchCategories()
    {
        try {
            $query = "SELECT * FROM jewelry_categories";
            $stmt = $this->prepare($query);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not get Jewelry Categories: " . $e->getMessage();
        }
    }

    public function fetchJewelery($sort = 'newest')
    {
        try {
            switch ($sort) {
                case 'price_desc':
                    $sortClause = 'ORDER BY j.price DESC';
                    break;
                case 'price_asc':
                    $sortClause = 'ORDER BY j.price ASC';
                    break;
                case 'newest':
                    $sortClause = 'ORDER BY j.created_at DESC'; // Assuming there's a created_at column for product creation time
                    break;
                case 'oldest':
                    $sortClause = 'ORDER BY j.created_at ASC'; // Change this if your table structure is different
                    break;
                default:
                    $sortClause = 'ORDER BY j.created_at DESC'; // Default to newest
                    break;
            }
            $query = "SELECT j.*, ji.id as image_id, ji.main_image,ji.image1,ji.image2, jc.id as categoryid, jc.name as categoryname 
            FROM jewelries j
            INNER JOIN jewelry_images ji ON j.id = ji.jewellry_id
            INNER JOIN jewelry_categories jc ON j.category_id = jc.id
            $sortClause";
            $stmt = $this->prepare($query);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not get jewelry :" . $e->getMessage();
        }
    }


    public function fetchFilteredJewelry($price, $materials = [])
    {
        try {
            // Build the WHERE clause based on the selected materials and optional price range
            $whereClauses = [];

            // Add price range condition only if it's provided
            if (isset($price)) {
                $whereClauses[] = "j.price <= :price";
            }

            // Add material condition only if materials are selected
            if (!empty($materials)) {
                $materialValues = implode("','", $materials);
                $whereClauses[] = "j.material IN ('$materialValues')";
            }

            $whereClause = !empty($whereClauses) ? 'WHERE ' . implode(' AND ', $whereClauses) : '';

            // Construct the query with the WHERE and ORDER BY clauses
            $query = "SELECT j.*, ji.id as image_id, ji.main_image, ji.image1, ji.image2, jc.id as categoryid, jc.name as categoryname 
            FROM jewelries j
            INNER JOIN jewelry_images ji ON j.id = ji.jewellry_id
            INNER JOIN jewelry_categories jc ON j.category_id = jc.id
            $whereClause";
            $stmt = $this->prepare($query);

            // Bind parameters only if they are provided
            if (isset($price)) {
                $stmt->bindParam(':price', $price, PDO::PARAM_INT);
            }

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not get jewelry: " . $e->getMessage();
        }
    }

    public function sortJewelriesByIds($jewelries, $sort = 'newest')
    {
        try {
            if (empty($jewelries)) {
                return []; // Return an empty array if no jewelries are provided
            }
    
            // Extract 'id' values from the input array
            $jewelryIds = array_column($jewelries, 'id');
    
            $placeholders = implode(',', array_fill(0, count($jewelryIds), '?'));
    
            // Sort clause based on the provided $sort parameter
            switch ($sort) {
                case 'price_desc':
                    $sortClause = 'ORDER BY j.price DESC';
                    break;
                case 'price_asc':
                    $sortClause = 'ORDER BY j.price ASC';
                    break;
                case 'newest':
                    $sortClause = 'ORDER BY j.created_at DESC';
                    break;
                case 'oldest':
                    $sortClause = 'ORDER BY j.created_at ASC';
                    break;
                default:
                    $sortClause = 'ORDER BY j.created_at DESC';
                    break;
            }
    
            // Construct the query to fetch jewelries based on provided IDs with sorting
            $query = "SELECT j.*, ji.id as image_id, ji.main_image, ji.image1, ji.image2, jc.id as categoryid, jc.name as categoryname 
                FROM jewelries j
                INNER JOIN jewelry_images ji ON j.id = ji.jewellry_id
                INNER JOIN jewelry_categories jc ON j.category_id = jc.id
                WHERE j.id IN ($placeholders)
                $sortClause";
    
            $stmt = $this->prepare($query);
    
            // Bind parameters
            foreach ($jewelryIds as $index => $jewelryId) {
                $stmt->bindValue($index + 1, $jewelryId, PDO::PARAM_INT);
            }
    
            $stmt->execute();
    
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not sort jewelry by IDs: " . $e->getMessage();
        }
    }
    public function fetchSingleJewelry($id)
    {
        try {
            $query = "SELECT j.*, ji.*, jc.id as category_id, jc.name as category_name 
            FROM jewelries j INNER JOIN jewelry_images ji ON j.id=ji.jewellry_id
            INNER JOIN jewelry_categories jc ON j.category_id = jc.id 
            WHERE j.id = :jewelryid";
            $stmt = $this->prepare($query);
            $stmt->bindParam(':jewelryid', $id);
            $stmt->execute();

            return $stmt->fetchObject();
        } catch (\Throwable $th) {
            echo "Could not get jewelry :" . $th->getMessage();
        }
    }
}

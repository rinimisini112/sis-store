<?php
include "Dbh.php";

class Mail extends Dbh
{
    public function isEmailInDatabase($email)
    {
        $query = "SELECT COUNT(*) FROM newsletters WHERE email = :email";
        $stmt = $this->prepare($query);
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        // Fetch the result
        $count = $stmt->fetchColumn();

        // If the count is greater than 0, the email exists
        return ($count > 0);
    }

    public function insertNewsletter($email)
    {
        // Sanitize and validate the email
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        
        // Validate the sanitized email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['mailMssg'] = 'Invalid Email Format';
            header("Location: ../../index.php#footer");
            return; // Invalid email format
        }
        // Check if the email already exists
        if ($this->isEmailInDatabase($email)) {
            $_SESSION['mailMssg'] = 'You are already subsribed to our newsletter';
            header("Location: ../../index.php#footer");
            return; // Email already exists
        }

        // If the email doesn't exist, insert it
        $query = "INSERT INTO newsletters(email) VALUES(:email)";
        $stmt = $this->prepare($query);
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        return true; // Email inserted successfully
    }
}
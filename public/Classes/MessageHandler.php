<?php
class MessageHandler
{
    private $fullName;
    private $email;
    private $message;
    private $conn;

    public function __construct(Dbh $conn)
    {
        $this->conn = $conn;
    }

    public function setName($name)
    {
        // Check if the name is not empty and contains only letters and spaces
        if (!empty($name) && preg_match("/^[a-zA-Z\s]+$/", $name)) {
            $this->fullName = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS);
            return true; // Validation successful
        } else {
            return false; // Validation failed
        }
    }

    public function setEmail($email)
    {
        // Check if the email is not empty and is a valid email address
        if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);
            return true; // Validation successful
        } else {
            return false; // Validation failed
        }
    }

    public function setMessage($message)
    {
        // Check if the message is not empty
        if (!empty($message)) {
            $this->message = filter_var($message, FILTER_SANITIZE_SPECIAL_CHARS);
            return true; // Validation successful
        } else {
            return false; // Validation failed
        }
    }

    public function sendMessage()
    {
        try {
            // Check if all required fields are validated
            if ($this->setName($this->fullName) && $this->setEmail($this->email) && $this->setMessage($this->message)) {
                $query = "INSERT INTO messages (username, user_email, user_message) VALUES (:name, :email, :message)";
                $stmt = $this->conn->connect()->prepare($query);

                // Bind parameters
                $stmt->bindParam(':name', $this->fullName);
                $stmt->bindParam(':email', $this->email);
                $stmt->bindParam(':message', $this->message);

                // Execute the query
                $stmt->execute();

                echo "Message sent successfully!";
            } else {
                echo "Invalid input. Please check your information and try again.";
            }
        } catch (Exception $e) {
            echo "Could not send message: " . $e->getMessage();
        }
    }
    public function readMessage()
    {
        try {
            $query = "SELECT * FROM messages";
            $stmt = $this->conn->connect()->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Messages: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function deleteMessage($messageId)
    {
        try {
            $query = "DELETE FROM messages WHERE message_id = :messageId";
            $stmt = $this->conn->connect()->prepare($query);
            $stmt->bindParam(':messageId', $messageId,PDO::PARAM_INT);

            $result = $stmt->execute();
            if($result) {
            return true;
            } else {
                throw new Exception('Mesazhi nuk mundi te fshihet, provoni prap');
            }
        } catch (Exception $e) {
            error_log("Could not get Messages: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
}

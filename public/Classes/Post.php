<?php 
require "Dbh.php";

class Post extends Dbh
{
    public $id;
    public $main_image;
    public $title;
    public $content;
    public $content1;
    public $content2;
    public $sub_title;
    public $sub_title_content;
    public $sub_title_content1;
    public $sub_title_content2;
    public $sub_title2;
    public $sub_title2_content;
    public $sub_title2_content1;
    public $sub_title2_content2;
    public $sub_title3;
    public $sub_title3_content;
    public $sub_title3_content1;
    public $sub_title3_content2;    
    public $sub_title4;
    public $sub_title4_content;
    public $sub_title4_content1;
    public $sub_title4_content2;    
    public $sub_title5;
    public $sub_title5_content;
    public $sub_title5_content1;
    public $sub_title5_content2;    
    public $sub_title6;
    public $sub_title6_content;
    public $sub_title6_content1;
    public $sub_title6_content2;    
    public $sub_title7;
    public $sub_title7_content;
    public $sub_title7_content1;
    public $sub_title7_content2;

    public function fetchAllPosts()
    {
        try{
            $query = "SELECT * FROM blog_posts";
            $stmt = $this->prepare($query);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not get blog posts:" . $e->getMessage();
        }
    }
    public function fetchPostWithId($id)
    {
        try{
            $query = "SELECT * FROM blog_posts WHERE id=:id";
            $stmt = $this->prepare($query);
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not get blog posts:" . $e->getMessage();
        }
    }
}
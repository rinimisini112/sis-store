<?php 

class Preference extends Dbh
{
    public function fetchIdFromCollections() {
        try {
            // Modify your original query to order by random and limit to 2
            $query = "SELECT product_id FROM products ORDER BY RAND() LIMIT 2";
            $stmt = $this->prepare($query);
            $stmt->execute();

            // Fetch the results as an array of IDs
            $result = $stmt->fetchAll(PDO::FETCH_COLUMN);

            return $result;
        } catch (Exception $e) {
            echo "Could not fetch IDs from collections: " . $e->getMessage();
        }
    }

    public function fetch2PreferencesByIds($id1, $id2)
    {
        try {
            // Construct the query for fetching two specific products
            $query = "SELECT
                        p.product_id, 
                        p.product_name,
                        pi.main_image,
                        pi.image1
                    FROM 
                        products p
                    INNER JOIN 
                        product_images pi ON p.product_id = pi.product_id
                    WHERE 
                        p.product_id IN (:id1, :id2)";
    
            $stmt = $this->prepare($query);
    
            // Bind parameters
            $stmt->bindParam(':id1', $id1, PDO::PARAM_INT);
            $stmt->bindParam(':id2', $id2, PDO::PARAM_INT);
    
            $stmt->execute();
    
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            echo "Could not get products based on id :" . $e->getMessage();
        }
    }
}
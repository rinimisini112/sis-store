<?php

class ProductFilter 
{
    public static function buildWhereClause($filters)
    {
        $filterConditions = [];
        if (isset($filters['priceRange'])) {
            $priceRange = (int)$filters['priceRange'];
            $filterConditions[] = "p.price <= $priceRange";
        }
        // Check if sizes are specified
        $sizes = ['XS', 'S', 'M', 'L', 'XL', '2XL'];
        $sizeConditions = [];
        foreach ($sizes as $size) {
            if (!empty($filters['size' . $size])) {
                $sizeConditions[] = "s.sizeName = '$size'";
            }
        }

        // Check if number sizes are specified
        $numberSizes = ['30', '31', '32', '34', '36', '38', '40'];
        $numberSizeConditions = [];
        foreach ($numberSizes as $size) {
            if (!empty($filters['size' . $size])) {
                $numberSizeConditions[] = "s.sizeName = '$size'";
            }
        }

        // Combine conditions using OR
        if (!empty($sizeConditions) || !empty($numberSizeConditions)) {
            $filterConditions[] = '(' . implode(' OR ', array_merge($sizeConditions, $numberSizeConditions)) . ')';
        }

        // Construct the final WHERE clause
        $whereClause = (!empty($filterConditions)) ? implode(' AND ', $filterConditions) : '';

        return $whereClause;
    }
  public function buildCategoryWhereClause( $filters = [])
    {
        $filterConditions = [];
    
        if (isset($filters['price'])) {
            $priceRange = (int)$filters['price'];
            $filterConditions[] = "p.price <= $priceRange";
        }
        $sizes = ['1', '2', '3', '4', '5', '6'];
        $numberSizes = ['14', '15', '16', '17', '18', '19', '20'];
        $sizeConditions = [];
        foreach ($sizes as $size) {
            $filterKey = 'size' . $size;
            if (!empty($filters[$filterKey])) {
                $sizeConditions[] = "ps.SizeID = " . (int)$filters[$filterKey];
            }
        }
        
        $numberSizeConditions = [];
        foreach ($numberSizes as $size) {
            $filterKey = 'size' . $size;
            if (!empty($filters[$filterKey])) {
                $numberSizeConditions[] = "ps.SizeID = " . (int)$filters[$filterKey];
            }
        }
    
        // Combine conditions using OR
        if (!empty($sizeConditions) || !empty($numberSizeConditions)) {
            $filterConditions[] = '(' . implode(' OR ', array_merge($sizeConditions, $numberSizeConditions)) . ')';
        }
    
        // Construct the final WHERE clause
        $whereClause = (!empty($filterConditions)) ? implode(' AND ', $filterConditions) : '';
    
        return $whereClause;
    }
    public static function buildGroupByClause()
    {
        return 'GROUP BY p.product_id';
    }
}
<?php
class VintageCategoryHandler
{
    private $conn;
    const CATEGORIES_TABLE = 'categories';
    const PRODUCTS_TABLE = 'vintageproducts';
    const PRODUCT_IMAGES_TABLE = 'vintage_images';

    public function __construct(Dbh $conn)
    {
        $this->conn = $conn->connect();

        if (!$this->conn) {
            error_log("Could not connect to the database.");
            echo "Sorry, something went wrong with the database connection.";
            exit();
        }
    }

    public function fetchCategories()
    {
        try {
            $query = "SELECT * FROM " . self::CATEGORIES_TABLE;

            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Categories: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function insertCategories($categoryName)
    {
        try {

            $query = "INSERT INTO " . self::CATEGORIES_TABLE . "(category_name) VALUES(:category_name)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':category_name', $categoryName);
            $result = $stmt->execute();
            if ($result) {
                return true;
            } else {
                throw new Exception('Deshtoi shtimi i kategorise');
            }
        } catch (Exception $e) {
            error_log("Deshtoi shtimi!! : " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchCategoriesWithProductCount()
    {
        try {
            $query = "
            SELECT c.*, COUNT(p.product_id) AS product_count
            FROM " . self::CATEGORIES_TABLE . " c
            LEFT JOIN " . self::PRODUCTS_TABLE . " p ON c.category_id = p.category_id
            GROUP BY c.category_id
        ";

            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Categories with Product Count: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchCategoriesForFooter()
    {
        try {
            $query = "SELECT * FROM " . self::CATEGORIES_TABLE . " LIMIT 4";

            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Categories: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchCategoriesWithId($cId)
    {
        try {
            $query = "SELECT * FROM " . self::CATEGORIES_TABLE . " WHERE category_id = :categoryId";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryId', $cId, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);


            return $result;
        } catch (Exception $e) {
            error_log("Could not get Categories: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function updateCategoryWithId($cId, $newCategoryName)
    {
        try {
            $query = "UPDATE " . self::CATEGORIES_TABLE . " SET category_name = :newCategoryName WHERE category_id = :categoryId";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryId', $cId, PDO::PARAM_INT);
            $stmt->bindParam(':newCategoryName', $newCategoryName, PDO::PARAM_STR);
            $stmt->execute();

            $rowCount = $stmt->rowCount();

            if ($rowCount > 0) {
                return true;
            } else {
                throw new Exception('Update failed. No matching category found.');
            }
        } catch (Exception $e) {
            error_log("Could not update category: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function filterVintageProductsByCategory($cName, $sort)
    {
        try {
            $query = "
                SELECT *
                FROM " . self::PRODUCTS_TABLE . "
                INNER JOIN " . self::CATEGORIES_TABLE . " ON " . self::PRODUCTS_TABLE . ".category_id = " . self::CATEGORIES_TABLE . ".category_id
                INNER JOIN " . self::PRODUCT_IMAGES_TABLE . " ON " . self::PRODUCTS_TABLE . ".vintage_id = " . self::PRODUCT_IMAGES_TABLE . ".vintage_id
                WHERE " . self::CATEGORIES_TABLE . ".category_id = :categoryName
            ";

            switch ($sort) {
                case 'price_desc':
                    $query .= " ORDER BY " . self::PRODUCTS_TABLE . ".v_price DESC";
                    break;
                case 'price_asc':
                    $query .= " ORDER BY " . self::PRODUCTS_TABLE . ".v_price ASC";
                    break;
                case 'newest':
                    $query .= " ORDER BY " . self::PRODUCTS_TABLE . ".created_at DESC";
                    break;
                case 'oldest':
                    $query .= " ORDER BY " . self::PRODUCTS_TABLE . ".created_at ASC";
                    break;
                default:
                    $query .= " ORDER BY " . self::PRODUCTS_TABLE . ".created_at DESC";
                    break;
            }

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryName', $cName, PDO::PARAM_STR);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        } catch (Exception $e) {
            error_log("Could not filter products by category: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function filterVintageByCategory($cName, $sort)
    {
        try {
            $query = "SELECT * FROM vintageproducts vp
                INNER JOIN " . self::CATEGORIES_TABLE . " ON vp.category_id = " . self::CATEGORIES_TABLE . ".category_id
                INNER JOIN vintage_images vim ON vp.vintage_id = vim.vintage_id
                WHERE " . self::CATEGORIES_TABLE . ".category_id = :categoryName
            ";

            switch ($sort) {
                case 'price_desc':
                    $query .= " ORDER BY vp.v_price DESC";
                    break;
                case 'price_asc':
                    $query .= " ORDER BY vp.v_price ASC";
                    break;
                case 'newest':
                    $query .= " ORDER BY vp.created_at DESC";
                    break;
                case 'oldest':
                    $query .= " ORDER BY vp.created_at ASC";
                    break;
                default:
                    $query .= " ORDER BY vp.created_at DESC";
                    break;
            }

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryName', $cName, PDO::PARAM_STR);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        } catch (Exception $e) {
            error_log("Could not filter products by category: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchFilteredProducts($categoryId,$filteredItems, $filters = [])
    {
        try {
            $productFilter = new VintageFilter();
    
            $whereClause = $productFilter->buildVintageCategoryWhereClause($filters);
            $groupByClause = $productFilter::buildGroupByClause();
    
            // Use the filtered items as the base result and apply additional filtering
            $filteredIds = array_column($filteredItems, 'vintage_id');
            $filteredIdsStr = implode(',', $filteredIds);
            if(empty($filteredIdsStr)) {
                throw new Exception('No ids were found');
            }
        $query = "SELECT p.vintage_id, p.category_id, MAX(p.v_name) as v_name, MAX(p.v_price) as price, MAX(p.created_at) as created_at
        FROM vintageproducts p
        LEFT JOIN vintagesizes ps ON p.vintage_id = ps.vintage_id 
        WHERE $whereClause AND  category_id = :categoryID AND p.vintage_id IN ($filteredIdsStr) $groupByClause";
        error_log($groupByClause);
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryID',$categoryId);
            $stmt->execute();
    
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            foreach ($result as &$product) {
                $productId = $product['vintage_id'];
                $images = $this->fetchImagesForFilteredProduct($productId);
                $product['vimages'] = $images;
            }
    
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function sortFilteredCategories($items, $categoryId, $sort = 'newest')
    {
        try {
            $productFilter = new VintageFilter();

            $groupByClause = $productFilter::buildGroupByClause();

            switch ($sort) {
                case 'price_desc':
                    $sortClause = 'ORDER BY MAX(p.v_price) DESC';
                    break;
                case 'price_asc':
                    $sortClause = 'ORDER BY MAX(p.v_price) ASC';
                    break;
                case 'newest':
                    $sortClause = 'ORDER BY MAX(p.created_at) DESC';
                    break;
                case 'oldest':
                    $sortClause = 'ORDER BY MAX(p.created_at) ASC';
                    break;
                default:
                    $sortClause = 'ORDER BY MAX(p.created_at) DESC';
                    break;
            }

            // Assuming $items is an array of items with relevant data
            $itemIds = implode(',', array_column($items, 'vintage_id'));

            $query = "SELECT p.vintage_id,p.category_id, p.v_name, MAX(p.v_price) as price, MAX(p.created_at) as created_at
                FROM vintagesizes ps
                RIGHT JOIN vintageproducts p ON ps.vintage_id = p.vintage_id
                WHERE p.vintage_id IN ($itemIds) AND p.category_id = :categoryID
                $groupByClause $sortClause";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryID', $categoryId, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as &$product) {
                $productId = $product['vintage_id'];
                $images = $this->fetchImagesForFilteredProduct($productId);
                $product['vimages'] = $images;
            }

            return $result;
        } catch (Exception $e) {
            error_log("Could not get Products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchImagesForFilteredProduct($productId)
    {
        try {
            $query = "SELECT * FROM vintage_images WHERE vintage_id = :productId";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!$result) {
                throw new Exception("Product not found.");
            }

            return $result;
        } catch (Exception $e) {
            error_log("Could not get Product: " . $e->getMessage());
        }
    }
    public function deleteCategory($categoryId)
    {
        try {
            $query = "DELETE FROM categories WHERE category_id = :categoryID";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':categoryID', $categoryId, PDO::PARAM_INT);
            $result = $stmt->execute();
            if ($result) {
                return true;
            } else {
                throw new Exception('Deshtoi fshirja e kategoris, provoni perseri');
            }
        } catch (Exception $e) {
            echo 'Na vjen keq: ' . $e->getMessage();
        }
    }
}

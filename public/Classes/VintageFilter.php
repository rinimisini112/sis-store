<?php

class VintageFilter 
{
    public static function buildVintageWhereClause($filters)
    {
        $filterConditions = [];
        if (isset($filters['v_price'])) {
            $priceRange = (int)$filters['v_price'];
            $filterConditions[] = "p.v_price <= $priceRange";
        }
        // Check if sizes are specified
        $sizes = ['XS', 'S', 'M', 'L', 'XL', '2XL'];
        $sizeConditions = [];
        foreach ($sizes as $size) {
            if (!empty($filters['size' . $size])) {
                $sizeConditions[] = "s.SizeName = '$size'";
            }
        }

        // Check if number sizes are specified
        $numberSizes = ['30', '31', '32', '34', '36', '38', '40'];
        $numberSizeConditions = [];
        foreach ($numberSizes as $size) {
            if (!empty($filters['size' . $size])) {
                $numberSizeConditions[] = "s.SizeName = '$size'";
            }
        }

        // Combine conditions using OR
        if (!empty($sizeConditions) || !empty($numberSizeConditions)) {
            $filterConditions[] = '(' . implode(' OR ', array_merge($sizeConditions, $numberSizeConditions)) . ')';
        }

        // Construct the final WHERE clause
        $whereClause = (!empty($filterConditions)) ? implode(' AND ', $filterConditions) : '';
        error_log($whereClause);
        return $whereClause;
    }
  public function buildVintageCategoryWhereClause( $filters = [])
    {
        $filterConditions = [];
    
        if (isset($filters['v_price'])) {
            $priceRange = (int)$filters['v_price'];
            $filterConditions[] = "p.v_price <= $priceRange";
        }
        $sizes = ['1', '2', '3', '4', '5', '6'];
        $numberSizes = ['14', '15', '16', '17', '18', '19', '20'];
        $sizeConditions = [];
        foreach ($sizes as $size) {
            $filterKey = 'size' . $size;
            if (!empty($filters[$filterKey])) {
                $sizeConditions[] = "ps.SizeID = " . (int)$filters[$filterKey];
            }
        }
        
        $numberSizeConditions = [];
        foreach ($numberSizes as $size) {
            $filterKey = 'size' . $size;
            if (!empty($filters[$filterKey])) {
                $numberSizeConditions[] = "ps.SizeID = " . (int)$filters[$filterKey];
            }
        }
    
        // Combine conditions using OR
        if (!empty($sizeConditions) || !empty($numberSizeConditions)) {
            $filterConditions[] = '(' . implode(' OR ', array_merge($sizeConditions, $numberSizeConditions)) . ')';
        }
    
        // Construct the final WHERE clause
        $whereClause = (!empty($filterConditions)) ? implode(' AND ', $filterConditions) : '';
    
        return $whereClause;
    }
    public static function buildGroupByClause()
    {
        return 'GROUP BY p.vintage_id';
    }

}
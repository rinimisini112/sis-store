<?php

class VintageHandler
{
    private $productName;
    private $productImage;
    private $productDescription;
    protected $conn;

    const PRODUCTS_TABLE = 'vintageproducts';
    const PRODUCT_IMAGES_TABLE = 'vintage_images';
    const CATEGORIES_TABLE = 'categories';
    const PRODUCTSIZES_TABLE = 'vintagesizes';
    const SIZES_TABLE = 'sizes';

    public function __construct(Dbh $conn)
    {
        $this->conn = $conn->connect();

        if (!$this->conn) {
            // Handle the error appropriately (e.g., log it, display a user-friendly message)
            error_log("Could not connect to the database.");
            echo "Sorry, something went wrong with the database connection.";
            exit(); // Stop execution if the connection fails
        }
    }

    public function setProductName($name)
    {
        // Add any necessary validation for the product name
        $this->productName = $name;
    }

    public function setProductImage($imageUrl)
    {
        // Add any necessary validation for the product image URL
        $this->productImage = $imageUrl;
    }

    public function setProductDescription($description)
    {
        // Add any necessary validation for the product description
        $this->productDescription = $description;
    }

    public function fetchVintageProducts($sort = 'newest')
    {
        try {
            switch ($sort) {
                case 'price_desc':
                    $sortClause = 'ORDER BY p.v_price DESC';
                    break;
                case 'price_asc':
                    $sortClause = 'ORDER BY p.v_price ASC';
                    break;
                case 'newest':
                    $sortClause = 'ORDER BY p.created_at DESC'; // Assuming there's a created_at column for product creation time
                    break;
                case 'oldest':
                    $sortClause = 'ORDER BY p.created_at ASC'; // Change this if your table structure is different
                    break;
                default:
                    $sortClause = 'ORDER BY p.created_at DESC'; // Default to newest
                    break;
            }
    
            $query = "SELECT * FROM " . self::PRODUCTS_TABLE . " p INNER JOIN " . self::PRODUCT_IMAGES_TABLE . " im ON p.vintage_id = im.vintage_id $sortClause";
            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchFilteredVintageProducts( $filters = [])
{
    try {
        $vintageFilter = new VintageFilter();

        $whereClause = $vintageFilter::buildVintageWhereClause($filters);
        $groupByClause = $vintageFilter::buildGroupByClause();

        $query = "SELECT p.vintage_id, p.v_name, MAX(p.v_price) as price, MAX(p.created_at) as created_at
            FROM vintagesizes ps
            RIGHT JOIN vintageproducts p ON ps.vintage_id = p.vintage_id
            LEFT JOIN sizes s ON ps.SizeID = s.SizeID
            WHERE $whereClause
            $groupByClause
            ORDER BY MAX(p.created_at) DESC";
        $stmt = $this->conn->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as &$product) {
            $productId = $product['vintage_id'];
            $images = $this->fetchVintageImagesForFilteredProduct($productId);
            $product['vimages'] = $images;
            
        }

        return $result;
    } catch (Exception $e) {
        error_log("Could not get Products: " . $e->getMessage());
    }
}
public function sizeDoesNotExist($postData)
{
    try {
        // Extract size names from the posted data
        $sizeNames = [];
        foreach ($postData as $key => $value) {
            // Check if the key starts with "size" and the value is "on"
            if (strpos($key, 'size') === 0 && $value === 'on') {
                // Extract the size name from the key
                $sizeNames[] = substr($key, 4); // Assuming "size" is 4 characters long
            }
        }

        // If there are no size names, return false (no need to check)
        if (empty($sizeNames)) {
            return false;
        }

        // Map size names to corresponding SizeID values
        $sizeIDs = $this->getSizeIDsForNames($sizeNames);

        // If there are no size IDs, return false (no need to check)
        if (empty($sizeIDs)) {
            return false;
        }

        // Build a placeholder string for the IN clause
        $placeholders = implode(',', array_fill(0, count($sizeIDs), '?'));

        // Query to check if any products exist with the selected sizes
        $query = "SELECT COUNT(*) FROM vintagesizes vs INNER JOIN sizes s 
        ON vs.SizeID = s.SizeID
        WHERE s.SizeID IN ($placeholders)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute($sizeIDs);

        $count = $stmt->fetchColumn();

        return $count > 0;
    } catch (PDOException $e) {
        // Handle database error
        throw new Exception("Database error: " . $e->getMessage());
    }
}    private function getSizeIDsForNames($sizeNames)
{
    try {
        // Build a placeholder string for the IN clause
        $placeholders = implode(',', array_fill(0, count($sizeNames), '?'));

        // Query to get SizeID values for the given SizeNames
        $query = "SELECT SizeID FROM sizes WHERE SizeName IN ($placeholders)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute($sizeNames);

        $sizeIDs = $stmt->fetchAll(PDO::FETCH_COLUMN);

        return $sizeIDs;
    } catch (PDOException $e) {
        // Handle database error
        throw new Exception("Database error: " . $e->getMessage());
    }
}
public function sortFilteredItems($items, $sort = 'newest')
{
    try {
        $productFilter = new VintageFilter();

        $groupByClause = $productFilter::buildGroupByClause();

        switch ($sort) {
            case 'price_desc':
                $sortClause = 'ORDER BY MAX(p.v_price) DESC';
                break;
            case 'price_asc':
                $sortClause = 'ORDER BY MAX(p.v_price) ASC';
                break;
            case 'newest':
                $sortClause = 'ORDER BY MAX(p.created_at) DESC';
                break;
            case 'oldest':
                $sortClause = 'ORDER BY MAX(p.created_at) ASC';
                break;
            default:
                $sortClause = 'ORDER BY MAX(p.created_at) DESC';
                break;
        }

        // Assuming $items is an array of items with relevant data
        $itemIds = implode(',', array_column($items, 'vintage_id'));

        $query = "SELECT p.vintage_id, p.v_name, MAX(p.v_price) as price, MAX(p.created_at) as created_at
            FROM vintagesizes ps
            RIGHT JOIN vintageproducts p ON ps.vintage_id = p.vintage_id
            LEFT JOIN sizes s ON ps.sizeID = s.sizeID
            WHERE p.vintage_id IN ($itemIds)
            $groupByClause
            $sortClause";
            error_log($query);

        $stmt = $this->conn->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as &$product) {
            $productId = $product['vintage_id'];
            $images = $this->fetchVintageImagesForFilteredProduct($productId);
            $product['vimages'] = $images;
        }

        return $result;
    } catch (Exception $e) {
        error_log("Could not get Products: " . $e->getMessage());
        throw new Exception("Sorry, something went wrong.");
    }
}
    public function fetchVintageImagesForFilteredProduct($productId)
    {
        try {
            $query = "SELECT * FROM vintage_images WHERE vintage_id = :productId";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!$result) {
                throw new Exception("Product not found.");
            }

            return $result;
        } catch (Exception $e) {
            error_log("Could not get Product: " . $e->getMessage());
        }
    }

    public function fetchSingleProduct($productId)
    {
        try {
            $query = "
            SELECT p.*, im.*, c.*
            FROM " . self::PRODUCTS_TABLE . " p
            INNER JOIN " . self::PRODUCT_IMAGES_TABLE . " im ON p.vintage_id = im.vintage_id
            INNER JOIN " . self::CATEGORIES_TABLE . " c ON p.category_id = c.category_id
            WHERE p.vintage_id = :productId
        ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$result) {
                throw new Exception("Product not found.");
            }

            return $result;
        } catch (Exception $e) {
            error_log("Could not get Product: " . $e->getMessage());
        }
    }
    public function fetchSizesForProduct($productId)
    {
        try {
            $query = "
            SELECT ps.*, sz.*
            FROM vintagesizes ps INNER JOIN sizes sz ON ps.SizeID = sz.SizeID
            WHERE ps.vintage_id = :productId ORDER BY sz.sizeID ASC
        ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!$result) {
                throw new Exception("Product not found.");
            }

            return $result;
        } catch (Exception $e) {
            error_log("Could not get Product: " . $e->getMessage());
        }
    }
    public function fetchAllSizes($whereClause = "")
    {
        try {
            $query = "SELECT * FROM sizes";
    
            // Append the WHERE clause if provided
            if (!empty($whereClause)) {
                $query .= " WHERE " . $whereClause;
            }
    
            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            return $result;
        } catch (Exception $e) {
            error_log("Could not get Products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchAllProducts($excludeProductID = null)
    {
        try {
            $query = "SELECT * FROM products p 
            INNER JOIN product_images im ON 
            p.product_id = im.product_id";
    
            if ($excludeProductID !== null) {
                $query .= " WHERE p.product_id <> :excludeProductID";
            }
    
            $stmt = $this->conn->prepare($query);
    
            if ($excludeProductID !== null) {
                $stmt->bindParam(':excludeProductID', $excludeProductID, PDO::PARAM_INT);
            }
    
            $stmt->execute();
    
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            return $result;
        } catch (Exception $e) {
            error_log("Could not fetch all products: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
    public function fetchProductsWith2Img()
    {
        try {
            $query = "SELECT * FROM products p 
                INNER JOIN product_images im1 ON p.product_id = im1.product_id
                WHERE im1.image1 IS NOT NULL";
    
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
    
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            return $result;
        } catch (Exception $e) {
            error_log("Could not fetch all products with 2 images: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
}

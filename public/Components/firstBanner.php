<div class="flex w-full gap-3 pb-3 relative" id="banner1">
            <a href="collections.php" class=" rische font-extrabold absolute text-center bottom-12 drop-shadow-lg-white left-0 w-full z-20 lg:text-7xl text-sis-white transform hover:-translate-y-2 duration-200 text-4xl word-spacing">View Collections Now</a>
            <div class="md:w-[33.33%] w-1/2 h-screen overflow-hidden">
                <img class=" transform hover:scale-110 duration-300 w-full h-full object-cover " src="resources/images/sis-banner-1-min.jpg" alt="Image of a girl in a black outfit">
            </div>
            <div class="md:w-[33.33%] w-1/2 h-screen overflow-hidden">
            <video class=" object-cover w-full h-screen relative " src="resources/images/aboutUsStoryVid.mp4"  type="video/mp4" loop autoplay muted poster="resources/images/Screenshot_20231117_135018_WhatsApp.jpg"></video>
            </div>
            <div class="w-[33.33%] h-screen overflow-hidden md:block hidden">
                <img class=" transform hover:scale-110 duration-300 w-full h-full object-cover" src="resources/images/sis-banner-3-min.jpg" alt="Image of a girl in a black outfit">
            </div>
        </div>
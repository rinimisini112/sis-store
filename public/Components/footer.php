<footer id="footer" class="w-full flex flex-col bg-sis-grey text-sis-white relative">
        <div class="flex md:flex-row flex-col-reverse">
        <div class="md:w-1/2 w-full py-8 md:block flex flex-col items-center justify-center">
        <img src="resources/images/Logo_Sis_Store_1_BW_edited.jpg" class=" w-[85%] md:-ml-16 -ml-0 object-cover" alt="company-logo">
        <form method="post" action="public/controllers/sendMail.php" class="lg:py-6 md:py-10 py-6 lg:w-[70%] md:w-[90%] w-full px-8">
            <?php 
            if(isset($_SESSION['mailMssg'])) {
                echo "<p class=' text-2xl'>" . $_SESSION['mailMssg'] . "</p>";
            unset($_SESSION['mailMssg']);
            } else {
            echo "<p class=' text-2xl'>Subsribe to our Newsletter,
            stay informed on sales and new relases.
            </p>";
            }
            ?>
            <span class="relative">
                <input class="w-full py-2 bg-sis-grey border-b-2 pl-1 border-white group-hover:border-green-500 duration-200 my-3 outline-none placeholder:text-sis-white" type="email" name="email" id="email" placeholder="Enter your email">
                <input type="submit" class=" absolute right-2 top-0 cursor-pointer group" value="Subscribe" name="subsribe">
            </span>
        </form>
        <div class="lg:w-[70%] md:w-[90%] w-full px-8 flex pb-12 md:pb-0 flex-col lg:py-0 md:py-4 py-0">
            <p class=" text-center text-3xl py-2">Follow us on social media.</p>
            <div class="flex items-center justify-center gap-8 py-6">
                <span class="text-xl">
                    <a href="https://www.instagram.com/sis.store.pr/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram hover:fill-sis-white hover:stroke-sis-grey duration-300">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                    </a>
                </span>
                <span class="text-xl">
                    <a href="https://www.facebook.com/profile.php?id=61553619745720" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook hover:fill-sis-white hover:stroke-sis-grey duration-300">
                            <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                        </svg>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class=" grid md:mt-28 mt-16 md:grid-cols-2 grid-cols-1 grid-rows-2 md:gap-x-16 gap-x-0 gap-y-0 md:gap-y-16 md:m-auto mx-auto md:w-1/2 w-[90%]">
        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Browse Clothes</p>
            <?php
            include 'public/controllers/footerCategories.php';
            ?>
        </ul>
        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto md:mt-0 mt-4">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Company</p>
            <li class="text-lg py-2">
                <a class="group relative" href="aboutUs.php">About Us
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="aboutUs.php">Story
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="https://www.instagram.com/sis.store.pr/">Collab with us
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
                </a>
            </li>
        </ul>

        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Contact Us</p>
            <li class="text-lg py-2">
                <a class="group relative" href="tel:+38343555450">043 555 450
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>

                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>

                </a>
            </li>
            <li class="text-lg py-2">
                <a class="group relative" href="mailto:sisxstore@gmail.com">sisxstore@gmail.com
                    <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>

                </a>
            </li>
        </ul>
        <ul class="md:mx-0 md:w-auto w-3/4 mx-auto md:mt-0 mt-4">
            <p class="md:text-4xl text-3xl angle-font font-semibold">Help</p>
            <li class="text-lg py-2">
            <a class="group relative" href="contactUs.php#FAQ">
                FAQ
                <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
            </a>
            </li>
            <li class="text-lg py-2">
            <a class="group relative" href="terms&conditions.php">
                Terms of Service
                <span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>
            </a>
            </li>
            <li class="text-lg py-2 cursor-pointer">No online shopping for the moment</li>
        </ul>
    </div>
    </div>
    <div class=" w-full bg-sis-white text-sis-grey flex md:flex-row flex-col py-2 items-center justify-center md:justify-evenly">
        <p class="lg:py-0 md:py-8 py-0 md:px-8 px-0 pt-6 md:text-xl text-lg rische">Copyright 2023 - SIS Store&copy;</p>
        <p class=" md:px-8 px-0 md:text-xl text-lg  rische">Website by : <a target="_blank" href="https://www.instagram.com/rinimisini/" class="angle-font font-semibold border-b border-sis-grey">Rini Misini</a></p>
    </div>
</footer>
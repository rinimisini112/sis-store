<nav id="navbar" class=" duration-200 z-30 fixed top-0 left-0 text-sis-white  flex md:px-16 px-4 lg:py-2 py-4 justify-between items-center bg-opacity-10 w-full">
    <a href="index.php" class=" relative z-[100]">
        <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
    </a>
    <div class='block lg:hidden' id='openPhoneMenuIcon'>
        <button class='relative group z-50'>
            <div class='flex flex-col justify-between w-[50px] h-[50px] transform transition-all duration-300 origin-center overflow-hidden p-2'>
                <div class='burger_line bg-sis-white h-[5px] w-10 transform transition-all duration-300 origin-left'></div>
                <div class='burger_line bg-sis-white h-[5px] w-10 rounded transform transition-all duration-300 delay-75'></div>
                <div class='burger_line bg-sis-white h-[5px] w-10 transform transition-all duration-300 origin-left delay-150'></div>
            </div>
        </button>
    </div>
    <div class="fixed top-0 left-0 w-[80%] max-w-0 h-full flex-col flex justify-between bg-sis-grey text-sis-white z-50 transition-all duration-100 overflow-hidden pt-16" id="phoneMenu">
        <ul class=" text-3xl pb-6 mt-4 transition-colors duration-300">
            <li id="collectionsLi" class="py-4 pl-3 border-b-2 border-t-2 relative"><span class="flex items-center gap-8">Collections
                    <svg id='phoneCollectionsIcon' xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                        <polyline points="6 9 12 15 18 9"></polyline>
                    </svg>
                </span>
                <div id="collectionsPhoneDropdown" style="max-height: 0; overflow: hidden; transition: max-height 0.3s ease-out;">
                    <ul class="w-[95%] pt-4">
                        <li class="py-3 pl-3 border-b border-b-sis-white"><a href="collections.php">New Collections</a></li>
                        <li class="py-3 pl-3 border-b border-b-sis-white"><a href="vintageCollection.php">Vintage Collection</a></li>
                        <li class="py-3 pl-3"><a href="jewelries.php">Jewelry</a></li>
                    </ul>
                </div>
            </li>
            <li class="py-4 pl-3 border-b-2"><a href="aboutUs.php">About Us</a></li>
            <li class="py-4 pl-3 border-b-2"><a href="blog.php">Blog</a></li>
            <li class="py-4 pl-3 border-b-2"><a href="contactUs.php">Contact Us</a></li>
        </ul>
        <ul class=" p-2 md:text-2xl text-xl text-center w-full">
            <li class="flex flex-col gap-2 items-center justify-between border-b-2">
                <span class="text-2xl">
                    Mobile
                </span> <a href="tel:+38343555450">043 555 450
                </a>
            </li>
            <li class="flex flex-col gap-3 mt-2 items-center justify-between">
                <span class="text-2xl">
                    Whatsapp </span> <a href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                </a>
            </li>
        </ul>
    </div>
    <ul class="w-[60%] items-center justify-around text-xl uppercase lg:flex hidden">
        <li class="px-8 w-[30%] relative group">
            <p id='toggleCollections' class="relative cursor-pointer flex justify-between items-center">Collections
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                    <polyline points="6 9 12 15 18 9"></polyline>
                </svg>
            </p>
            <div class="absolute shadow-lg left-0 w-full  " id="collectionsDropdown" style="visibility: hidden; z-index: -50; opacity: 0;">
                <ul class="w-full">
                    <a href="collections.php">
                        <li class="hover:bg-[#1e1e1ebc] duration-200 py-4 pl-4 border-b ">New Collections</li>
                    </a>
                    <a href="vintageCollection.php">
                        <li class="hover:bg-[#1e1e1ebc] duration-200 py-4 pl-4 border-b">Vintage Collection</li>
                    </a>
                    <a href="jewelries.php">
                        <li class="hover:bg-[#1e1e1ebc] duration-200 py-4 pl-4">Jewelry</li>
                    </a>
                </ul>
            </div>
        </li>
        <li class="pl-8 w-[30%] relative group">
            <a href="aboutUs.php" class="relative">About Us
                <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-white"></span>
            </a>
        </li>
        <li class="pl-8 w-[30%] relative group">
            <a href="blog.php" class="relative">Blog
                <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-white"></span>
            </a>
        </li>
        <li class="pl-8 w-[30%] relative group">
            <a href="contactUs.php" class="relative">Contact Us
                <span class="duration-200 w-0 h-px absolute -bottom-1 left-0 group-hover:w-full bg-sis-white"></span>
            </a>
        </li>
    </ul>
</nav>
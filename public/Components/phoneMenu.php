<div class="fixed top-0 left-0 w-[80%] max-w-0 h-full flex-col flex justify-between bg-sis-white text-sis-grey z-40 transition-all duration-100 overflow-hidden pt-16" id="phoneMenu">
<ul class=" text-3xl pb-6 mt-4 transition-colors duration-300">
            <li id="collectionsLi" class="py-4 pl-3 border-b-2 border-t-2 border-sis-grey relative"><span class="flex items-center gap-8">Collections
                    <svg id='phoneCollectionsIcon' xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down" id="chevronIcon">
                        <polyline points="6 9 12 15 18 9"></polyline>
                    </svg>
                </span>
                <div id="collectionsPhoneDropdown" style="max-height: 0; overflow: hidden; transition: max-height 0.3s ease-out;">
                    <ul class="w-[95%] pt-4">
                        <li class="py-3 pl-3 border-b border-b-sis-grey"><a href="collections.php">New Collections</a></li>
                        <li class="py-3 pl-3 border-b border-b-sis-grey"><a href="vintageCollection.php">Vintage Collection</a></li>
                        <li class="py-3 pl-3"><a href="jewelries.php">Jewelry</a></li>
                    </ul>
                </div>
            </li>
            <li class="py-4 pl-3 border-b-2 border-b-sis-grey"><a href="aboutUs.php">About Us</a></li>
            <li class="py-4 pl-3 border-b-2 border-b-sis-grey"><a href="blog.php">Blog</a></li>
            <li class="py-4 pl-3 border-b-2 border-b-sis-grey"><a href="contactUs.php">Contact Us</a></li>
        </ul>
                <ul class=" p-2 md:text-2xl text-xl text-center w-full">
                    <li class="flex flex-col gap-2 items-center justify-between border-b-2 border-sis-grey pb-2">
                        <span class="text-2xl">
                            Mobile
                        </span> <a href="tel:+38343555450">043 555 450
                        </a>
                    </li>
                    <li class="flex flex-col gap-3 mt-2 items-center justify-between">
                        <span class="text-2xl">
                            Whatsapp </span> <a href="https://wa.me/+38343555450" target="_blank">+383 43 555 450
                        </a>
                    </li>
                </ul>
            </div>
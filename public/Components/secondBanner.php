<?php
include 'public/Classes/Dbh.php';
include 'public/Classes/CMS.php';
$conn = new Dbh();
$CMS = new CMS($conn);
$fetchIds = $CMS->fetchCmsID();
$secondBanerFirstImg = $fetchIds['secondBanerFirstImg'];
$secondBanerSecondImg = $fetchIds['secondBanerSecondImg'];
$fetchTwoitems = $CMS->fetchSecondBannerProducts($secondBanerFirstImg, $secondBanerSecondImg);
?>

<div class="flex items-center justify-evenly mx-auto w-full pb-3" id="banner2">
<div class="w-[45%] h-screen py-2 relative group">
    <a href="public/collections/product.php?id=<?=$fetchTwoitems['pr_id1']?>">
    <img class="object-cover w-full h-full" src="resources/images/<?php echo $fetchTwoitems['image_path1']; ?>" alt="Image of a model with a cool sweater on">
    </a>
    <a href="public/collections/product.php?id=<?=$fetchTwoitems['pr_id1']?>">
    <p class="flex items-center horizontal-txt absolute top-16 md:left-4 left-2 cursor-pointer text-sis-grey lg:text-3xl md:text-4xl text-3xl graphicus-regular">
        <?php echo $fetchTwoitems['product_name1']; ?>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-down-right group-hover:w-12 duration-300">
            <polyline points="15 10 20 15 15 20"></polyline>
            <path d="M4 4v7a4 4 0 0 0 4 4h12"></path>
        </svg>
    </p>
    </a>
</div>
<div class="w-[45%] h-screen py-2 relative group">
<a href="public/collections/product.php?id=<?=$fetchTwoitems['pr_id2']?>">
    <img class="object-cover w-full h-full" src="resources/images/<?php echo $fetchTwoitems['image_path2']; ?>" alt="Image of a model with a cool sweater on">
    </a>
    <a href="public/collections/product.php?id=<?=$fetchTwoitems['pr_id2']?>">
    <p class="flex items-center horizontal-txt absolute top-16 md:left-4 left-2 cursor-pointer text-sis-white lg:text-3xl md:text-4xl text-3xl graphicus-regular">
        <?php echo $fetchTwoitems['product_name2']; ?>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-down-right group-hover:w-12 duration-300">
            <polyline points="15 10 20 15 15 20"></polyline>
            <path d="M4 4v7a4 4 0 0 0 4 4h12"></path>
        </svg>
    </p>
    </a>
</div>
</div>
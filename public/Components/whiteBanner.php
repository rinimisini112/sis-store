<?php 
$fetchIds = $CMS->fetchCmsID();
$whiteBanerFirstImg = $fetchIds['whiteBanerFirstImg'];
$whiteBannerItems = $CMS->fetchWhiteBannerProducts($whiteBanerFirstImg);
?>
<div class="flex md:w-[90%] w-full mx-auto justify-evenly py-16 bg-sis-white" id="white_banner">
    <div class=" w-[20%] md:block hidden h-[600px] relative">
        <a href="public/collections/product.php?id=<?= $whiteBannerItems['product_id'] ?>">
            <img class="object-cover w-full h-full " src="resources/images/<?= $whiteBannerItems['image1'] ?>" alt="Image of a model in a pink japanese inspired kimono">
        </a>
        <p class="text-3xl absolute bottom-2 right-0 md:p-2 p-0  text-sis-grey font-semibold horizontal-txt ">Japanese Inspired</p>
    </div>
    <div class=" w-[30%] h-[600px] relative">
        <a href="public/collections/product.php?id=<?= $whiteBannerItems['product_id'] ?>">
            <img class="object-cover w-full h-full" src="resources/images/<?= $whiteBannerItems['image2'] ?>" alt="Image of a model in a pink japanese inspired kimono">
        </a>
        <p class=" text-right text-3xl absolute bottom-0 right-0 md:p-4 p-2 text-sis-grey font-semibold horizontal-txt ">Cherry Blossom Kimono</p>
    </div>
    <div class=" w-[40%]  h-[600px] relative">
        <a href="public/collections/product.php?id=<?= $whiteBannerItems['product_id'] ?>">
            <img class="object-cover w-full h-full" src="resources/images/<?= $whiteBannerItems['main_image'] ?>" alt="Image of a model in a pink japanese inspired kimono">
        </a>
        <p class=" text-right text-3xl absolute bottom-0 right-0 md:p-4 p-2 text-sis-grey font-semibold horizontal-txt ">Browse Now!</p>
    </div>
</div>
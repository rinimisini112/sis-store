<div>
            <p class=" md:text-3xl text-xl text-sis-grey flex items-center justify-between md:px-24 px-4">Lady in Black Set
                <a href="public/collections/product.php?id=7" class="bg-sis-white border-sis-grey border hover:bg-sis-grey hover:text-sis-white duration-200 text-sis-grey rische py-2 md:px-16 px-12 flex items-center justify-center">
                    View
                </a>
            </p>
            <div class="overflow-hidden relative w-full py-4 bg-sis-white">
                <div class="carousel-track duration-500 ease-in-out" id="track">
                    <img class="carousel-item object-cover h-[450px] " src="resources/images/sis_banner-1-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-5-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-3-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px] " src="resources/images/sis_banner-4-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-2-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-6-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-7-min.jpg" alt="A carousel of images with a model on with a black suit on">

                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-1-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-2-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-3-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-4-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-5-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-6-min.jpg" alt="A carousel of images with a model on with a black suit on">
                    <img class="carousel-item object-cover h-[450px]" src="resources/images/sis_banner-7-min.jpg" alt="A carousel of images with a model on with a black suit on">

                </div>
            </div>
        </div>
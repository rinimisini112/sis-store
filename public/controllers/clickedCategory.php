<?php
$conn = new Dbh();
$products = new CategoryHandler($conn);

$items = $products->fetchCategories();
if(isset($_SESSION['filterVals'])) {
 $filter = '&filter=remove';
} else {
    $filter = '';
}
foreach ($items as $product) {
    echo "<li class='py-1.5 col-span-1 lg:text-xl text-md font-bold text-sis-grey'>";
    echo "<a href='categories.php?cg={$product['category_id']}{$filter}' class='relative group rische'>";

    // Check if the category ID in the URL matches the current category ID
    $currentCategoryId = isset($_GET['cg']) ? $_GET['cg'] : null;
    $isActive = ($currentCategoryId == $product['category_id']) ? 'w-full' : '';

    echo "{$product['category_name']}";
    echo "<span class='absolute bottom-0 left-0 duration-200 {$isActive} h-px group-hover:w-full bg-sis-grey'></span>";
    echo "</a>";
    echo "</li>";
}

?>
<?php
foreach ($itemsSubset as $product) {
    // Escape and encode product_id
    $productId = htmlspecialchars($product['product_id'], ENT_QUOTES, 'UTF-8');

    echo "<div class='md:w-[50%] w-full lg:h-screen h-auto lg:px-4 px-6 lg:mb-0 mb-4 relative'>";
    echo "<div class='relative lg:h-[90%] h-[85%] group'>";
    if(isset($product['images'][0]['main_image'])) {
    echo "<a  href='product.php?id={$productId}'><img src='resources/images/{$product['images'][0]['main_image']}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
    } else {
    echo "<a  href='product.php?id={$productId}'><img src='resources/images/{$product['main_image']}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
    }
if(isset($product['image1'])){
        echo "<img src='resources/images/{$product['image1']}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
    }
    if(isset($product['images'][0]['image1'])) {
        echo "<img src='resources/images/{$product['images'][0]['image1']}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
    }
    echo "</a>";
    echo "</div>";
    echo "<h2 class='lg:text-2xl pb-4 lg:pb-0 text-lg pt-2 font-bold flex items-center justify-between'><a href='public/collections/product.php?id={$productId}'>{$product['product_name']}</a>" .
    "<p class='md:pr-4 pr-0 pl-3 font-semibold'>{$product['price']}€</p></h2>";
    echo "</div>";
}
if( isset($_POST['filterClothes']) && empty($items)) {
    echo "<p class='text-3xl text-center py-16 w-full'>No items found matching your filtering</p>";
}


// Output the pagination links
?>

<?php
require 'public/Classes/Dbh.php';
require 'public/Classes/FaqHandler.php';

$conn = new Dbh();
$faq = new FaqHandler($conn);
$questions = $faq->readFAQ();
foreach ($questions as $question) {
    echo   "<section class='lg:w-[80%] w-full mx-auto accordion-section  cursor-pointer'>";
    echo        "<p class='text-sis-white  bg-sis-grey p-4 lg:text-2xl text-xl font-semibold flex items-center justify-between accordion-box duration-200 transition-colors'>" . $question['question'];
    echo           "<img src='resources/SVG/chevronDown.svg' alt='' class='accordion-img duration-300'>";
    echo      "</p>";
    echo    "<div class='accordion-content transition-all duration-300 px-4 overflow-hidden'>";
    echo        "<div class='p-4 bg-[#f2ede6]'>";
    echo          "<p class=' lg:text-xl text-lg'>" . $question['answer'] . "</p>";
    echo        "</div>";
    echo    "</div>";
    echo  "</section>";
}

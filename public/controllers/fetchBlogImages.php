<?php 
require 'public/Classes/Post.php';

$posts = new Post();

$postImages = $posts->fetchAllPosts();

foreach($postImages as $image) {
  echo " <div class='w-[350px] drop-shadow-xl relative shadow-md overflow-hidden h-[300px] flex-shrink-0 group duration-200'>";
  echo  "<img src='resources/images/" . $image->blog_image .  "' alt='' class='transform h-auto object-contain max-w-full group-hover:scale-110  duration-200'>";
echo  "<span class='w-full h-full bg-[#000000] absolute top-0 left-0 z-20 bg-opacity-40 hover:bg-opacity-0 duration-300'></span>";                
echo "</div>";
}
?>
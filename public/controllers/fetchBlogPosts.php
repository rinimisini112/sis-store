<?php

$postsResult = $posts->fetchLatestPosts();
function truncateContent($content, $maxLength = 200) {
    // Check if the content length is greater than the maximum length
    if (mb_strlen($content) > $maxLength) {
        // Truncate the content and add ellipsis
        $truncatedContent = mb_substr($content, 0, $maxLength) . '...';
        return $truncatedContent;
    }

    // If the content is already within the limit, return it as is
    return $content;
}

foreach ($postsResult as $post) {
    $truncatedContent = truncateContent($post->content);
    echo   "<div class='w-full bg-[rgb(208,208,208)] drop-shadow-lg rounded-sm flex'>";
    echo  "<img src='resources/images/" . $post->blog_image . "' alt='' class='w-[400px] h-[500px] object-cover'>";
    echo "<div class='w-3/5'>";
    echo  "<p class=' text-4xl rische p-4 bg-gradient-to-tr from-stone-900 via-stone-800 to-black text-sis-white'>" . $post->title . "</p>";
    echo "<p class='flex items-center p-4 justify-between'><span class='flex text-xl'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-user'>";
    echo   "<path d='M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2'></path>";
    echo   "<circle cx='12' cy='7' r='4'></circle>";
    echo    "</svg>: " . $post->author . "";
    echo    "</span>";
    echo    "<span class='flex text-xl'>";
    echo      "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-calendar'>";
    echo          "<rect x='3' y='4' width='18' height='18' rx='2' ry='2'></rect>";
    echo          "<line x1='16' y1='2' x2='16' y2='6'></line>";
    echo          "<line x1='8' y1='2' x2='8' y2='6'></line>";
    echo          "<line x1='3' y1='10' x2='21' y2='10'></line>";
    echo       "</svg> - " . $post->published_at ."</span>";
    echo      "</p>";
    echo      "<p class='px-12 pt-4 text-xl'>" . $truncatedContent . "</p>";
    echo      "<a href='' class=' px-16 py-3 text-lg font-bold bg-gradient-to-tr from-stone-900 via-stone-800 to-black text-sis-white ml-12 mt-12 inline-block'>Continue Reading</a>";
    echo          "<p class='flex items-center gap-8 text-2xl px-12 mt-12'><span>Likes : " . $post->likes . "</span> <svg xmlns='http://www.w3.org/2000/svg' width='34' height='34' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-heart'><path d='M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z'></path></svg></p>";
    echo     "</div>";
    echo    "</div>";
}

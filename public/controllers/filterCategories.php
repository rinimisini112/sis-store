<?php 
    foreach ($itemsSubset as $product) {
        // Escape and encode product_id
        $productId = htmlspecialchars($product['product_id'], ENT_QUOTES, 'UTF-8');
    
        echo "<div class='md:w-[50%] w-full lg:h-screen h-auto lg:py-0 py-4 mb-4 lg:px-4 px-4 relative'>";
        echo "<div class='relative lg:h-[90%] h-[400px] group'>";
        
        if(!empty($product['images'][0]['main_image'])) {
            echo "<a  href='product.php?id={$productId}'><img src='resources/images/{$product['images'][0]['main_image']}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
        }
        if(!empty($product['main_image'])) {
            echo "<a  href='product.php?id={$productId}'><img src='resources/images/{$product['main_image']}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
        }
        if(!empty($product['images'][0]['image1'])) {
            echo "<img src='resources/images/{$product['images'][0]['image1']}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
        }
        if(!empty($product['image1'])) {
            echo "<img src='resources/images/{$product['image1']}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
        }
        echo "</a>";
        echo "</div>";
        echo "<h2 class='lg:text-2xl text-lg pt-2 font-bold flex items-center justify-between'><a href='product.php?id={$productId}'>{$product['product_name']}</a>" .
        "<p class='pl-2 font-semibold'>{$product['price']}€</p></h2>";
        echo "</div>";
    }
 if( isset($_POST['filterClothes']) && empty($itemsSubset)) {
    echo "<p class='text-3xl text-center py-16 w-full'>No items found matching your filtering</p>";
} else if (isset($_SESSION['noItemsMssg'])) {
    echo "<p class='text-3xl text-center py-16 w-full'>{$_SESSION['noItemsMssg']}.</p>";
    unset($_SESSION['noItemsMssg']);
}
?>
<?php 
session_start();
include '../Classes/Dbh.php';
include '../Classes/ProductFilter.php';
include '../Classes/CategoryHandler.php';
$conn = new Dbh();
$productsByCategory = new CategoryHandler($conn);


if (isset($_POST['filterClothes'])) {
    if(isset($_POST['cg'])) {
        $cName = $_POST['cg'];
    }
    if(isset($_POST['sort']) && !empty($_POST['sort'])) {
        $sort = '&sort=' . $_POST['sort'];
    } else {
        $sort = '';
    }
        $filters = $_POST;
        $items = $productsByCategory->fetchFilteredProducts($cName, $filters);
        $_SESSION['categoriesPriceRange'] = $_POST['priceRange'];
        $_SESSION['categoriesSelectedSizes'] = $_POST;
        $_SESSION['filteredCatItems'] = $items;
        $sortParam = !empty($sort) ? $sort : '';

        header('Location: ../collections/categories.php?cg=' . $cName . '&filtered=true' . $sortParam);
    }
    if (isset($_POST['removefilter'])) {
        $cName = $_POST['cg'];
        $sort = isset($_POST['sort']) ? '&sort=' . $_POST['sort'] : '';
        unset($_SESSION['categoriesSelectedSizes']);
        unset($_SESSION['categoriesPriceRange']);
        unset($_SESSION['filteredCatItems']);
        $sortParam = !empty($sort) ? $sort : '';

        header('Location: ../collections/categories.php?cg=' . $cName . '&filtered=true' . $sortParam);    }
    
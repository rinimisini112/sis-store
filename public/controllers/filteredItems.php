<?php
include '../Classes/Dbh.php';
include '../Classes/ProductHandler.php';
include '../Classes/ProductFilter.php';
 session_start();
$conn = new Dbh();
$products = new ProductHandler($conn);

if (isset($_POST['filterClothes'])) {
    try {
        $filters = $_POST;
        $sizes[] = isset($_POST['size2XL']) ? $_POST['size2XL'] : '';
        $sizes[] = isset($_POST['sizeXL']) ? $_POST['sizeXL'] : '';
        $sizes[] = isset($_POST['sizeL']) ? $_POST['sizeL'] : '';
        $sizes[] = isset($_POST['sizeM']) ? $_POST['sizeM'] : '';
        $sizes[] = isset($_POST['sizeS']) ? $_POST['sizeS'] : '';
        $sizes[] = isset($_POST['sizeXS']) ? $_POST['sizeXS'] : '';
        $sizes[] = isset($_POST['size31']) ? $_POST['size31'] : '';
        $sizes[] = isset($_POST['size32']) ? $_POST['size32'] : '';
        $sizes[] = isset($_POST['size33']) ? $_POST['size33'] : '';
        $sizes[] = isset($_POST['size34']) ? $_POST['size34'] : '';
        $sizes[] = isset($_POST['size36']) ? $_POST['size36'] : '';
        $sizes[] = isset($_POST['size38']) ? $_POST['size38'] : '';
        $sizes[] = isset($_POST['size40']) ? $_POST['size40'] : '';
        if (empty($filters)) {
            throw new Exception('Please select your filters');
        }
        // Check if products exist with the selected sizes
        if ($products->sizeDoesNotExist($sizes)) {
            throw new Exception('No products found with the selected sizes');
        }
        $items = $products->fetchFilteredProducts($filters);
        if (!$items) {
            throw new Exception('No items found matching your filtering');
        }
        $_SESSION['priceRange'] = $_POST['priceRange'];
        $_SESSION['selectedSizes'] = $_POST;
        $_SESSION['filteredItems'] = $items; // Store $items in the session
        header('Location: ../../collections.php?filtered=true');
    } catch (Exception $e) {
        $_SESSION['vMssg'] = $e->getMessage();
        header('Location: ../../collections.php');
    }
}

if (isset($_POST['removefilter'])) {
    unset($_SESSION['selectedSizes']);
    unset($_SESSION['priceRange']);
    unset($_SESSION['filteredItems']); // Remove the filtered items from the session
    header('Location: ../../collections.php');
}

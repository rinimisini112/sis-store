

    <?php
    session_start();
    include 'public/Classes/Dbh.php';
    include 'public/Classes/ProductFilter.php';
    include 'public/Classes/CategoryHandler.php';

    if (isset($_GET['cg'])) {
        try {
        if (isset($_GET['filter'])) {
            if ($_GET['filter'] == 'remove') {
                unset($_SESSION['categoriesSelectedSizes']);
                unset($_SESSION['categoriesPriceRange']);
                unset($_SESSION['filterVals']);
            }
        }
        $cName = $_GET['cg'];
        $sort = isset($_GET['sort']) ? $_GET['sort'] : 'newest';
        $sortMessage = '';

        switch ($sort) {
            case 'price_desc':
                $sortMessage = 'Price High to Low';
                break;
            case 'price_asc':
                $sortMessage = 'Price Low to High';
                break;
            case 'newest':
                $sortMessage = 'Newest';
                break;
            case 'oldest':
                $sortMessage = 'Oldest';
                break;
            default:
                $sortMessage = 'Newest';
                break;
        }

        $conn = new Dbh();
        $productsByCategory = new CategoryHandler($conn);
        try 
        {
        $categoryInfo = $productsByCategory->fetchCategoriesWithId($cName);
        $items = $productsByCategory->filterProductsByCategory($cName, $sort);
        if(isset($_POST['filterClothes']) && empty($items)) {
         }
        if(empty($items)) {
            throw new Exception('No products Found');
            echo "No items found";
        }
    } catch(Exception $e) {
            $_SESSION['noItemsMssg'] = 'No items found for this category';
        }

        try {  
        if (isset($_POST['filterClothes'])) {
            if (isset($_GET['cg'])) {
                $cName = $_GET['cg'];
            }
            $filters = $_POST;
            
            if(empty($filters)) {
                throw new Exception('Please select your filtering options');
                echo "Please select your filtering options";
            }
            
            $_SESSION['filterVals'] = $filters;
            $_SESSION['categoriesPriceRange'] = $_POST['price'];
            $_SESSION['categoriesSelectedSizes'] = $_POST;
        } 
        //nese prekim remove filter heki krejt vlerat pej session
        if (isset($_POST['removefilter'])) {
            unset($_SESSION['categoriesSelectedSizes']);
            unset($_SESSION['categoriesPriceRange']);
            unset($_SESSION['filterVals']);
            header("Location: categories.php?cg=" . $cName);
        }
        } catch (Exception $e) {
            echo "Could not get Filtered Items";
        }
                //nese vlerat pej postit jane te deklarume vazhdo me filtrim tprodukteve
        if (isset($_SESSION['filterVals'])) {
            //merri krejt produktet per qat kategori
            $allItems = $productsByCategory->filterProductsByCategory($cName, $sort);
            //nbaz tkrejt produkteve merri produktet qe i pershtaten filtrimit
            $items = $productsByCategory->fetchFilteredProducts($cName, $allItems, $_SESSION['filterVals']);
            //nese osht e deklarume sort nurl sortoj produktet e filtrume
            if(isset($_GET['sort'])) {
                $items = $productsByCategory->sortFilteredCategories($items,$cName,$sort);
            }
        }
    } catch(Exception $e) {  
        $_SESSION['noItemsToFilter'] = 'Could not filter items, no items found for this category';
   
    }
        //nese klikojm nkategori tjera heki filtrimet 
    } else {
        //nese klikon ne categori pa id tkategoris ktheje ncollections
        header("Location: collections.php");
    }

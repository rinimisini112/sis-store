<?php
include "public/Classes/CategoryHandler.php";
$conn = new Dbh();
$products = new CategoryHandler($conn);

$items = $products->fetchCategoriesForFooter();

foreach ($items as $product) {
    echo    "<li class='text-lg py-2'>";
    echo "<a href='public/collections/categories.php?cg={$product['category_id']}' class='relative group'>";
    echo "{$product['category_name']}";
    echo "<span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>";
    echo "</a>";
    echo "</li>";
}
echo "<li class='text-lg py-2'>";
echo "<a href='collections.php' class='relative group'>";
echo "View All...";
echo "<span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-white'></span>";
echo "</a>";
echo "</li>";
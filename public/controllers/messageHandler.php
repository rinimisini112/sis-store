<?php

require '../Classes/Dbh.php';
require '../Classes/MessageHandler.php';
session_start();

function containsBadWords($message, $badWords)
{
    $message = strtolower($message); // Convert to lowercase for case-insensitive comparison

    foreach ($badWords as $badWord) {
        if (stripos($message, $badWord) !== false) {
            return true; // Message contains a bad word
        }
    }

    return false; // Message is clean
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Define your list of bad words
    $badWords = [
        'fuck', 'shit', 'bitch','kurv','mut','qifsha','robt','pidh','kurva'];

    // Get user input
    $userName = filter_input(INPUT_POST, 'fullName', FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $userMessage = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_SPECIAL_CHARS);

    // Check for bad words
    if (containsBadWords($userMessage, $badWords)) {
        // The message contains bad words, handle accordingly
        header("Location: ../../contactUs.php");
        $_SESSION['message'] = 'Your message contains inappropriate language. Please revise your message.';
    } else {
        // The message is clean, proceed with sending it
        $conn = new Dbh();
        $messageHandler = new MessageHandler($conn);

        $messageHandler->setName($userName);
        $messageHandler->setEmail($email);
        $messageHandler->setMessage($userMessage);
        $result = $messageHandler->sendMessage();
        if (!$result) {
            header("Location: ../../contactUs.php");
            $_SESSION['message'] = 'Thank you for writing to us. We will get in touch with you as soon as we can.';
        } else {
            $_SESSION['message'] = 'Could not send the message. Please try again.';
            header("Location: ../../contactUs.php");
        }
    }
}
?>
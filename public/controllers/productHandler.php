<?php
include 'public/Classes/Dbh.php';
include 'public/Classes/ProductHandler.php';
include 'public/Classes/ProductFilter.php';
session_start();
$conn = new Dbh();
$products = new ProductHandler($conn);

$sort = isset($_GET['sort']) ? $_GET['sort'] : 'newest';
$sortMessage = '';

switch ($sort) {
    case 'price_desc':
        $sortMessage = 'Price High to Low';
        break;
    case 'price_asc':
        $sortMessage = 'Price Low to High';
        break;
    case 'newest':
        $sortMessage = 'Newest';
        break;
    case 'oldest':
        $sortMessage = 'Oldest';
        break;
    default:
        $sortMessage = 'Newest'; 
        break;
}

?>
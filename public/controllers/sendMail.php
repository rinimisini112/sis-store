<?php
include "../Classes/Mail.php";
session_start();
if(isset($_POST['subsribe'])) {
$email = $_POST['email'];
$mail = new Mail();

$sendMail = $mail->insertNewsletter($email);
if(!$sendMail) {
throw new Exception('Could not send mail');
header("Location: ../../index.php");
return;
}
try {
$to = $email;
$subject = 'Welcome to SIS Store Newsletter';
$message = 'Thank you for subscribing to our newsletter, we will stay in touch with you and keep you notified about news in our store!';
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: SIS Store <info@sisx.store>' . "\r\n";

// Additional headers if needed
// $headers .= 'Cc: another@example.com' . "\r\n";
// $headers .= 'Bcc: third@example.com' . "\r\n";

if (mail($to, $subject, $message, $headers)) {
    $_SESSION['mailMssg'] = 'Thank you for subscribing';
    header("Location: ../../index.php#footer");
} else {
    $_SESSION['mailMssg'] = 'Could not proccess subscription.';
}
} catch(Exception $e) {
    echo 'Mail Error :'. $e->getMessage();
}
}
?>
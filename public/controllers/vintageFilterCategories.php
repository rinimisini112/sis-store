<?php 
     foreach ($itemsSubset as $product) {
        // Escape and encode product_id
        $productId = htmlspecialchars($product['vintage_id'], ENT_QUOTES, 'UTF-8');
    
        echo "<div class='md:w-[48%] bg-vintage-white  shadow-xl w-full lg:h-screen h-auto lg:py-0 py-4 lg:mb-12 mb-12 lg:px-4 px-4 relative'>";
        echo "<div class='relative lg:h-[90%] h-[400px] group'>";
        
        if(!empty($product['vimages'][0]['vmain_image'])) {
            echo "<a  href='vintageProduct.php?id={$productId}'><img src='resources/images/{$product['vimages'][0]['vmain_image']}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
        }
        if(!empty($product['vmain_image'])) {
            echo "<a  href='vintageProduct.php?id={$productId}'><img src='resources/images/{$product['vmain_image']}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
        }
        if(!empty($product['vimages'][0]['vimage1'])) {
            echo "<img src='resources/images/{$product['vimages'][0]['vimage1']}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
        }
        if(!empty($product['vimage1'])) {
            echo "<img src='resources/images/{$product['vimage1']}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
        }
        if(isset($_SESSION['categoriesVintagePriceRange'])) {
            $price = $product['price'];
        } else {
            $price = $product['v_price'];
        }
        echo "</a>";
        echo "</div>";
        echo "<h2 class='lg:text-2xl text-lg pt-2 font-bold flex items-center justify-between'><a href='product.php?id={$productId}'>{$product['v_name']}</a>" .
        "<p class='pl-2 font-semibold'>{$price}€</p></h2>";
        echo "</div>";
    }
 if( isset($_POST['filterClothes']) && empty($itemsSubset)) {
    echo "<p class='text-3xl text-center py-16 w-full'>No items found.</p>";
}
?>
<?php
 session_start();
include '../Classes/Dbh.php';
include '../Classes/VintageHandler.php';
include '../Classes/VintageFilter.php';
$conn = new Dbh();
$products = new VintageHandler($conn);

if (isset($_POST['filterVintageClothes'])) {
    try {
        $vintageFilters = $_POST;
        $sizes[] = isset($_POST['size2XL']) ? $_POST['size2XL'] : '';
        $sizes[] = isset($_POST['sizeXL']) ? $_POST['sizeXL'] : '';
        $sizes[] = isset($_POST['sizeL']) ? $_POST['sizeL'] : '';
        $sizes[] = isset($_POST['sizeM']) ? $_POST['sizeM'] : '';
        $sizes[] = isset($_POST['sizeS']) ? $_POST['sizeS'] : '';
        $sizes[] = isset($_POST['sizeXS']) ? $_POST['sizeXS'] : '';
        $sizes[] = isset($_POST['size32']) ? $_POST['size32'] : '';
        $sizes[] = isset($_POST['size31']) ? $_POST['size31'] : '';
        $sizes[] = isset($_POST['size34']) ? $_POST['size34'] : '';
        if (empty($vintageFilters)) {
            throw new Exception('Please select your filters');
        }
        // Check if products exist with the selected sizes
        if ($products->sizeDoesNotExist($sizes)) {
            throw new Exception('No products found with the selected sizes');
        }

        $items = $products->fetchFilteredVintageProducts($vintageFilters);

        if (!$items) {
            throw new Exception('No items found, matching your filtering');
            $_SESSION['vMssg'] = "No items were found matching your filters";
        }

        $_SESSION['priceVintageRange'] = $_POST['v_price'];
        $_SESSION['selectedVintageSizes'] = $_POST;
        $_SESSION['filteredVintageItems'] = $items;
        header('Location: ../../vintageCollection.php?filtered=true');
    } catch (Exception $e) {
        $_SESSION['vMssg'] = $e->getMessage();
        header('Location: ../../vintageCollection.php');
    }
}   

if (isset($_POST['removefilter'])) {
    unset($_SESSION['selectedVintageSizes']);
    unset($_SESSION['priceVintageRange']);
    unset($_SESSION['filteredVintageItems']); // Remove the filtered items from the session
    header('Location: ../../vintageCollection.php');
}

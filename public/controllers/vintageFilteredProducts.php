
<?php
    session_start();
    include 'public/Classes/Dbh.php';
    include 'public/Classes/VintageFilter.php';
    include 'public/Classes/VintageCategoryHandler.php';

    if (isset($_GET['cg'])) {
        if (isset($_GET['filter'])) {
            if ($_GET['filter'] == 'remove') {
                unset($_SESSION['categoriesVintageSelectedSizes']);
                unset($_SESSION['categoriesVintagePriceRange']);
                unset($_SESSION['filterVintageVals']);
            }
        }
        $cName = $_GET['cg'];
        $sort = isset($_GET['sort']) ? $_GET['sort'] : 'newest';
        $sortMessage = '';

        switch ($sort) {
            case 'price_desc':
                $sortMessage = 'Price High to Low';
                break;
            case 'price_asc':
                $sortMessage = 'Price Low to High';
                break;
            case 'newest':
                $sortMessage = 'Newest';
                break;
            case 'oldest':
                $sortMessage = 'Oldest';
                break;
            default:
                $sortMessage = 'Newest';
                break;
        }

        $conn = new Dbh();
        $productsByCategory = new VintageCategoryHandler($conn);
        try 
        {
        $categoryInfo = $productsByCategory->fetchCategoriesWithId($cName);
        $items = $productsByCategory->filterVintageProductsByCategory($cName, $sort);
        if(isset($_POST['filterClothes']) && empty($items)) {
            throw new Exception('No items found matching your filters for this category');
         }
        if(empty($items)) {
            throw new Exception('No items found for this category');
        }
 
        if (isset($_POST['filterClothes'])) {
            if (isset($_GET['cg'])) {
                $cName = $_GET['cg'];
            }
            $vintageFilters = $_POST;
            
            if(empty($vintageFilters)) {
                throw new Exception('Please select your filtering options');
            }
            
            $_SESSION['filterVintageVals'] = $vintageFilters;
            $_SESSION['categoriesVintagePriceRange'] = $_POST['v_price'];
            $_SESSION['categoriesVintageSelectedSizes'] = $_POST;
        } 
        //nese prekim remove filter heki krejt vlerat pej session
        if (isset($_POST['removefilter'])) {
            unset($_SESSION['categoriesVintageSelectedSizes']);
            unset($_SESSION['categoriesVintagePriceRange']);
            unset($_SESSION['filterVintageVals']);
            header("Location: vintageCategories.php?cg=" . $cName);
        }
                //nese vlerat pej postit jane te deklarume vazhdo me filtrim tprodukteve
        if (isset($_SESSION['filterVintageVals'])) {
            //merri krejt produktet per qat kategori
            $allItems = $productsByCategory->filterVintageProductsByCategory($cName, $sort);
if(empty($allItems)) {
    throw new Exception('No items found for these filters');
}
            $items = $productsByCategory->fetchFilteredProducts($cName, $allItems, $_SESSION['filterVintageVals']);
            //nese osht e deklarume sort nurl sortoj produktet e filtrume
            if(isset($_GET['sort'])) {
                $items = $productsByCategory->sortFilteredCategories($items,$cName,$sort);
            }
        }
    } catch(Exception $e) {  
        $_SESSION['noItemsToFilter'] = $e->getMessage();
    }
        //nese klikojm nkategori tjera heki filtrimet 
    } else {
        //nese klikon ne categori pa id tkategoris ktheje ncollections
        header("Location: vintageCollection.php");
    }

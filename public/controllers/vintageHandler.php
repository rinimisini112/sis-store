<?php
include 'public/Classes/Dbh.php';
include 'public/Classes/VintageHandler.php';
include 'public/Classes/VintageFilter.php';
session_start();
$conn = new Dbh();
$products = new VintageHandler($conn);

$sort = isset($_GET['sort']) ? $_GET['sort'] : 'newest';
$sortMessage = '';

switch ($sort) {
    case 'price_desc':
        $sortMessage = 'Price High to Low';
        break;
    case 'price_asc':
        $sortMessage = 'Price Low to High';
        break;
    case 'newest':
        $sortMessage = 'Newest';
        break;
    case 'oldest':
        $sortMessage = 'Oldest';
        break;
    default:
        $sortMessage = 'Newest'; 
        break;
}

?>
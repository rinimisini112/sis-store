<?php
include 'public/Classes/Dbh.php';
include 'public/Classes/VintageHandler.php';
// Check if $item is set and not empty

if(isset($_GET['id'])) {
$product_id = $_GET['id'];


$conn = new Dbh();
$products = new VintageHandler($conn);
$sizes = $products->fetchSizesForProduct($product_id);
$item = $products->fetchSingleProduct($product_id);
if (!$item) {
    // Product not found, redirect to a 404 page or another appropriate page
    header("Location: collections.php");
    exit();
}
} else {
    // No ID provided, redirect to a 404 page or another appropriate page
    header("Location: collections.php");
    exit();
}
?>
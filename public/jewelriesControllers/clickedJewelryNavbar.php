<?php
$items = $jewelry->fetchCategories();
if(isset($_SESSION['filteredCatJewelry'])){
    $filterRemove = '&filter=remove';
} else {
    $filterRemove = '';
}
foreach ($items as $product) {
    $categoryId = $product->id;
    $isSelected = isset($_GET['cid']) && $_GET['cid'] == $categoryId;
    $widthClass = $isSelected ? 'w-full' : 'w-0';

    echo "<li class='py-1.5 font-semibold text-sis-yellow'>";
    echo "<a href='jewelryCategories.php?cid={$categoryId}{$filterRemove}' class='relative group rische'>";
    echo "{$product->name}";
    echo "<span class='absolute bottom-0 left-0 duration-200 {$widthClass} h-px group-hover:w-full bg-sis-yellow'></span>";
    echo "</a>";
    echo "</li>";
}
?>

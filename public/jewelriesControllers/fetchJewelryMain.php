<?php
foreach ($itemsSubset as $product) {
    // Escape and encode product_id
    $productId = htmlspecialchars($product->id, ENT_QUOTES, 'UTF-8');

    echo "<div class='lg:w-[32%] w-[90%] pb-4 lg:mx-0 mx-auto lg:h-auto bg-[rgb(192,202,208)] shadow-xl h-[500px] lg:px-4 px-3 lg:mb-8 mb-6 relative'>";
    echo "<div class='relative lg:h-[500px] h-[85%] group'>";
    echo "<a  href='jewelry.php?id={$productId}'><img src='resources/images/{$product->main_image}' alt='Image of model in clothes' class='w-full h-full object-cover duration-300'>";
if(isset($product->image1)){
        echo "<img src='resources/images/{$product->image1}' alt='Image of model in clothes' class='w-full h-full object-cover absolute top-0 left-0 opacity-0 group-hover:opacity-100 duration-300'>";
    }
    echo "</a>";
    echo "</div>";
    echo "<h2 class='lg:text-2xl pb-4 lg:pb-0 text-lg pt-2 font-bold flex items-center justify-between'><a href='public/collections/product.php?id={$productId}'>{$product->name}</a>" .
    "<p class='md:pr-4 pr-0 pl-3 text-sis-sandy font-semibold'>{$product->price}€</p></h2>";
    echo "</div>";
}
if(empty($itemsSubset)) {
    echo "<p class='text-3xl text-center py-16 w-full'>No items found!</p>";
}


// Output the pagination links
?>

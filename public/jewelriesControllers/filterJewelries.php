<?php 
session_start();
include "../Classes/Jewelery.php";

if(isset($_POST['removefilter'])) {
    unset($_SESSION['filterJewelryResults']);
    unset($_SESSION['jewelryPrice']);
    unset($_SESSION['selectedMaterials']);
    header("Location:../../jewelries.php");
}

if(isset($_POST['filterJewelry'])) {
    try{
    $price = isset($_POST['priceRange']) ? $_POST['priceRange'] : ''; 
    $materials = isset($_POST['material']) ? $_POST['material'] : [];
    $jewelry = new Jewelery();
    if(isset($_POST['sortVal']) && $_POST['sortVal'] !== '') {
    $sortRedirect = '&sort=' . $_POST['sortVal'];
    } else  {
        $sortRedirect = '';
    }
    $filterJewelry = $jewelry->fetchFilteredJewelry($price, $materials);

    $_SESSION['filterJewelryResults'] = $filterJewelry;
    $_SESSION['jewelryPrice'] = $price;

    $_SESSION['selectedMaterials'] = $materials;
    header('Location:../../jewelries.php?filtered=true' . $sortRedirect);
    if(!$filterJewelry) {
        throw new Exception('No items retrieved from the filter');
        return;
    }
    } catch (Exception $e) {
        echo "Error :" . $e->getMessage();
    }
}

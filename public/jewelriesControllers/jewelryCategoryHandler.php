<?php
session_start();
include "public/Classes/JeweleryCategory.php";
$jewelry = new JeweleryCategory();
if (isset($_GET['cid'])) {
    try {
        if(isset($_GET['filter'])) {
            unset($_SESSION['filteredCatJewelry']);
            unset($_SESSION['jewelryCatPrice']);
            unset($_SESSION['selectedCatMaterials']);
        }
        if (isset($_POST['removefilter'])) {
            unset($_SESSION['filteredCatJewelry']);
            unset($_SESSION['jewelryCatPrice']);
            unset($_SESSION['selectedCatMaterials']);
        }
        $categoryId = $_GET['cid'];
        $jewelryCategory = $jewelry->fetchCategoriesByID($categoryId);
        $sortMessage = 'Newest';

        if (isset($_GET['sort'])) {
            $sort = isset($_GET['sort']) ? $_GET['sort'] : 'newest';

            switch ($sort) {
                case 'price_desc':
                    $sortMessage = 'Price High to Low';
                    break;
                case 'price_asc':
                    $sortMessage = 'Price Low to High';
                    break;
                case 'newest':
                    $sortMessage = 'Newest';
                    break;
                case 'oldest':
                    $sortMessage = 'Oldest';
                    break;
                default:
                    $sortMessage = 'Newest';
                    break;
            }
        }
        if (isset($_POST['filterJewelry'])) {
            $price = isset($_POST['priceRange']) ? $_POST['priceRange'] : '';
            $materials = isset($_POST['material']) ? $_POST['material'] : [];
            $filterJewelry = $jewelry->fetchFilteredCategoryJewelry($categoryId, $price, $materials);

            $_SESSION['filteredCatJewelry'] = $filterJewelry;
            $_SESSION['jewelryCatPrice'] = $price;

            $_SESSION['selectedCatMaterials'] = $materials;
        }
        if (isset($_GET['cid']) && isset($_SESSION['filteredCatJewelry']) && !isset($_GET['sort'])) {
            $jewelries = $_SESSION['filteredCatJewelry'];
            if (empty($jewelries)) {
                throw new Exception('No items found matching your filtering');
            }
        } else if (isset($_GET['cid']) && isset($_SESSION['filteredCatJewelry']) && isset($_GET['sort'])) {
            $itemIds = $_SESSION['filteredCatJewelry'];
            $jewelries = $jewelry->sortJewelryCategoryByIds($categoryId, $_SESSION['filteredCatJewelry'], $sort);
            if (empty($jewelries)) {
                throw new Exception('No items found matching your filtering');
            }
        } else if (isset($_GET['cid']) && isset($_GET['sort'])) {
            $jewelries = $jewelry->fetchJeweleryFilteredByCategory($categoryId, $sort);
            if (empty($jewelries)) {
                throw new Exception('No items found for this category');
            }
        } else {
            $jewelries = $jewelry->fetchJeweleryFilteredByCategory($categoryId);
            if (empty($jewelries)) {
                throw new Exception('No items found for this category');
            }
        }
    } catch (Exception $e) {
        $_SESSION['catFilterMssgJewelry'] = $e->getMessage();
    }
} else {
    header("Location:jewelries.php");
}

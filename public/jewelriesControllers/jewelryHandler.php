<?php 
session_start();
include "public/Classes/Jewelery.php";
$jewelry = new Jewelery();
$sortMessage = 'Newest';

if(isset($_GET['sort'])) {
    $sort = isset($_GET['sort']) ? $_GET['sort'] : 'newest';

switch ($sort) {
    case 'price_desc':
        $sortMessage = 'Price High to Low';
        break;
    case 'price_asc':
        $sortMessage = 'Price Low to High';
        break;
    case 'newest':
        $sortMessage = 'Newest';
        break;
    case 'oldest':
        $sortMessage = 'Oldest';
        break;
    default:
        $sortMessage = 'Newest'; 
        break;
}
     
}


<?php
$items = $jewelry->fetchCategories();

foreach ($items as $product) {
    echo    "<li class='py-1.5 font-semibold text-sis-yellow'>";
    echo "<a href='jewelryCategories.php?cid={$product->id}' class='relative group rische'>";
    echo "{$product->name}";
    echo "<span class=' absolute bottom-0 left-0 duration-200 w-0 h-px group-hover:w-full bg-sis-yellow'></span>";
    echo "</a>";
    echo "</li>";
}
?>
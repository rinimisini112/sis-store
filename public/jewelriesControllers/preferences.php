<?php
include "public/Classes/Preference.php";
$preference = new Preference();

// Fetch two distinct random IDs
$ids = $preference->fetchIdFromCollections();

// Fetch details for the two products
$results = $preference->fetch2PreferencesByIds($ids[0], $ids[1]);
?>

<div class="lg:w-[80%] w-full ml-auto my-8 text-sis-blue">
    <p class="lg:text-5xl text-4xl md:text-left text-center rische py-4 text-sis-blue underline underline-offset-2 decoration-sis-sandy">You may also like</p>
    <div class="w-4/5 lg:mx-auto ml-auto justify-between flex items-center my-8">
        <div class="lg:w-[50%] w-[80%] md:h-[500px] h-[400px] relative group">
            <a href="product.php?id=<?php echo $results[0]->product_id ?>">
                <img src="resources/images/<?php echo $results[0]->main_image; ?>" alt="" class="w-full h-full object-cover">
                <?php if (isset($results[0]->image1)) {
                    echo "<img src='resources/images/" . $results[0]->image1 . "' alt='' class='w-full h-full absolute top-0 left-0 opacity-0 duration-200 group-hover:opacity-100 object-cover'>";
                }
                ?>
                <p class="horizontal-txt absolute top-0 left-0 transform -translate-x-11 text-3xl"><?php echo $results[0]->product_name; ?></p>
            </a>

        </div>
        <div class="w-[40%] md:block hidden h-[500px] relative group">
            <a href="product.php?id=<?php echo $results[1]->product_id ?>">
                <img src="resources/images/<?php echo $results[1]->main_image; ?>" alt="" class="w-full h-full object-cover">
                <?php if (isset($results[1]->image1)) {
                    echo "<img src='resources/images/" . $results[1]->image1 . "' alt='' class='w-full h-full absolute top-0 left-0 opacity-0 duration-200 group-hover:opacity-100 object-cover'>";
                }
                ?>
                <p class="horizontal-txt absolute top-0 left-0 transform -translate-x-11 text-3xl"><?php echo $results[1]->product_name; ?></p>
            </a>
        </div>
    </div>
</div>
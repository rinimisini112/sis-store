<?php
include 'public/Classes/Jewelery.php';
// Check if $item is set and not empty

if(isset($_GET['id'])) {
$jewelry_id = $_GET['id'];


$jewelry = new Jewelery();
$item= $jewelry->fetchSingleJewelry($jewelry_id);
if (!$item) {
    // Product not found, redirect to a 404 page or another appropriate page
    header("Location: jewelries.php");
    exit();
}
} else {
    // No ID provided, redirect to a 404 page or another appropriate page
    header("Location: jewelries.php");
    exit();
}
?>
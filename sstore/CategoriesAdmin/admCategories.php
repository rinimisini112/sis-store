<?php
session_start();
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../adm.php");
    exit();
}
?>
<body class="bg-sis-white">
    <main class="w-[80%] flex mb-6 pb-2">
        <div class="w-full">
        <?php 
        include "../admInc/categoriesNavbar.php";
        ?>
        <div class="">
        <div class=" w-full text-center bg-gray-400 flex items-center justify-between pl-6 text-sis-white text-2xl font-bold">
            <p>Admin Paneli i SIS store - Kategorit, shto, ndrysho dhe fshij Kategorit</p>
            <a href="add_product.php" class="h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl font-bold">Shto Kategori +</a>
            </div>
            <div id="categoryForm" class="w-full bg-gray-300 text-sis-grey">
                <form id="addCategory" action="controllers/addCategory.php" method="post" class="w-[70%] mx-auto py-8 flex items-center justify-between">
                    <div class="w-1/2">
                    <label for="addCategory" class="text-2xl font-bold">Emri i Kategoris :</label>
                    <input type="text" name="addCategory" id="addCategory" class="w-[60%] text-sis-grey pl-3 text-xl py-2">
                    </div>
                    <input type="submit" value="Shto" name="submitCategory" class="px-16 text-xl font-bold cursor-pointer rounded-lg py-2 bg-blue-400 hover:bg-blue-600 duration-150 hover:text-sis-white">
                </form>
        </div>
        </div>
        <?php
        if (isset($_SESSION['error_mssg'])) {
            echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['error_mssg'] . "</p>";
            unset($_SESSION['error_mssg']);
        } else if (isset($_SESSION['success_mssg'])) {
            echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['success_mssg'] . "</p>";
            unset($_SESSION['success_mssg']);
        }
        ?>
        <div class="w-full h-auto flex flex-col items-center">
            <table class="w-full border-none outline-none" style="border: none!important;">
                <thead class="">
                <tr class="w-full shadow-lg bg-gradient-to-r from-[#8e5ac3] via-[#8453b4] to-[#774ca2] text-sis-white text-xl">
                        <th class="">Emri i Kategoris</th>
                        <th class="bg-[#68438d]">Total Produktet per ate kategori</th>
                        <th class="">Ndrysho</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'controllers/fetchCategoriesAdm.php';
                    ?>
                </tbody>
            </table>
        </div>
        </div>
      <?php include "../admInc/categoriesMenu.php"?>
    </main>
    <script>
        $(document).ready(function() {
    $.validator.addMethod("alphabetsOnly", function(value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");

    $("#addCategory").validate({
        rules: {
            addCategory: {
                required: true,
            },
        },
        messages: {
            addCategory: {
                required: "Fusha nuk duhet te jete e zbrazt",
            },

        },
        errorPlacement: function(error, element) {
            // Place the error message below the input element
            error.insertAfter(element);

            // Add a class to the input when there is an error
            element.addClass("error_effect");
        },
        success: function(label, element) {
            // Remove the error class when the input is valid
            $(element).removeClass("error_effect");
        }
    });
});
    document.addEventListener("DOMContentLoaded", function () {
        // Get the categoryForm and initial height
        var categoryForm = document.getElementById("categoryForm");
        var initialHeight = categoryForm.offsetHeight;

        // Initially hide the form
        gsap.set(categoryForm, { height: 0, overflow: "hidden" });

        // Toggle the form on link click
        document.querySelector("a[href='add_product.php']").addEventListener("click", function (event) {
            event.preventDefault(); // Prevent the default link behavior

            // Toggle the form's height
            gsap.to(categoryForm, { height: categoryForm.offsetHeight === 0 ? initialHeight : 0, duration: 0.3 });
        });
    });

        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 5000); // 5000 milliseconds = 5 seconds
        });
    </script>
</body>

</html>
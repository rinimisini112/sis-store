<?php 
session_start();
include "../../../public/Classes/Dbh.php";
include "../../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../../adm.php");
    exit();
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submitCategory'])) {
    $categoryName = $_POST['addCategory'];

    try {
    include '../../../public/Classes/CategoryHandler.php';

    $addCategory = new CategoryHandler(new Dbh);
    $insertation = $addCategory->insertCategories($categoryName);
    if($insertation) {
        $_SESSION['success_mssg'] = 'Kategoria u fut me sukses';
        header("Location: ../admCategories.php");
        exit;
    } else {
        $_SESSION['error_mssg'] = 'Kategoria nuk mund te futet, ju lutem provoni prap';
        header("Location: ../admCategories.php"); 
        exit;   
    }
    } catch(Exception $e) {
        echo "Na vjen keq diqka shkoj keq";
    }
}

<?php 
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['editCategory'])) {
    $cID = $_POST['categoryID'];
    $categoryName = $_POST['categoryName'];
    include '../../../public/Classes/Dbh.php';
    include '../../../public/Classes/CategoryHandler.php';
try {
    $updateCategory = new CategoryHandler(new Dbh);
    $updatedResult = $updateCategory->updateCategoryWithId($cID,$categoryName);
    if($updatedResult) {
        $_SESSION['success_mssg'] = 'Kategoria u ndryshua me sukses';
        header("Location: ../edit_categories.php?cid={$cID}");
        exit;
    } else {
        $_SESSION['error_mssg'] = 'Kategoria nuk mundi te ndryshohet, provoni prap';
        header("Location: ../edit_categories.php?cid={$cID}");
        exit;
    }
} catch (Exception $e) {
    $_SESSION['error_mssg'] = 'Keni vendosur emrin e njejt, nese deshironi ta ndryshoni provoni prap duke ndryshuar emrin';
    header("Location: ../edit_categories.php?cid={$cID}");
}
}
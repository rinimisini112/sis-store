<?php
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteCategory'])) {
    include "../../../public/Classes/Dbh.php";
    include "../../../public/Classes/CategoryHandler.php";

    try {

        $productDelete = new CategoryHandler(new Dbh);
        $categoryId = $_POST['categoryID'];
        $result = $productDelete->deleteCategory($categoryId);
        if ($result) {
            $_SESSION['success_mssg'] = 'Kategoria u fshi me sukses';
            header("Location:../admCategories.php");
            exit(); 
        } else {
            $_SESSION['error_mssg'] = 'Kategoria nuk mundi te fshihet, provo prap';
            header("Location:../edit_categories.php?cid=" . $categoryId);
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        // You might want to handle this error more gracefully, such as logging it
    }
}
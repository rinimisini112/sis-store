<?php 
include "../../public/Classes/CategoryHandler.php";

$categoryList = new CategoryHandler(new Dbh);
$categories = $categoryList->fetchCategoriesWithProductCount();
foreach ($categories as $index => $row) {
    $categoryName = $row['category_name'];
    $productCount = $row['product_count']; // Access the product_count field

    $bgColorClass = ($index % 2 == 0) ? 'bg-slate-400' : 'bg-gray-300';

    echo '<tr class="h-auto w-full ' . $bgColorClass . '">';
    echo '<td class="border-4 text-xl font-bold border-sis-white text-center">' . $categoryName . '</td>';
    echo '<td class="border-4 text-xl font-bold border-sis-white text-center">' . $productCount . '</td>'; // Display the product count
    echo '<td class="border-4 border-sis-white text-center underline underline-offset-1 text-2xl font-bold bg-sis-grey text-sis-white hover:bg-green-500 duration-200 "><a href="edit_categories.php?cid=' . $row['category_id'] . '">Edit</td>';
    echo '</tr>';
}
?>
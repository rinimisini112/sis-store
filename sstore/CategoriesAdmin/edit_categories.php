<?php
session_start();
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../adm.php");
    exit();
}
if(isset($_GET['cid'])) {
    $categoryID = $_GET['cid'];

    include '../../public/Classes/CategoryHandler.php';
    $editCategory = new CategoryHandler(new Dbh);
    $categoryData =  $editCategory->fetchCategoriesWithId($categoryID);
}
?>
<body class="bg-sis-white">
        
    <main class="w-[80%] h-auto mb-6">
    <div>
    <?php
        include '../admInc/categoriesNavbar.php';
        ?>    
    <div class=" w-full text-center bg-gray-400 flex items-center justify-between pl-6 text-sis-white text-2xl font-bold">
            <p>Admin Paneli i SIS store - Nrysho te dhenat e Kategorise</p>
            <a href="admCategories.php" class="h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl font-bold">Shko mbrapa</a>
        </div>
        <form action="controllers/categoryEdit.php" id="editProductForm" method="post" enctype="multipart/form-data" class="w-full pb-6 flex flex-col">
            <input type="hidden" name="categoryID" value="<?= $categoryID ?>">
                <p class="bg-gray-600 text-sis-white text-center flex items-center text-2xl font-bold justify-between pl-2 h-12">Ktu munesh me pa kategorin, munesh me i ndryshu ose mi fshi <a class="bg-red-500 px-4 flex items-center justify-center hover:bg-red-600 border border-red-700 duration-150 text-sis-grey h-full hover:text-sis-white" href="deleteCategories.php?cid=<?= $categoryID ?>">Fshij Kategorin</a></p>
                <?php
                if (isset($_SESSION['error_mssg'])) {
                    echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['error_mssg'] . "</p>";
                    unset($_SESSION['error_mssg']);
                } else if (isset($_SESSION['success_mssg'])) {
                    echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['success_mssg'] . "</p>";
                    unset($_SESSION['success_mssg']);
                }
                ?>
                <div class="flex flex-col w-[60%] mx-auto gap-y-4">
                    <label class="text-xl font-bold pt-3" for="productName">Emri i Kategoris</label>
                    <input type="text" class="py-3 text-xl pl-3" disabled name="oldCategoryName" id="oldCategoryName" value="<?php echo $categoryData['category_name'] ?? ''; ?>">
                    <label class="text-xl font-bold pt-3" for="productDesc">Emri i Ri i Kategorise</label>
                    <input type="text" class="py-3 text-xl pl-3" name="categoryName" id="categoryName" value="<?php echo $categoryData['category_name'] ?? ''; ?>">
                        </div>
                    <?php
                    ?>
                    <input type="submit" name="editCategory" id="editCategory" value='Ndrysho Te dhenat' class=" hover:bg-blue-700 bg-blue-300 hover:text-sis-white duration-100 text-xl font-bold border mx-auto border-blue-500 px-8 w-1/2 py-2 mt-8 cursor-pointer">

                </div>
        </form>
        </div>
        <?php include "../admInc/categoriesMenu.php"?>
    </main>
<script>
    $(document).ready(function() {
    // Wait for the document to be ready
    setTimeout(function() {
        $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
    }, 5000); // 5000 milliseconds = 5 seconds
});
</script>
</body>

</html>
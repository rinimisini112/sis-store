<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="../dist/output.css">
    <link rel="stylesheet" href="../dist/customs.css">
    <script src="../dist/javascript/jquery.js"></script>
    <script src="../dist/javascript/minified/gsap.min.js"></script>
    <script src="../dist/javascript/minified/ScrollTrigger.min.js"></script>
    <script src="../dist/javascript/jquery.validate.js"></script>
    <script src="../dist/javascript/jquery.validate.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="../resources/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../resources/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../resources/favicon/favicon-16x16.png">
    <link rel="manifest" href="../resources/favicon/site.webmanifest">
    <title>SIS store</title>
    <style>
           .error {
    color: red!important;
    padding-top: 5px!important;
    margin-top: 0!important;
    outline-color: red!important;
}
    .error:focus {
    color: red;
    border-color: red!important;
    outline-color: red!important;

    }
    .error_effect {
        border-color: red!important;
        outline-color: red!important;

    }
    </style>
</head>
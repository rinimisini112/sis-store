<nav id="navbar" class=" duration-200 z-30 text-sis-grey  flex md:px-16 px-8 lg:py-2 py-4 justify-between items-center bg-opacity-10 w-full">
            <a href="../../index.php" class=" relative z-[100]">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
            <div class=" text-2xl font-bold flex justify-between bg-sis-white">
                <a href='../adm_panel.php' class=" text-center px-6">Produktet</a>
                <a href='../vintageProducts/vintage_panel.php' class=" text-center px-6">Produktet Vintage</a>
                <a href='../CategoriesAdmin/admCategories.php' class=" text-center px-6">Kategorit</a>
                <a href='../faqAdmin/faq_panel.php' class=" text-center px-6">FAQ</a>
                <a href='../messagesAdmin/message_panel.php' class=" text-center px-6 ">Mesazhet</a>
                <a href='homepageControl.php' class=" text-center px-6  underline underline-offset-4">Homepage</a>
            </div>
            <a href="../auth/logout.php" class=" border-sis-grey border text-sis-grey lg:px-16 px-8 py-2 text-xl font-bold hover:bg-sis-grey hover:text-sis-white duration-200">Logout</a>
        </nav>
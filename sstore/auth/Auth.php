<?php
class Auth
{
    private $conn;

    public function __construct(Dbh $conn)
    {
        try {
            if (!$conn) {
                throw new Exception('Something went wrong');
            }
            $this->conn = $conn->connect();
        } catch (Exception $e) {
            echo "We are sorry something went wrong " . $e->getMessage();
        }
    }
   public function isAuthTokenValid()
    {
        if (isset($_COOKIE['auth_token'])) {
            $token = $_COOKIE['auth_token'];
            $query = "SELECT COUNT(*) FROM adm WHERE auth_token = :token";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':token', $token, PDO::PARAM_STR);
            $stmt->execute();

            return ($stmt->fetchColumn() > 0);
        }

        return false;
    }
    public function registerAdmin($username, $password)
    {
        // Hash the password using Bcrypt
        $hashedPassword = password_hash($password, PASSWORD_BCRYPT);

        // Insert the admin into the database with the hashed password
        $this->insertAdminIntoDatabase($username, $hashedPassword);
    }

    private function insertAdminIntoDatabase($username, $hashedPassword)
    {
        $query = "INSERT INTO adm (username, password_hash) VALUES (:username, :hashedPassword)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':hashedPassword', $hashedPassword, PDO::PARAM_STR);
        $stmt->execute();
    }
        public function removeAuthToken($token)
    {
        $query = "UPDATE adm SET auth_token = NULL WHERE auth_token = :token";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':token', $token, PDO::PARAM_STR);
        $stmt->execute();
    }
    public function AuthenticateAdm($username, $password)
    {
        // Retrieve hashed password from the database
        $hashedPassword = $this->getHashedPassword($username);

        // Check if the provided password matches the hashed password
        if ($hashedPassword && password_verify($password, $hashedPassword)) {
            // Generate a new auth token
            $token = bin2hex(random_bytes(32));

            // Store the token in the database
            $this->storeTokenInDatabase($username, $token);

            // Set the auth token as a secure cookie
            setcookie('auth_token', $token, time() + (86400 * 30), "/", "", true, true);

            // Authentication successful
            $_SESSION['username'] = $username;
            header("Location: ../adm_panel.php");
            exit();
        } else {
            // Incorrect credentials
            $_SESSION['mssg'] = 'Incorrect credentials, Please try again';
            header("Location: ../adm.php");
            exit();
        }
    }

    private function getHashedPassword($username)
    {
        $query = "SELECT password_hash FROM adm WHERE username = :username";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return ($result) ? $result['password_hash'] : null;
    }

    private function storeTokenInDatabase($username, $token)
    {
        $query = "UPDATE adm SET auth_token = :token WHERE username = :username";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':token', $token, PDO::PARAM_STR);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();
    }
}
?>
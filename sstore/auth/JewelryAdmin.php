<?php

class JewelryAdmin extends Dbh
{
    private $pdo;

    private function getPDO()
    {
        if (!$this->pdo) {
            $this->pdo = $this->connect();
        }
        return $this->pdo;
    }
    public function fetchJewelryAdmin($jewelry_id = '')
    {
        try {
            $query = "SELECT j.*, ji.*, jc.id as category_id, jc.name as category_name
            FROM jewelries j INNER JOIN jewelry_images ji ON j.id = ji.jewellry_id
            INNER JOIN jewelry_categories jc ON j.category_id = jc.id ";

            if (!empty($jewelry_id)) {
                $query .= 'WHERE j.id = :jewelryid';
            } else {
                $query .= "ORDER BY j.created_at DESC";
            }
            $stmt = $this->prepare($query);
            if (!empty($jewelry_id)) {
                $stmt->bindParam(':jewelryid', $jewelry_id);
            }
            $stmt->execute();
            if (!empty($jewelry_id)) {
                return $stmt->fetchObject();
            } else {
                return $stmt->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (\Throwable $th) {
            echo "Could not get jewelries :" . $th->getMessage();
        }
    }
    public function fetchCategories($selectedCategory = '')
    {
        try {
            $query = "SELECT jewelry_categories.* FROM jewelry_categories";

            // Check if a specific category is selected
            if (!empty($selectedCategory)) {
                // Add WHERE clause to filter by selected category ID
                $query .= " INNER JOIN jewelries ON jewelry_categories.id = jewelries.category_id WHERE jewelries.id = :selectedCategory";
            }

            $stmt = $this->prepare($query);

            // Bind parameter if a specific category is selected
            if (!empty($selectedCategory)) {
                $stmt->bindParam(':selectedCategory', $selectedCategory, PDO::PARAM_INT);
            }

            $stmt->execute();
            if (!empty($selectedCategory)) {
                return $stmt->fetchObject();
            } else {
                return $stmt->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (\Throwable $th) {
            // Handle exceptions if needed
            echo "Error: " . $th->getMessage();
            return []; // Return an empty array in case of an error
        }
    }
    public function fetchMaterials($id = '')
    {
        try {
            $query = "SELECT material FROM jewelries";
            if (!empty($id)) {
                $query .= " WHERE id = :id";
            }
            $stmt = $this->prepare($query);
            if (!empty($id)) {
                $stmt->bindParam(":id", $id);
            }
            $stmt->execute();

            if (!empty($id)) {
                return $stmt->fetchObject();
            } else {
                return $stmt->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (\Throwable $th) {
            echo "Could not get materials :" . $th->getMessage();
        }
    }
    public function fetchColors($id = '')
    {
        try {
            $query = "SELECT color FROM jewelries";
            if (!empty($id)) {
                $query .= ' WHERE id = :id';
            }

            $stmt = $this->prepare($query);

            if (!empty($id)) {
                $stmt->bindParam(':id', $id);
            }
            $stmt->execute();


            if (!empty($id)) {
                return $stmt->fetchObject();
            } else {
                return $stmt->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (\Throwable $th) {
            echo "Could not get colors :" . $th->getMessage();
        }
    }

    public function insertJewelryWithImages($name, $description, $price, $material, $color, $category, $main_image, $image1, $image2, $image3, $image4, $image5)
    {
        try {
            // Start a transaction
            $this->getPDO()->beginTransaction();

            // Insert jewelry product
            $query = "INSERT INTO jewelries(name, description, price, material, color, category_id)
            VALUES(:name, :description, :price, :material, :color, :category_id)";
            $stmt = $this->getPDO()->prepare($query);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':price', $price);
            $stmt->bindParam(':material', $material);
            $stmt->bindParam(':color', $color);
            $stmt->bindParam(':category_id', $category);
            $stmt->execute();

            // Get the generated ID from the last insertion
            $jewelryId = $this->getPDO()->lastInsertId();

            // Insert jewelry images using the generated jewelry ID
            $queryImages = "INSERT INTO jewelry_images(jewellry_id, main_image, image1, image2, image3, image4, image5)
            VALUES(:jewelry_id, :main_image, :image1, :image2, :image3, :image4, :image5)";
            $stmtImages = $this->getPDO()->prepare($queryImages);
            $stmtImages->bindParam(':jewelry_id', $jewelryId);
            $stmtImages->bindParam(':main_image', $main_image);
            $stmtImages->bindParam(':image1', $image1);
            $stmtImages->bindParam(':image2', $image2);
            $stmtImages->bindParam(':image3', $image3);
            $stmtImages->bindParam(':image4', $image4);
            $stmtImages->bindParam(':image5', $image5);
            $stmtImages->execute();

            // Commit the transaction
            $this->getPDO()->commit();

            return true;
        } catch (\Throwable $th) {
            // Rollback the transaction in case of an error
            $this->getPDO()->rollBack();
            echo "Error: " . $th->getMessage();
            return false;
        }
    }
    // update jewelry products
    public function updateJewelryWithImages($jewelryId, $name, $description, $price, $material, $color, $category, $main_image, $image1, $image2, $image3, $image4, $image5)
    {
        try {
            // Start a transaction
            $this->getPDO()->beginTransaction();

            // Update jewelry product
            $query = "UPDATE jewelries SET
            name = :name,
            description = :description,
            price = :price,
            material = :material,
            color = :color,
            category_id = :category_id
            WHERE id = :jewelry_id";

            $stmt = $this->getPDO()->prepare($query);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':price', $price);
            $stmt->bindParam(':material', $material);
            $stmt->bindParam(':color', $color);
            $stmt->bindParam(':category_id', $category);
            $stmt->bindParam(':jewelry_id', $jewelryId);
            $stmt->execute();

            // Update jewelry images
            $queryImages = "UPDATE jewelry_images SET
            main_image = :main_image,
            image1 = :image1,
            image2 = :image2,
            image3 = :image3,
            image4 = :image4,
            image5 = :image5
            WHERE jewellry_id = :jewelry_id";

            $stmtImages = $this->getPDO()->prepare($queryImages);
            $stmtImages->bindParam(':main_image', $main_image);
            $stmtImages->bindParam(':image1', $image1);
            $stmtImages->bindParam(':image2', $image2);
            $stmtImages->bindParam(':image3', $image3);
            $stmtImages->bindParam(':image4', $image4);
            $stmtImages->bindParam(':image5', $image5);
            $stmtImages->bindParam(':jewelry_id', $jewelryId);
            $stmtImages->execute();

            // Commit the transaction
            $this->getPDO()->commit();

            return true;
        } catch (\Throwable $th) {
            // Rollback the transaction in case of an error
            $this->getPDO()->rollBack();
            echo "Error: " . $th->getMessage();
            return false;
        }
    }
    public function deleteJewelry($id)
    {
        try {
            // Start a transaction
            $this->getPDO()->beginTransaction();
    
            // Delete the jewelry record
            $deleteQuery = "DELETE FROM jewelries WHERE id = :id";
            $deleteStatement = $this->getPDO()->prepare($deleteQuery);
            $deleteStatement->bindParam(':id', $id, PDO::PARAM_INT);
            $deleteStatement->execute();
    
            // If deletion is successful, commit the transaction
            $this->getPDO()->commit();
    
            return true;
        } catch (\Throwable $th) {
            // If an error occurs, rollback the transaction and return false
            $this->getPDO()->rollBack();
            echo "Error: " . $th->getMessage();
            return false;
        }
    }
}

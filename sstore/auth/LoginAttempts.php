<?php 

class LoginAttempts
{
    private $conn;
    
    public function __construct(Dbh $conn) 
    {
        try {
            if(!$conn) {
                throw new Exception('Something went wrong');
            }
            $this->conn = $conn->connect();
            
        } catch (Exception $e) {
            echo "We are sorry something went wrong " . $e->getMessage();
        }
    }

    public function getLoginAttempts($username)
    {
        $query = "SELECT * FROM login_attempts WHERE username = :username";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();
    
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function updateLoginAttempts($username, $attemptCount)
    {
        $query = "INSERT INTO login_attempts (username, attempt_count) 
                  VALUES (:username, :attempt_count)
                  ON DUPLICATE KEY UPDATE attempt_count = :attempt_count";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':attempt_count', $attemptCount, PDO::PARAM_INT);
        $stmt->execute();
    }
}
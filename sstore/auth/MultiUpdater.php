<?php
include 'MultiHandler.php';
class MultiUpdater extends MultiHandler 
{

    public function updateProductAndRelatedData($productID, $productName, $productDesc, $productPrice, $inStock, $categoryID, $mainImage, $image1, $image2, $image3, $image4, $image5, $image6, $selectedSizes)
    {
        try {
            // Start a transaction
            $this->conn->beginTransaction();

            // Update product data
            $this->updateProduct($productID, $productName, $productDesc, $productPrice, $inStock, $categoryID);

            // Update product images
            $this->updateProductImage($productID, $mainImage, $image1, $image2, $image3, $image4, $image5, $image6);

            // Update product sizes
            $this->updateProductSizes($productID, $selectedSizes);

            // Commit the transaction
            $this->conn->commit();

            return true;
        } catch (Exception $e) {
            // An error occurred, rollback the transaction
            $this->conn->rollBack();

            echo "Error: " . $e->getMessage();
        }
    }

    private function updateProduct($productID, $productName, $productDesc, $productPrice, $inStock, $categoryID)
    {
        try {
            $query = "UPDATE products 
                      SET product_name = :productName, 
                          product_description = :productDesc, 
                          price = :productPrice, 
                          in_stock = :inStock, 
                          category_id = :categoryID 
                      WHERE product_id = :productID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productName', $productName);
            $stmt->bindParam(':productDesc', $productDesc);
            $stmt->bindParam(':productPrice', $productPrice);
            $stmt->bindParam(':inStock', $inStock, PDO::PARAM_BOOL); // Specify the data type
            $stmt->bindParam(':categoryID', $categoryID);
            $stmt->bindParam(':productID', $productID);

            $stmt->execute();
        } catch (Exception $e) {
            throw new Exception("Error updating product: " . $e->getMessage());
        }
    }

    private function updateProductImage($productID, $mainImage, $image1, $image2, $image3, $image4, $image5, $image6)
    {
        try {
            $query = "UPDATE product_images 
                      SET main_image = :mainImage, 
                          image1 = :image1, 
                          image2 = :image2, 
                          image3 = :image3, 
                          image4 = :image4, 
                          image5 = :image5, 
                          image6 = :image6 
                      WHERE product_id = :productID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':mainImage', $mainImage);
            $stmt->bindParam(':image1', $image1);
            $stmt->bindParam(':image2', $image2);
            $stmt->bindParam(':image3', $image3);
            $stmt->bindParam(':image4', $image4);
            $stmt->bindParam(':image5', $image5);
            $stmt->bindParam(':image6', $image6);
            $stmt->bindParam(':productID', $productID);

            $stmt->execute();
        } catch (Exception $e) {
            throw new Exception("Error updating product image: " . $e->getMessage());
        }
    }

    private function updateProductSizes($productID, $selectedSizes)
    {
        try {
            // Delete existing product sizes
            $this->deleteProductSizes($productID);

            // Insert updated product sizes
            $this->insertProductSizes($productID, $selectedSizes);
        } catch (Exception $e) {
            throw new Exception("Error updating product sizes: " . $e->getMessage());
        }
    }

    private function deleteProductSizes($productID)
    {
        try {
            $query = "DELETE FROM productsizes WHERE product_id = :productID";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productID', $productID);
            $stmt->execute();
        } catch (Exception $e) {
            throw new Exception("Error deleting product sizes: " . $e->getMessage());
        }
    }

    private function insertProductSizes($productID, $selectedSizes)
    {
        try {
            // Iterate through selected sizes and insert into product_sizes
            foreach ($selectedSizes as $sizeID) {
                $query = "INSERT INTO productsizes (product_id, SizeID) 
                          VALUES (:productID, :sizeID)";

                $stmt = $this->conn->prepare($query);
                $stmt->bindParam(':productID', $productID);
                $stmt->bindParam(':sizeID', $sizeID);

                $stmt->execute();
            }
        } catch (Exception $e) {
            throw new Exception("Error inserting product sizes: " . $e->getMessage());
        }
    }
}
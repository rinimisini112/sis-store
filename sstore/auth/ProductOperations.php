<?php

class ProductOperations
{
    private $conn;

    public function __construct(Dbh $conn)
    {
        try {
        if (!$conn) {
            throw new Exception('Something went wrong');
        }
        $this->conn = $conn->connect();
    } catch (Exception $e) {
        echo 'Were sorry: ' . $e->getMessage();
        }
    }

    public function fetchProductsAdm()
    {
        try {
        $query = "SELECT * FROM 
        products p INNER JOIN categories c 
        ON p.category_id = c.category_id
        INNER JOIN product_images im 
        ON p.product_id = im.product_id ORDER BY created_at DESC";

        $stmt = $this->conn->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!$result) {
            throw new Exception('Gabim ne databaze');
        }
        return $result;
    } catch (Exception $e) {
        echo "Were sorry: " . $e->getMessage();
    }
}

    public function deleteProduct($productId) {
        try {
            $query = "DELETE FROM products WHERE product_id = :productID";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productID',$productId,PDO::PARAM_INT);
            $result = $stmt->execute();
            if($result) {
                return true;
            } else {
                throw new Exception('Deshtoi fshirja e produktit, provoni perseri');
            }
        } catch (Exception $e) {
            echo 'Na vjen keq: ' . $e->getMessage();
        }
    }
}
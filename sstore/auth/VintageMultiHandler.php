<?php

class VintageMultiHandler
{
    protected $conn;

    public function __construct(Dbh $conn)
    {
        $this->conn = $conn->connect();
    }

    public function insertProductAndRelatedData($productName, $productDesc, $productPrice, $inStock, $categoryID, $mainImage, $image1, $image2, $image3, $image4, $image5, $image6, $selectedSizes)
    {
        try {
            // Start a transaction
            $this->conn->beginTransaction();

                // Insert product data
                $productID = $this->insertProduct($productName, $productDesc, $productPrice, $inStock, $categoryID);
            
                if(!$productID) {
                    throw new Exception('Nuk mund të futet produkti');
                }
            
                // Insert product image
                $this->insertProductImage($productID, $mainImage, $image1, $image2, $image3, $image4, $image5, $image6);
            
                // Insert product sizes
                $this->insertProductSizes($productID, $selectedSizes);
            
                // Commit the transaction
                $this->conn->commit();
                return true;
            } catch (Exception $e) {
                // Rollback the transaction in case of an exception
                $this->conn->rollBack();
                throw $e; // Re-throw the exception after rolling back the transaction
            error_log("Error: " . $e->getMessage());
        }
    }

    private function insertProduct($productName, $productDesc, $productPrice, $inStock, $categoryID)
    {
        try {
            $query = "INSERT INTO vintageproducts (v_name, v_description, v_price, in_stock, category_id) 
                      VALUES (:productName, :productDesc, :productPrice, :inStock, :categoryID)";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productName', $productName);
            $stmt->bindParam(':productDesc', $productDesc);
            $stmt->bindParam(':productPrice', $productPrice);
            $stmt->bindParam(':inStock', $inStock, PDO::PARAM_BOOL); // Specify the data type
            $stmt->bindParam(':categoryID', $categoryID);

            $stmt->execute();

            return $this->conn->lastInsertId(); // Return the inserted product_id
        } catch (Exception $e) {
            throw new Exception("Error inserting product: " . $e->getMessage());
        }
    }

    private function insertProductImage($productID, $mainImage, $image1, $image2, $image3, $image4, $image5, $image6)
    {
        try {
            $query = "INSERT INTO vintage_images (vintage_id, vmain_image, vimage1, vimage2, vimage3, vimage4, vimage5, vimage6) 
                      VALUES (:productID, :mainImage, :image1, :image2, :image3, :image4, :image5, :image6)";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productID', $productID);
            $stmt->bindParam(':mainImage', $mainImage);
            $stmt->bindParam(':image1', $image1);
            $stmt->bindParam(':image2', $image2);
            $stmt->bindParam(':image3', $image3);
            $stmt->bindParam(':image4', $image4);
            $stmt->bindParam(':image5', $image5);
            $stmt->bindParam(':image6', $image6);
            $stmt->execute();
        } catch (Exception $e) {
            throw new Exception("Error inserting product image: " . $e->getMessage());
        }
    }

    private function insertProductSizes($productID, $selectedSizes)
    {
        try {
            // Iterate through selected sizes and insert into product_sizes
            foreach ($selectedSizes as $sizeID) {
                $query = "INSERT INTO vintagesizes (vintage_id, SizeID) 
                          VALUES (:productID, :sizeID)";

                $stmt = $this->conn->prepare($query);
                $stmt->bindParam(':productID', $productID);
                $stmt->bindParam(':sizeID', $sizeID);

                $stmt->execute();
            }
        } catch (Exception $e) {
            throw new Exception("Error inserting product sizes: " . $e->getMessage());
        }
    }
    public function getSizeIdByName($sizeName)
    {
        $query = "SELECT SizeID FROM sizes WHERE SizeName = :sizeName";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':sizeName', $sizeName);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result['sizeID'] ?? null;
    }
    public function getProductData($productID)
    {
        try {
            $query = "SELECT p.*, pi.vmain_image, pi.vimage1, pi.vimage2, pi.vimage3, pi.vimage4, pi.vimage5, pi.vimage6,
                          ps.*
                  FROM vintageproducts p
                  LEFT JOIN vintage_images pi ON p.vintage_id = pi.vintage_id
                  LEFT JOIN vintagesizes ps ON p.vintage_id = ps.vintage_id
                  WHERE p.vintage_id = :productID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productID', $productID);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result;
        } catch (Exception $e) {
            throw new Exception("Error getting product data: " . $e->getMessage());
        }
    }
    public function getSelectedSizesForProduct($productID)
    {
        try {
            $query = "SELECT ps.SizeID, s.SizeName
                  FROM vintagesizes ps
                  INNER JOIN sizes s ON ps.SizeID = s.SizeID
                  WHERE ps.vintage_id = :productID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':productID', $productID);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            throw new Exception("Error fetching selected sizes: " . $e->getMessage());
        }
    }
    public function fetchSizesByType($sizeType)
    {
        try {
            $query = "SELECT * FROM sizes WHERE ";
    
            switch ($sizeType) {
                case 'letters':
                    $query .= "SizeName IN ('XS','S', 'M', 'L', 'XL', '2XL') ";
                    break;
                case 'numbers':
                    $query .= "SizeName LIKE '3%' OR SizeName LIKE '4%' ";
                    break;
                default:
                    $query .= "1"; // Default to true if no specific type is provided
                    break;
            }
            $query .= ' ORDER BY SizeID ASC';
    
            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // Separate sizes into letterSizes and numberSizes
            $letterSizes = array_filter($result, function ($size) {
                return in_array($size['SizeName'], ['XS', 'S', 'M', 'L', 'XL', '2XL']);
            });
    
            $numberSizes = array_filter($result, function ($size) {
                return preg_match('/^[34]/', $size['SizeName']);
            });
    
            return [
                'letterSizes' => $letterSizes,
                'numberSizes' => $numberSizes,
            ];
        } catch (Exception $e) {
            error_log("Could not get Sizes: " . $e->getMessage());
            throw new Exception("Sorry, something went wrong.");
        }
    }
}

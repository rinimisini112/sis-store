<?php
session_start();

include "../../public/Classes/Dbh.php";
include "Auth.php";

$conn = new Dbh();
$auth = new Auth($conn);
$auth->removeAuthToken($_COOKIE['auth_token']);

setcookie('auth_token', '', time() - 3600, '/');
session_destroy();
header("Location: ../../adm.php");
exit();
?>
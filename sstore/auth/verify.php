<?php
session_start();
include "../../public/Classes/Dbh.php";
include "Auth.php";
include "LoginAttempts.php";

$maxAttempts = 5;
$username = filter_input(INPUT_POST, 'authUsername', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$conn = new Dbh();
$loginS = new LoginAttempts($conn);
$loginAttempts = $loginS->getLoginAttempts($username);

if ($loginAttempts['attempt_count'] >= $maxAttempts) {
    echo "Account locked. Too many failed login attempts.";
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['login'])) {
    $auth = new Auth($conn);
    // Pass the username and password from the form to AuthenticateAdm method
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $auth->AuthenticateAdm($username, $password);

    $loginS->updateLoginAttempts($username, $loginAttempts['attempt_count'] + 1);
} else {
    header("Location:../adm.php");
}

?>
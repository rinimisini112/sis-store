<?php 
include "../admInc/categoriesHead.php";
?>
<body>
    <main class="bg-gray-300 w-[80%]">
    <div>
        <nav id="navbar" class=" duration-200 drop-shadow-lg sticky top-0 bg-sis-grey z-30 text-sis-white  flex md:px-16 px-8 lg:py-2 py-4 justify-between items-center w-full">
            <a href="../../index.php" class=" relative z-[100]">
                <h1 class="rische text-4xl">SlS <span>st<span class="rische text-[1.95rem]">o</span>re</span></h1>
            </a>
          <a href="../auth/logout.php" class="bg-gradient-to-r from-sis-white via-neutral-300 to-gray-400 border-sis-white border text-sis-grey lg:px-16 px-8 py-2 text-xl font-bold duration-200">Logout</a>
        </nav>
        <div>
            <div class="flex items-center justify-between  bg-gray-400">
            <p class="text-2xl pl-4 py-4 font-bold">Paneli i Postimeve te blogut</p>
            <div class="flex items-center">
            <form action="" method="post" class="flex items-center">
                <input type="search" class="py-1 pl-2" placeholder="Kerko Postime" name="kerkoPostim" id="kerkoPostim">
                <button class=" bg-sis-grey w-10 h-10 rounded-full transform shadow-lg -translate-x-6 duration-200 flex items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-search">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                </svg>
                            </button>
            </form>
            <a href="" class=" h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl font-bold">Shto Postim +</a>
            </div>
        </div>
        </div>
        <div class="w-[95%] mt-8 mx-auto flex flex-col gap-12">
        <p class='text-4xl text-center py-4'>Ne punime</p>
    </div>
    </div>
    <div class=" text-xl font-bold fixed w-[20%] px-8 bg-sis-grey text-sis-white pt-12 shadow-2xl right-0 top-0 h-full flex flex-col ">
            <p class="text-2xl text-center border-b border-b-white pb-4 font-bold ">Menuja</p>
                <a href='../adm_panel.php' class="hover:bg-gradient-to-r hover:from-[#8e5ac3] hover:via-[#8453b4] hover:to-[#774ca2] bg-neutral-800 rounded-2xl my-2  duration-200 text-center py-3 ">Produktet</a>
                <a href='../vintageProducts/vintage_panel.php' class="hover:bg-gradient-to-r hover:from-[#8e5ac3] hover:via-[#8453b4] hover:to-[#774ca2]  bg-neutral-800 rounded-2xl my-2  duration-200 text-center py-3 ">Produktet Vintage</a>
                <a href='blog_panel.php' class=" bg-gradient-to-r from-[#53b49a] via-[#479b85] to-[#367665] rounded-2xl my-2 text-sis-white duration-200 text-center py-[13px] ">Blogu</a>
                <a href='../CategoriesAdmin/admCategories.php' class="hover:bg-gradient-to-r hover:from-[#8e5ac3] hover:via-[#8453b4] hover:to-[#774ca2] bg-neutral-800 rounded-2xl my-2  duration-200 text-center py-3  ">Kategorit</a>
                <a href='../faqAdmin/faq_panel.php' class="hover:bg-gradient-to-r hover:from-[#8e5ac3] hover:via-[#8453b4] hover:to-[#774ca2] bg-neutral-800 rounded-2xl my-2  duration-200 text-center py-3  ">FAQ</a>
                <a href='../messagesAdmin/message_panel.php' class="hover:bg-gradient-to-r hover:from-[#8e5ac3] hover:via-[#8453b4] hover:to-[#774ca2] bg-neutral-800 rounded-2xl my-2  duration-200 text-center py-3  ">Mesazhet</a>
                <a href='' class="hover:bg-gradient-to-r hover:from-[#8e5ac3] hover:via-[#8453b4] hover:to-[#774ca2] bg-neutral-800 rounded-2xl my-2  duration-200 underline-offset-4 text-center py-3  ">Homepage</a>
            </div>
    </main>
</body>

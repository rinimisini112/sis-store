<?php
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteProduct'])) {
    include "../../public/Classes/Dbh.php";
    include "../auth/ProductOperations.php";


    try {

        $productDelete = new ProductOperations(new Dbh);
        $productID = $_POST['productID'];
        $result = $productDelete->deleteProduct($productID);
        if ($result) {
            $_SESSION['success_mssg'] = 'Produkti u fshi me sukses';
            header("Location:../adm_panel.php");
            exit(); 
        } else {
            $_SESSION['error_mssg'] = 'Produkti nuk mund te fshihet, provo prap';
            header("Location:../edit_product.php?pid=" . $productID);
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        // You might want to handle this error more gracefully, such as logging it
    }
}
<?php
include "../../public/Classes/Dbh.php";
session_start();
// ... (rest of your code for getting form data)

if ($_SERVER["REQUEST_METHOD"] === "POST") {
try
{

    $productName = isset($_POST['productName']) ? $_POST['productName'] : '';
    $productDesc = isset($_POST['productDesc']) ? $_POST['productDesc'] : '';
    $productPrice = isset($_POST['productPrice']) ? $_POST['productPrice'] : '';
    $inStock = isset($_POST['yes']) ? 1 : 0; // Assuming 'yes' checkbox is checked
    $categoryID = isset($_POST['category']) ? $_POST['category'] : '';

    $mainImage = isset($_FILES['main_image']['name']) ? $_FILES['main_image']['name'] : '';
    $image1 = isset($_FILES['image1']['name']) && $_FILES['image1']['size'] > 0 ? $_FILES['image1']['name'] : null;
    $image2 = isset($_FILES['image2']['name']) && $_FILES['image2']['size'] > 0 ? $_FILES['image2']['name'] : null;
    $image3 = isset($_FILES['image3']['name']) && $_FILES['image3']['size'] > 0 ? $_FILES['image3']['name'] : null;
    $image4 = isset($_FILES['image4']['name']) && $_FILES['image4']['size'] > 0 ? $_FILES['image4']['name'] : null;
    $image5 = isset($_FILES['image5']['name']) && $_FILES['image5']['size'] > 0 ? $_FILES['image5']['name'] : null;
    $image6 = isset($_FILES['image6']['name']) && $_FILES['image6']['size'] > 0 ? $_FILES['image6']['name'] : null;
    if(empty($productName) ||
    empty($productDesc) || 
    empty($productPrice) || 
    empty($categoryID) || 
    empty($mainImage)  
    ) {
        throw new Exception('Fushat nuk duhen asnjera te zbrazta');
    }

} catch (Exception $e) {
    echo 'Fushat nuk duhen asnjera te zbrazta';
}

    function moveUploadedFile($tempFilePath, $destinationPath)
{
    if (move_uploaded_file($tempFilePath, $destinationPath)) {
        return true;
    } else {
        throw new Exception('Failed to move uploaded file');
    }
}

// ... (your existing code)

// Move main image
if (!empty($_FILES['main_image']['tmp_name'])) {
    $tempMainImage = $_FILES['main_image']['tmp_name'];
    $destinationMainImage = "../../resources/images/{$_FILES['main_image']['name']}";
    moveUploadedFile($tempMainImage, $destinationMainImage);
}

// Move additional images
$additionalImages = ['image1', 'image2', 'image3', 'image4', 'image5', 'image6'];

foreach ($additionalImages as $imageInputName) {
    if (!empty($_FILES[$imageInputName]['tmp_name'])) {
        $tempImage = $_FILES[$imageInputName]['tmp_name'];
        $destinationImage = "../../resources/images/" . $_FILES[$imageInputName]['name'];
        moveUploadedFile($tempImage, $destinationImage);
    }
}
    $selectedSizes = [];
    foreach ($_POST as $key => $value) {
        if (strpos($key, 'size_') === 0 && $value == 'on') {
            // Extract the size ID from the checkbox name
            $sizeID = substr($key, 5);
            $selectedSizes[] = $sizeID;
        }
    }
    include "../auth/MultiHandler.php";

    // Now you have the form data, you can proceed to use it
    // For example, you can use the ProductHandler class to insert the data into the database

    $productHandler = new MultiHandler(new Dbh);

    try {
        $insertResult = $productHandler->insertProductAndRelatedData($productName, $productDesc, $productPrice, $inStock,$categoryID,
        $mainImage, $image1, $image2,$image3,$image4,$image5,$image6,
        $selectedSizes);
        if ($insertResult) {
            $_SESSION['success_mssg'] = 'Produkti u Shtua me sukses';
            header("Location:../adm_panel.php");
            exit(); // Exit to prevent further script execution
        } else {
            $_SESSION['error_mssg'] = 'Produkti nuk mund te shtohet, provo prap';
            header("Location:../add_product.php");
            exit(); // Exit to prevent further script execution
        }
    } catch (Exception $e) {
        error_log("Error: " . $e->getMessage());
        // You might want to handle this error more gracefully, such as logging it
    }
}

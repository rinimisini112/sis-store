<?php
include "../../public/Classes/Dbh.php";
session_start();

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    include "../auth/MultiUpdater.php";
    $productUpdater = new MultiUpdater(new Dbh);

    $productID = isset($_POST['productID']) ? $_POST['productID'] : '';
    $productName = isset($_POST['productName']) ? $_POST['productName'] : '';
    $productDesc = isset($_POST['productDesc']) ? $_POST['productDesc'] : '';
    $productPrice = isset($_POST['productPrice']) ? $_POST['productPrice'] : '';
    $inStock = isset($_POST['yes']) ? 1 : 0;
    $categoryID = isset($_POST['category']) ? $_POST['category'] : '';

    // Fetch product data
    $editHandler = new MultiHandler(new Dbh());
    $productData = $editHandler->getProductData($productID);

    $mainImage = isset($_FILES['main_image']['name']) && $_FILES['main_image']['size'] > 0 ? $_FILES['main_image']['name'] : null;
    $image1 = isset($_FILES['image1']['name']) && $_FILES['image1']['size'] > 0 ? $_FILES['image1']['name'] : null;
    $image2 = isset($_FILES['image2']['name']) && $_FILES['image2']['size'] > 0 ? $_FILES['image2']['name'] : null;
    $image3 = isset($_FILES['image3']['name']) && $_FILES['image3']['size'] > 0 ? $_FILES['image3']['name'] : null;
    $image4 = isset($_FILES['image4']['name']) && $_FILES['image4']['size'] > 0 ? $_FILES['image4']['name'] : null;
    $image5 = isset($_FILES['image5']['name']) && $_FILES['image5']['size'] > 0 ? $_FILES['image5']['name'] : null;
    $image6 = isset($_FILES['image6']['name']) && $_FILES['image6']['size'] > 0 ? $_FILES['image6']['name'] : null;

    function moveUploadedFile($tempFilePath, $destinationPath)
    {
        if (move_uploaded_file($tempFilePath, $destinationPath)) {
            return true;
        } else {
            throw new Exception('Failed to move uploaded file');
        }
    }
   // Move main image
$destinationMainImage = "../../resources/images/" . $_FILES['main_image']['name'];
if (!empty($_FILES['main_image']['tmp_name'])) {
    $tempMainImage = $_FILES['main_image']['tmp_name'];
    moveUploadedFile($tempMainImage, $destinationMainImage);
}

// Move additional images
$additionalImages = ['image1', 'image2', 'image3', 'image4', 'image5', 'image6'];

foreach ($additionalImages as $imageInputName) {
    if (!empty($_FILES[$imageInputName]['tmp_name'])) {
        $tempImage = $_FILES[$imageInputName]['tmp_name'];
        $destinationImage = "../../resources/images/" . $_FILES[$imageInputName]['name'];
        moveUploadedFile($tempImage, $destinationImage);
    }
}

    if ($_FILES['main_image']['error'] == 4) {
        $mainImage = $productData['main_image']; // Use the existing image if no new file is uploaded
    } else {
        $mainImage = $_FILES['main_image']['name'];
    }

    $imageFields = ['image1', 'image2', 'image3', 'image4', 'image5', 'image6'];

    foreach ($imageFields as $field) {
        // Check if the delete checkbox is checked
        $deleteCheckbox = 'delete_' . $field;
        $deleteImage = isset($_POST[$deleteCheckbox]) && $_POST[$deleteCheckbox] === 'on';

        if ($_FILES[$field]['error'] == 4) {
            // No new file uploaded, use the existing image or null if delete checkbox is checked
            $$field = $deleteImage ? null : $productData[$field];
        } else {
            // New file uploaded
            $$field = $_FILES[$field]['name'];
        }
    }

    $selectedSizes = [];
    foreach ($_POST as $key => $value) {
        if (strpos($key, 'size_') === 0 && $value == 'on') {
            $sizeID = substr($key, 5);
            $selectedSizes[] = $sizeID;
        }
    }

    try {

        $updateResult = $productUpdater->updateProductAndRelatedData(
            $productID,
            $productName,
            $productDesc,
            $productPrice,
            $inStock,
            $categoryID,
            $mainImage,
            $image1,
            $image2,
            $image3,
            $image4,
            $image5,
            $image6,
            $selectedSizes
        );

        if ($updateResult) {
            $_SESSION['success_mssg'] = 'Produkti u ndryshua me sukses';
            header("Location:../edit_product.php?pid=" . $productID);
            exit();
        } else {
            $_SESSION['error_mssg'] = 'Produkti nuk mund te ndryshohet, provo prap';
            header("Location:../edit_product.php?pid=" . $productID);
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        // You might want to handle this error more gracefully, such as logging it
    }
}

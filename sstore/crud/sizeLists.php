<?php

$sizeHandler = new ProductHandler(new Dbh);

$letterSizes = $sizeHandler->fetchAllSizes("SizeName IN ('XS','S', 'M', 'L', 'XL', '2XL') ORDER BY sizeID ASC");
$numberSizes = $sizeHandler->fetchAllSizes("SizeName LIKE '3%' OR SizeName LIKE '4%'");

?>

<!-- HTML for letter sizes -->
<div>
    <p class="text-xl font-bold">Zgjidhi nese produkti i ka kto numra ne Etikete</p>
    <?php foreach ($letterSizes as $size): ?>
        <label class="text-2xl" for="<?php echo $size['SizeName']; ?>">
            <?php echo $size['SizeName']; ?>
        </label>
        <input type="checkbox" class=" accent-black w-5 h-5 pr-2" name="size_<?php echo $size['SizeID']; ?>" id="<?php echo $size['SizeName']; ?>">
    <?php endforeach; ?>
</div>

<!-- HTML for number sizes -->
<div>
    <p class="text-xl font-bold">Zgjidhi nese produkti i ka kto numra ne Etikete</p>
    <?php foreach ($numberSizes as $size): ?>
        <label class="text-2xl" for="<?php echo $size['SizeName']; ?>">
            <?php echo $size['SizeName']; ?>
        </label>
        <input type="checkbox" class="accent-black w-5 h-5 pr-2" name="size_<?php echo $size['SizeID']; ?>" id="<?php echo $size['SizeName']; ?>">
    <?php endforeach; ?>
</div>
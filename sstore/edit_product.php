<?php
session_start();
include "admInc/AdmHead.php";
include "../public/Classes/Dbh.php";
include "auth/Auth.php";
include "auth/MultiHandler.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: adm.php");
    exit();
}
if (isset($_GET['pid'])) {
    $productID = $_GET['pid'];
    $editHandler = new MultiHandler(new Dbh());

    $productData =  $editHandler->getProductData($productID);
}
?>

<body class="bg-sis-white">
    <main class="w-[80%] h-auto mb-6">
        <div class="w-full">
        <?php
        include 'admInc/productsNavbar.php';
        ?>
        <div class=" w-full text-center bg-gray-400 flex items-center justify-between pl-6 text-sis-white text-2xl font-bold">
            <p>Admin Paneli i SIS store - Nrysho te dhenat e Produktit</p>
            <a href="adm_panel.php" class="h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl font-bold">Shko mbrapa</a>
        </div>
        <form action="crud/productEdit.php" id="editProductForm" method="post" enctype="multipart/form-data" class="w-[80%  ] flex">
            <input type="hidden" name="productID" value="<?= $productID ?>">
            <div class="w-[30%] flex flex-col gap-y-6 border-r-2 border-r-sis-grey">
                <p class="bg-gray-600 text-sis-white text-center text-2xl font-bold py-2">Fotot aktuale te produkteve</p>
                <label for="main_image" class="px-4 pt-2">Fotoja Kryesore kjo foto del ne collections kur shfaqen krejt produktet.</label>
                <div id="mainImg" class="w-[95%] px-4 h-[300px]">
                    <img src="../resources/images/<?= $productData['main_image'] ?>" alt="" class="h-full w-full object-cover">
                </div>
                <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="main_image" id="main_image" value="<?= $productData['main_image'] ?>">
                <?php
                $imageFields = ['image1', 'image2', 'image3', 'image4', 'image5', 'image6'];

                foreach ($imageFields as $field) :
                ?>
                    <label for="<?= $field ?>" class="px-4 pt-2">Fotoja <?= substr($field, -1) + 0 ?> nuk osht obligative mu qit</label>
                    <div id="<?= 'img' . substr($field, -1) ?>" class="w-[95%] h-[300px]  px-4">
                        <?php
                        if (isset($productData[$field])) :
                        ?>
                            <img src="../resources/images/<?= $productData[$field] ?? '' ?>" alt="" class="h-full w-full object-cover">
                        <?php
                        endif;
                        ?>
                    </div>
                    <p class="flex items-center justify-center">Fshije foton <?= $field ?> <input type="checkbox" name="delete_image1"></p>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="<?= $field ?>" id="<?= $field ?>">
                <?php
                endforeach;
                ?>
            </div>
            <div class="w-[70%] flex flex-col gap-y-8">
                <p class="bg-gray-600 text-sis-white text-center flex items-center text-2xl font-bold justify-between pl-2 h-12">Produktet, shiko, ndryshoj ose fshij <a class="bg-red-500 px-4 flex items-center justify-center hover:bg-red-600 border border-red-700 duration-150 text-sis-grey h-full hover:text-sis-white" href="delete_product.php?pid=<?= $productID ?>">Fshij Produktin</a></p>
                <?php
                if (isset($_SESSION['error_mssg'])) {
                    echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['error_mssg'] . "</p>";
                    unset($_SESSION['error_mssg']);
                } else if (isset($_SESSION['success_mssg'])) {
                    echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['success_mssg'] . "</p>";
                    unset($_SESSION['success_mssg']);
                }
                ?>
                <div class="flex flex-col w-[60%] mx-auto gap-y-4">
                    <label for="productName">Emri i Produktit</label>
                    <input type="text" class="py-3 text-xl pl-3" name="productName" id="productName" value="<?php echo $productData['product_name'] ?? ''; ?>">
                    <label for="productDesc">Pershkrimi i Produktit</label>
                    <input type="text" class="py-3 text-xl pl-3" name="productDesc" id="productDesc" value="<?php echo $productData['product_description'] ?? ''; ?>">
                    <label for="productPrice">Qmimi</label>
                    <input type="text" class="py-3 text-xl pl-3" name="productPrice" id="productPrice" value="<?php echo $productData['price'] ?? ''; ?>">
                    <label for="" class="text-xl">A eshte in stock</label>
                    <div>
                        <div>
                            <label class="text-xl" for="yes">Po</label>
                            <input class="w-5 h-5 accent-black" type="checkbox" name="yes" id="yes" <?php echo ($productData['in_stock'] == 1) ? 'checked' : ''; ?>>
                            <label class="text-xl" for="no">Jo</label>
                            <input class="w-5 h-5 accent-black" type="checkbox" name="no" id="no" <?php echo ($productData['in_stock'] == 0) ? 'checked' : ''; ?>>
                        </div>
                    </div>
                    <?php
                    include "crud/productCategoryDrpd.php";
                    include 'crud/selectedSizeList.php';
                    ?>
                    <input type="submit" name="editProduct" id="editProduct" value='Ndrysho Te dhenat' class=" hover:bg-blue-700 bg-blue-300 hover:text-sis-white duration-100 text-xl font-bold border mx-auto border-blue-500 px-8 w-1/2 py-2 cursor-pointer">

                </div>
            </div>
        </form>
        </div>
        <?php include "admInc/productsMenu.php"?>
    </main>
    <script src="../dist/javascript/admVal.js"></script>
</body>

</html>
<?php
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../adm.php");
    exit();
}
if(isset($_GET['fid'])) {
$faqId = $_GET['fid'];
}

?>
<body class="bg-gray-300 w-full h-screen overflow-hidden flex items-center justify-center">
    <div class="w-1/2 h-[250px] bg-sis-white shadow-lg border-t border-t-red-400 shadow-red-600 rounded-md flex flex-col items-center justify-around">
        <p class="text-center py-4 text-2xl font-bold px-4">A jeni e sigurt qe deshironi te fshini Kategorine, ky veprim nuk mund te kthehet.</p>
        <form method="post" action="faqControllers/deleteFAQ.php">
            <input type="hidden" name="faqId" value="<?=$faqId ?>">
            <input class="px-12 rounded-xl py-3 border bg-red-600 cursor-pointer text-xl text-white font-extrabold hover:bg-red-400 duration-150 hover:text-sis-white" type="submit" value="Po Fshije Produktin" name="deleteFaq">
            <a class="px-12 rounded-xl py-3 border inline-block cursor-pointer text-xl text-white font-extrabold bg-blue-700 hover:bg-blue-400 duration-150 hover:text-sis-white" href="edit_faq.php?fid=<?=$faqId?>">Jo Kthehu mbrapa</a>
        </form>
    </div>
    <script>

    </script>
</body>

</html>
<?php
session_start();
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../adm.php");
    exit();
}
if(isset($_GET['fid'])) {
    $faqId = $_GET['fid'];

    include '../../public/Classes/FaqHandler.php';
    $faqHandler = new FaqHandler(new Dbh);
    $editFaq =  $faqHandler->readSingleFAQ($faqId);
}
?>
<body class="bg-sis-white">
    <main class="w-[80%] h-auto mb-6 pb-2">
        <div>
        <?php
        include "../admInc/categoriesNavbar.php";
        ?>
        <div class="">
        <div class=" w-full text-center bg-gray-400 flex items-center justify-between pl-6 text-sis-white text-2xl font-bold">
            <p>Frequently asked question, shto, ndrysho dhe fshij Pyetjet</p>
            <a href="faq_panel.php" class="h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl font-bold">Shko mbrapa</a>
        </div>
        <form action="faqControllers/editFAQ.php" id="FAQform" method="post" class="w-full pb-6 flex flex-col">
                <p class="bg-gray-600 text-sis-white text-center flex items-center text-2xl font-bold justify-between pl-2 h-12">Ktu munesh shtu Frequently asked questions, kto dalin te Contact Us
                <a class="bg-red-500 px-4 flex items-center justify-center hover:bg-red-600 border border-red-700 duration-150 text-sis-grey h-full hover:text-sis-white" href="delete_faq.php?fid=<?= $faqId ?>">Fshij Pyetjen</a>
                </p>
                <?php
                if (isset($_SESSION['error_mssg'])) {
                    echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['error_mssg'] . "</p>";
                    unset($_SESSION['error_mssg']);
                } else if (isset($_SESSION['success_mssg'])) {
                    echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['success_mssg'] . "</p>";
                    unset($_SESSION['success_mssg']);
                }
                ?>
                <div class="flex flex-col w-[60%] mx-auto gap-y-4">
                    <input type="hidden" name="faqId" value="<?=$editFaq['faq_id']?>">;
                    <label class="text-xl font-bold pt-3" for="productName">Pyetja</label>
                    <textarea type="text" class=" max-w-full max-h-32 h-32 min-h-full outline outline-black text-xl pl-3" name="question" id="question"><?=$editFaq['question']?></textarea>
                    <label class="text-xl font-bold pt-3" for="productDesc">Pergjigja</label>
                    <textarea type="text" class=" max-w-full h-60 max-h-60 min-h-full outline outline-black text-xl pl-3" name="answer" id="answer"><?=$editFaq['answer']?></textarea>
                        </div>
                    <?php
                    ?>
                    <input type="submit" name="editFAQ" id="editFAQ" value='Ndrysho Pyetjen' class=" hover:bg-blue-700 bg-blue-300 hover:text-sis-white duration-100 text-xl font-bold border mx-auto border-blue-500 px-8 w-1/2 py-2 mt-8 cursor-pointer">

                </div>
        </form>
        </div>
            <?php include "../admInc/faqMenu.php"?>
    </main>
<script>
       $(document).ready(function() {
    $.validator.addMethod("alphabetsOnly", function(value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");

    $("#FAQform").validate({
        rules: {
            question: {
                required: true,
            },
            answer: {
                required: true,
            },
        },
        messages: {
            question: {
                required: "Fusha nuk duhet te jete e zbrazt",
            },
            answer: {
                required: "Fusha nuk duhet te jete e zbrazt",
            },

        },
        errorPlacement: function(error, element) {
            // Place the error message below the input element
            error.insertAfter(element);

            // Add a class to the input when there is an error
            element.addClass("error_effect");
        },
        success: function(label, element) {
            // Remove the error class when the input is valid
            $(element).removeClass("error_effect");
        }
    });
});
    $(document).ready(function() {
    // Wait for the document to be ready
    setTimeout(function() {
        $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
    }, 5000); // 5000 milliseconds = 5 seconds
});
</script>
</body>

</html>
<?php
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['addFAQ'])) {    
    $question = $_POST['question'];
    $answer = $_POST['answer'];

    try {
    include '../../../public/Classes/Dbh.php';
    include '../../../public/Classes/FaqHandler.php';

    $faqHandler = new FaqHandler(new Dbh);
    $insertFaq = $faqHandler->insertFAQ($question,$answer);
    
    if ($insertFaq) {
        $_SESSION['success_mssg'] = 'Pyetja u Shtua me sukses';
        header("Location:../faq_panel.php");
        exit();
    } else {
        $_SESSION['error_mssg'] = 'Pyetja nuk mund te shtohet, provo prap';
        header("Location:../addFAQ.php");
        exit(); 
    }
    } catch (Exception $e) {
        echo "Diqka shkoj keq na vjen keq";
    }
}

header('Location: ../../../index.php');
<?php
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteFaq'])) {
    include "../../../public/Classes/Dbh.php";
    include "../../../public/Classes/FaqHandler.php";

    try {

        $FaqHandler = new FaqHandler(new Dbh);
        $faqId = $_POST['faqId'];
        $result = $FaqHandler->deleteFaq($faqId);
        if ($result) {
            $_SESSION['success_mssg'] = 'Pyetja u fshi me sukses';
            header("Location:../faq_panel.php");
            exit(); 
        } else {
            $_SESSION['error_mssg'] = 'Pyetja nuk mundi te fshihet, provo prap';
            header("Location:../edit_faq.php?fid=" . $faqId);
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        // You might want to handle this error more gracefully, such as logging it
    }
}


<?php 
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['editFAQ'])) {
    $faqId = $_POST['faqId'];
    $question = $_POST['question'];
    $answer = $_POST['answer'];

    try {
        include '../../../public/Classes/Dbh.php';
        include '../../../public/Classes/FaqHandler.php';

        $faqHandler = new FaqHandler(new Dbh);
        $updateFaq = $faqHandler->updateFAQ($faqId,$question,$answer);
        if($updateFaq) {
            $_SESSION['success_mssg'] = 'Pyetja u Ndryshua me sukses';
            header("Location:../edit_faq.php?fid={$faqId}");
            exit();
        } else {
            $_SESSION['error_mssg'] = 'Pyetja nuk mund te ndryshohet, provo prap';
            header("Location:../edit_faq.php?fid={$faqId}");
            exit(); 
        }
    } catch (Exception $e) {
        echo "Diqka shkoj keq!";
    }
}
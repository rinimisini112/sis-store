<?php 
include "../../public/Classes/FaqHandler.php";

$faqQuestions = new FaqHandler(new Dbh);
$questions = $faqQuestions->readFAQ();
foreach ($questions as $index => $row) {
    $question = $row['question'];
    $answer = $row['answer']; 
    $faqId = $row['faq_id'];

    $bgColorClass = ($index % 2 == 0) ? 'bg-slate-400' : 'bg-gray-300';

    echo '<tr class="h-auto w-full ' . $bgColorClass . '">';
    echo '<td class="border-4 py-2 px-3 text-xl font-bold border-sis-white text-center">' . $question . '</td>';
    echo '<td class="border-4 py-2 px-3 text-xl font-bold border-sis-white text-center">' . $answer . '</td>'; // Display the product count
    echo '<td class="border-4 border-sis-white text-center underline underline-offset-1 text-2xl font-bold bg-sis-grey text-sis-white hover:bg-green-500 duration-200 "><a href="edit_faq.php?fid=' . $faqId . '">Edit</td>';
    echo '</tr>';
}
?>
<?php
session_start();
include "../../public/Classes/Dbh.php";
include "../../public/Classes/CMS.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

if (!$auth->isAuthTokenValid()) {
    header("Location: adm.php");
    exit();
}
include "../admInc/categoriesHead.php";
?>

<body class="bg-sis-white">
    <header>
        <?php
        include '../admInc/cmsNavbar.php';
        ?>
    </header>
    <main class="w-[95%] mx-auto mt-12">
        <p class="text-4xl pb-8 font-bold text-center">Pjesa per editimin e fotove ne Homepage, me posht i keni videot qe ju tregojn ne cilen pjes dhe cilen foto jeni duke e ndryshuar.</p>
        <div class="flex flex-col pt-16">
            <div class="w-[70%] mx-auto">
                <video src="instructions.mp4" controls type='mp4/mpeg'></video>
            </div>
            <form action="updateCMS.php" method="post" class="w-full mx-auto flex flex-col items-center pt-6">
                <?php
                if (isset($_SESSION['error_mssg'])) {
                    echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['error_mssg'] . "</p>";
                    unset($_SESSION['error_mssg']);
                } else if (isset($_SESSION['success_mssg'])) {
                    echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['success_mssg'] . "</p>";
                    unset($_SESSION['success_mssg']);
                }
                ?>
                <div class="flex items-center w-full pb-16">
                    <div class="w-2/5">
                        <label class="text-2xl font-bold pt-4" for="first_image">Ktu ndryshohet produkti per foton 1 si ne video</label>
                        <?php
                        $CMS = new CMS(new Dbh);
                        $cmsId = $CMS->fetchCmsID();
                        $firstImgId = $cmsId['secondBanerFirstImg'];
                        $secondImgId = $cmsId['secondBanerSecondImg'];
                        $displayedImg1 = $CMS->fetchProductWithId($firstImgId);
                        $displayedImg2 = $CMS->fetchProductWithId($secondImgId);
                        $productOptions = $CMS->fetchAllProducts($firstImgId);
                        $productOptions2 = $CMS->fetchAllProducts($secondImgId);
                        ?>

                        <p class="text-xl font-bold">Kjo eshte fotoja qe esht momentalisht te fotoja 1</p>
                        <img id="productImage1" src="../../<?= $displayedImg1['main_image'] ?>" alt="" class="w-[90%] mx-auto object-cover h-[450px]">
                    </div>
                    <div class="w-3/5 flex flex-wrap overflow-y-scroll h-[600px] gap-3 relative shadow-md">
                        <p class="text-center pt-3 pb-4 font-bold text-2xl w-[32%]">Zgjidh produktin qe don me vendos</p>
                        <?php
                        foreach ($productOptions as $product) {
                            echo "<div class='w-[32%] h-[250px]'>";
                            echo "<input type='radio' name='selectedProduct' value='" . $product['product_id'] . "' id='product" . $product['product_id'] . "'>";
                            echo "<label for='product" . $product['product_id'] . "'>";
                            echo "<img class='w-full h-[95%] object-cover' src=../../" . $product['main_image'] . ">";
                            echo "<p class='text-center '>" . $product['product_name'] . "<p>";
                            echo "</label>";
                            echo "</div>";
                        }
                        ?>
                    </div>
                </div>
                <div class="flex flex-col items-center pt-32 border-t-2 border-t-sis-grey">
                    <div class="flex items-center w-full">
                        <div class="w-2/5">
                            <label class="text-2xl font-bold pt-4" for="first_image">Ktu ndryshohet produkti per foton 2 si ne video</label>
                            <p class="text-xl font-bold">Kjo eshte fotoja qe esht momentalisht te fotoja 2</p>
                            <img id="productImage1" src="../../<?= $displayedImg2['main_image'] ?>" alt="" class="w-[90%] mx-auto object-cover h-[450px]">
                        </div>
                        <div class="w-3/5 flex flex-wrap overflow-y-scroll h-[600px] gap-3 relative shadow-md">
                            <p class="text-center pt-3 pb-4 font-bold text-2xl w-[32%]">Zgjidh produktin qe don me vendos</p>
                            <?php
                            foreach ($productOptions2 as $product) {
                                echo "<div class='w-[32%] h-[250px]'>";
                                echo "<input type='radio' name='2selectedProduct'  value='" . $product['product_id'] . "' id='2secondProduct" . $product['product_id'] . "'>";
                                echo "<label for='2secondProduct" . $product['product_id'] . "'>";
                                echo "<img class='w-full h-[95%] object-cover' src=../../" . $product['main_image'] . ">";
                                echo "<p class='text-center '>" . $product['product_name'] . "<p>";
                                echo "</label>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    </div>
                    <input type="submit" name="changeSecondBanner" value="Kliko ktu per me i Ndryshu Fotot e para" class="text-xl font-bold cursor-pointer mt-12 mb-6 w-[60%] py-3 bg-blue-500 hover:bg-blue-700 duration-150 hover:text-sis-white border border-blue-700">
            </form>
            <div class="flex flex-col pt-16">
            <div class="w-[70%] mx-auto">
                <video src="instructions2.mp4" controls type='mp4/mpeg'></video>
            </div>
            <form action="updateCMS.php" method="post" class="w-full mx-auto flex flex-col items-center pt-6">
                <div class="flex items-center w-full pb-16">
                    <div class="w-2/5">
                        <label class="text-2xl font-bold pt-4" for="first_image">Ktu ndryshohet produkti per foton 3 si ne video</label>
                        <?php
                        $cmsId = $CMS->fetchCmsID();
                        $whiteBannerFirstImgId = $cmsId['whiteBanerFirstImg'];
                        $whiteBannerSecondImgId = $cmsId['whiteBanerSecondImg'];
                        $displayedImg1 = $CMS->fetchProductWithId($whiteBannerFirstImgId);
                        $displayedImg2 = $CMS->fetchProductWithId($whiteBannerSecondImgId);
                        $productOptions = $CMS->fetchAllProducts($firstImgId);
                        $productOptions2 = $CMS->fetchProductsWith2Img();
                        ?>

                        <p class="text-xl font-bold">Kjo eshte fotoja qe esht momentalisht te fotoja 3</p>
                        <img id="productImage1" src="../../<?= $displayedImg1['main_image'] ?>" alt="" class="w-[90%] mx-auto object-cover h-[450px]">
                    </div>
                    <div class="w-3/5 flex flex-wrap overflow-y-scroll h-[600px] gap-3 relative shadow-md">
                        <p class="text-center pt-3 pb-4 font-bold text-2xl w-[32%]">Zgjidh produktin qe don me vendos</p>
                        <?php
                        foreach ($productOptions as $product) {
                            echo "<div class='w-[32%] h-[250px]'>";
                            echo "<input type='radio' name='whiteBannerProduct' value='" . $product['product_id'] . "' id='whiteBanner" . $product['product_id'] . "'>";
                            echo "<label for='whiteBanner" . $product['product_id'] . "'>";
                            echo "<img class='w-full h-[95%] object-cover' src=../../" . $product['main_image'] . ">";
                            echo "<p class='text-center '>" . $product['product_name'] . "<p>";
                            echo "</label>";
                            echo "</div>";
                        }
                        ?>
                    </div>
                </div>
                <div class="flex flex-col items-center pt-32 border-t-2 border-t-sis-grey">
                    <div class="flex items-center w-full">
                        <div class="w-2/5">
                            <label class="text-2xl font-bold pt-4" for="first_image">Ktu ndryshohet produkti per foton 1 dhe 2 si ne video</label>
                            <p class="text-xl font-bold">Kjo eshte fotoja qe esht momentalisht te fotoja 1 dhe 2</p>
                            <img id="productImage1" src="../../<?= $displayedImg2['main_image'] ?>" alt="" class="w-[90%] mx-auto object-cover h-[450px]">
                        </div>
                        <div class="w-3/5 flex flex-wrap overflow-y-scroll h-[600px] gap-3 relative shadow-md">
                            <p class="text-center pt-3 pb-4 font-bold text-2xl w-[32%]">Kto jan krejt produktet qe i kan nga 2 foto qe mujna mi vendos ktu.</p>
                            <?php
                            foreach ($productOptions2 as $product) {
                                echo "<div class='w-[32%] h-[250px]'>";
                                echo "<input type='radio' name='2whiteBannerProduct'  value='" . $product['product_id'] . "' id='2whiteBanner" . $product['product_id'] . "'>";
                                echo "<label for='2whiteBanner" . $product['product_id'] . "'>";
                                echo "<img class='w-full h-[95%] object-cover' src=../../" . $product['main_image'] . ">";
                                echo "<p class='text-center '>" . $product['product_name'] . "<p>";
                                echo "</label>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    </div>
                    <input type="submit" name="changeWhiteBanner" value="Kliko ktu per me i Ndryshu renin e 2 te fotove" class="text-xl font-bold cursor-pointer mt-12 mb-6 w-[60%] py-3 bg-blue-500 hover:bg-blue-700 duration-150 hover:text-sis-white border border-blue-700">
            </form>
            </div>
    </main>
    <script>
        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 5000); // 5000 milliseconds = 5 seconds
        });
    </script>
</body>

</html>
<?php
session_start();
include "../../public/Classes/Dbh.php";
include "../../public/Classes/CMS.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['changeSecondBanner'])) {
    $selectedProductId = $_POST['selectedProduct'];
    $selectedProductId2 = $_POST['2selectedProduct'];

    $CMS = new CMS(new Dbh);

    try {

        $result = $CMS->updateSecondBanner($selectedProductId, $selectedProductId2);
        if($result) {
            $_SESSION['success_mssg'] = 'Fotot u ndryshua me sukses';
            header("Location:homepageControl.php");
            exit();
        } else {
            $_SESSION['error_mssg'] = 'Fotot nuk mund te ndryshohet, provo prap';
            header("Location:homepageControl.php");
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['changeWhiteBanner'])) {
    $selectedProductId = $_POST['whiteBannerProduct'];
    $selectedProductId2 = $_POST['2whiteBannerProduct'];

    $CMS = new CMS(new Dbh);

    try {

        $result = $CMS->updateWhiteBanner($selectedProductId, $selectedProductId2);
        if($result) {
            $_SESSION['success_mssg'] = 'Fotot u ndryshua me sukses';
            header("Location:homepageControl.php");
            exit();
        } else {
            $_SESSION['error_mssg'] = 'Fotot nuk mund te ndryshohet, provo prap';
            header("Location:homepageControl.php");
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
}
?>
<?php
$colors = $categoryHandler->fetchColors();
$colorSelected = $categoryHandler->fetchColors($productID);

// Assuming $colorSelected is a single color
$colorSelected = ($colorSelected) ? $colorSelected->color : null;
?>
<p>Zgjidh Ngjyren, nese nuk deshironi ta ndryshoni ngjyren mos shenoni ne fushen e dyte</p>
<select name="colorList" id="colorList" class="py-2 pl-2">
    <?php foreach ($colors as $color): ?>
        <option value="<?php echo $color->color; ?>" <?php echo ($color->color == $colorSelected) ? 'selected' : ''; ?>>
            <?php echo ucfirst($color->color); ?>
        </option>
    <?php endforeach; ?>
</select>

<label for="color">Ose Shto Ngjyre te re nese nuk eshte ne liste</label>
<input type="text" name="color" id="color" class="py-2" placeholder="Lere te zbrazet nese nuk don me e ndryshu ngjyren">
<?php

$colors = $categoryHandler->fetchColors();
?>
<p>Zgjidh Ngjyren</p>
<select name="colorList" id="colorList" class="py-2 pl-2">
    <option value="0">Zgjidh Ngjyren</option>
    
    <?php 
    foreach ($colors as $color): ?>
        <option value="<?php echo $color->color; ?>">
            <?php echo $color->color; ?>
        </option>
    <?php endforeach; ?>
</select>
<label for="color">Ose Shto Ngjyre te re nese nuk eshte ne liste</label>
<input type="text" name="color" id="color" class="py-2" placeholder="Sheno ngjyre te re nese deshiron">

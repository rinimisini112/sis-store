<?php
$categoryHandler = new JewelryAdmin();
$categories = $categoryHandler->fetchCategories();
$selectedCategory = $categoryHandler->fetchCategories($productID);
?>

<select name="category" id="category" class="py-2 pl-2">
    <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category->id; ?>" <?php echo ($category->id == $selectedCategory->id) ? 'selected' : ''; ?>>
            <?php echo $category->name; ?>
        </option>
    <?php endforeach; ?>
</select>
<?php
include "../../../public/Classes/Dbh.php";
include "../../auth/JewelryAdmin.php";
session_start();
// ... (rest of your code for getting form data)

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    try {

        $productName = isset($_POST['productName']) ? $_POST['productName'] : '';
        $productDesc = isset($_POST['productDesc']) ? $_POST['productDesc'] : '';
        $productPrice = isset($_POST['productPrice']) ? $_POST['productPrice'] : '';
        $categoryID = isset($_POST['category']) ? $_POST['category'] : '';

        // Check if the dropdown has a selected value, otherwise use the input field
        $materialList = isset($_POST['materialList']) ? $_POST['materialList'] : '';
        $material = !empty($materialList) ? $materialList : (isset($_POST['material']) ? $_POST['material'] : '');

        // Check if the dropdown has a selected value, otherwise use the input field
        $colorList = isset($_POST['colorList']) ? $_POST['colorList'] : '';
        $color = !empty($colorList) ? $colorList : (isset($_POST['color']) ? $_POST['color'] : '');

        $mainImage = isset($_FILES['main_image']['name']) ? $_FILES['main_image']['name'] : '';
        $image1 = isset($_FILES['image1']['name']) && $_FILES['image1']['size'] > 0 ? $_FILES['image1']['name'] : null;
        $image2 = isset($_FILES['image2']['name']) && $_FILES['image2']['size'] > 0 ? $_FILES['image2']['name'] : null;
        $image3 = isset($_FILES['image3']['name']) && $_FILES['image3']['size'] > 0 ? $_FILES['image3']['name'] : null;
        $image4 = isset($_FILES['image4']['name']) && $_FILES['image4']['size'] > 0 ? $_FILES['image4']['name'] : null;
        $image5 = isset($_FILES['image5']['name']) && $_FILES['image5']['size'] > 0 ? $_FILES['image5']['name'] : null;
        if (
            empty($productName) ||
            empty($productDesc) ||
            empty($productPrice) ||
            empty($categoryID) ||
            empty($mainImage)
        ) {
            throw new Exception('Fushat nuk duhen asnjera te zbrazta');
        }


        function moveUploadedFile($tempFilePath, $destinationPath)
        {
            if (move_uploaded_file($tempFilePath, $destinationPath)) {
                return true;
            } else {
                throw new Exception('Failed to move uploaded file');
            }
        }


        if (!empty($_FILES['main_image']['tmp_name'])) {
            $tempMainImage = $_FILES['main_image']['tmp_name'];
            $destinationMainImage = "../../../resources/images/{$_FILES['main_image']['name']}";
            moveUploadedFile($tempMainImage, $destinationMainImage);
        }

        // Move additional images
        $additionalImages = ['image1', 'image2', 'image3', 'image4', 'image5'];

        foreach ($additionalImages as $imageInputName) {
            if (!empty($_FILES[$imageInputName]['tmp_name'])) {
                $tempImage = $_FILES[$imageInputName]['tmp_name'];
                $destinationImage = "../../../resources/images/" . $_FILES[$imageInputName]['name'];
                moveUploadedFile($tempImage, $destinationImage);
            }
        }
        // Now you have the form data, you can proceed to use it
        // For example, you can use the ProductHandler class to insert the data into the database

        $productHandler = new JewelryAdmin();

        $insertResult = $productHandler->insertJewelryWithImages(
            $productName,
            $productDesc,
            $productPrice,
            $material,
            $color,
            $categoryID,
            $mainImage,
            $image1,
            $image2,
            $image3,
            $image4,
            $image5
        );
        if ($insertResult) {
            $_SESSION['success_mssg'] = 'Bizhuteria u Shtua me sukses';
            header("Location:../jewelry_panel.php");
            exit(); // Exit to prevent further script execution
        } else {
            throw new Exception('Bizhuteria nuk mund te shtohet, provo prap');
            exit(); // Exit to prevent further script execution
        }
    } catch (Exception $e) {
        $_SESSION['error_mssg'] = $e->getMessage();
        error_log($e->getMessage());
        header("Location:../jewelry_add.php");
    }
}

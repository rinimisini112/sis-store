<?php
include "../auth/JewelryAdmin.php";

$categoryHandler = new JewelryAdmin();
$categories = $categoryHandler->fetchCategories();
?>

<select name="category" id="category" class="py-2 pl-2">
    <option value="0">Zgjidh kategorin</option>
    
    <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category->id; ?>">
            <?php echo $category->name; ?>
        </option>
    <?php endforeach; ?>
</select>
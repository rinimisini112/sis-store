<?php
include "../../../public/Classes/Dbh.php";
session_start();

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    try {
        include "../../auth/JewelryAdmin.php";
        $productUpdater = new JewelryAdmin();

        $productID = isset($_POST['productID']) ? $_POST['productID'] : '';
        $productName = isset($_POST['productName']) ? $_POST['productName'] : '';
        $productDesc = isset($_POST['productDesc']) ? $_POST['productDesc'] : '';
        $productPrice = isset($_POST['productPrice']) ? $_POST['productPrice'] : '';
        $categoryID = isset($_POST['category']) ? $_POST['category'] : '';

        $materialList = isset($_POST['materialList']) ? $_POST['materialList'] : '';
        $material = !empty($materialList) ? $materialList : (isset($_POST['material']) ? $_POST['material'] : '');

        // Check if the dropdown has a selected value, otherwise use the input field
        $colorList = isset($_POST['colorList']) ? $_POST['colorList'] : '';
        $color = !empty($colorList) ? $colorList : (isset($_POST['color']) ? $_POST['color'] : '');

        $productData = $productUpdater->fetchJewelryAdmin($productID);

        $mainImage = isset($_FILES['main_image']['name']) && $_FILES['main_image']['size'] > 0 ? $_FILES['main_image']['name'] : null;
        $image1 = isset($_FILES['image1']['name']) && $_FILES['image1']['size'] > 0 ? $_FILES['image1']['name'] : null;
        $image2 = isset($_FILES['image2']['name']) && $_FILES['image2']['size'] > 0 ? $_FILES['image2']['name'] : null;
        $image3 = isset($_FILES['image3']['name']) && $_FILES['image3']['size'] > 0 ? $_FILES['image3']['name'] : null;
        $image4 = isset($_FILES['image4']['name']) && $_FILES['image4']['size'] > 0 ? $_FILES['image4']['name'] : null;
        $image5 = isset($_FILES['image5']['name']) && $_FILES['image5']['size'] > 0 ? $_FILES['image5']['name'] : null;
        function moveUploadedFile($tempFilePath, $destinationPath)
        {
            if (move_uploaded_file($tempFilePath, $destinationPath)) {
                return true;
            } else {
                throw new Exception('Failed to move uploaded file');
            }
        }
        // Move main image
        $destinationMainImage = "../../../resources/images/" . $_FILES['main_image']['name'];
        if (!empty($_FILES['main_image']['tmp_name'])) {
            $tempMainImage = $_FILES['main_image']['tmp_name'];
            moveUploadedFile($tempMainImage, $destinationMainImage);
        }

        // Move additional images
        $additionalImages = ['image1', 'image2', 'image3', 'image4', 'image5'];

        foreach ($additionalImages as $imageInputName) {
            if (!empty($_FILES[$imageInputName]['tmp_name'])) {
                $tempImage = $_FILES[$imageInputName]['tmp_name'];
                $destinationImage = "../../../resources/images/" . $_FILES[$imageInputName]['name'];
                moveUploadedFile($tempImage, $destinationImage);
            }
        }

        if ($_FILES['main_image']['error'] == 4) {
            $mainImage = $productData->main_image; // Use the existing image if no new file is uploaded
        } else {
            $mainImage = $_FILES['main_image']['name'];
        }

        $imageFields = ['image1', 'image2', 'image3', 'image4', 'image5'];

        foreach ($imageFields as $field) {
            // Check if the delete checkbox is checked
            $deleteCheckbox = 'delete_' . $field;
            $deleteImage = isset($_POST[$deleteCheckbox]) && $_POST[$deleteCheckbox] === 'on';

            if ($_FILES[$field]['error'] == 4 && !$deleteImage) {
                // No new file uploaded, use the existing image if delete checkbox is not checked
                $$field = $productData->$field;
            } elseif ($_FILES[$field]['error'] == 4 && $deleteImage) {
                // No new file uploaded, and delete checkbox is checked, set the image field to null
                $$field = null;
            } else {
                // New file uploaded
                $$field = $_FILES[$field]['name'];
            }
        }

        $updateResult = $productUpdater->updateJewelryWithImages(
            $productID,
            $productName,
            $productDesc,
            $productPrice,
            $material,
            $color,
            $categoryID,
            $mainImage,
            $image1,
            $image2,
            $image3,
            $image4,
            $image5,
            $image6
                );

        if ($updateResult) {
            $_SESSION['success_mssg'] = 'Bizhuteria u ndryshua me sukses';
            header("Location:../jewelry_edit.php?pid=" . $productID);
            exit();
        } else {
            throw new Exception('Produkti nuk mund te ndryshohet, provo prap');
            exit();
        }
    } catch (Exception $e) {
        $_SESSION['error_mssg'] = $e->getMessage();
        header("Location:../jewelry_edit.php?pid=" . $productID);

        // You might want to handle this error more gracefully, such as logging it
    }
}

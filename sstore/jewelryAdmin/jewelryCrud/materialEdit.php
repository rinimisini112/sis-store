<?php

$materials = $categoryHandler->fetchMaterials();
$materialSelected = $categoryHandler->fetchMaterials($productID);
?>

<p>Nese nuk deshiron ta ndryshosh materialin mos e mbush asnjeren nga fushat</p>

<select name="materialList" id="materialList" class="py-2 pl-2">
    <?php foreach ($materials as $material): ?>
        <option value="<?php echo $material->material; ?>" <?php echo ($material->material == $materialSelected) ? 'selected' : ''; ?>>
            <?php echo ucfirst($material->material); ?>
        </option>
    <?php endforeach; ?>
</select>

<label for="material">Ose Shto te ri nese nuk eshte ne liste</label>
<input type="text" name="material" id="material" class="py-2" placeholder="Nese nuk don me ndryshu lere kete fush te zbrazet" <?php echo ($materialSelected === null) ? '' : 'disabled'; ?>>
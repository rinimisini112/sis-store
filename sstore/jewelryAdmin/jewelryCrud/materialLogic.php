<?php

$materials = $categoryHandler->fetchMaterials();
?>
<p>Zgjidh Materialin</p>
<select name="materialList" id="materialList" class="py-2 pl-2">
    <option value="0">Zgjidh Materialin</option>
    
    <?php 
    foreach ($materials as $material): 
$materialName = ucfirst($material->material);
?>
        <option value="<?php echo $material->material; ?>">
            <?php echo $materialName; ?>
        </option>
    <?php endforeach; ?>
</select>
<label for="material">Ose Shto te ri nese nuk eshte ne liste</label>
<input type="text" name="material" id="material" class="py-2" placeholder="Sheno material te ri nese deshiron">

<?php
session_start();
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: adm.php");
    exit();
}
if (isset($_GET['pid'])) {
    $productID = $_GET['pid'];


    try {
        if (!isset($productID)) {
            throw new Exception('Nuk keni zgjedhur produkt per te fshir kthenuni mbrapa');
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteProduct'])) {
            include "../../../public/Classes/Dbh.php";
            include "../auth/JewelryAdmin.php";

            $productDelete = new JewelryAdmin();
            $productID = $_POST['productID'];
            $result = $productDelete->deleteJewelry($productID);
            if ($result) {
                $_SESSION['success_mssg'] = 'Bizhuteria u fshi me sukses';
                header("Location:jewelry_panel.php");
                exit();
            } else {
                throw new Exception('Produkti nuk mund te fshihet, provo prap');
                exit();
            }
        }
    } catch (Exception $e) {
        $_SESSION['error_mssg'] =  $e->getMessage();
        header("Location:jewlry_edit.php?pid=" . $productID);
    }
}
?>

<body class="bg-gray-300 w-full h-screen overflow-hidden flex items-center justify-center">
    <div class="w-1/2 h-[250px] bg-sis-white shadow-lg border-t border-t-red-400 shadow-red-600 rounded-md flex flex-col items-center justify-around">
        <p class="text-center py-4 text-2xl px-4 font-bold">A jeni e sigurt qe deshironi te fshini bizhuterin, ky veprim nuk mund te kthehet.</p>
        <form method="post" action="">
            <input type="hidden" name="productID" value="<?= $productID ?>">
            <input class="px-12 rounded-xl py-3 border bg-red-600 cursor-pointer text-xl text-white font-extrabold hover:bg-red-400 duration-150 hover:text-sis-white" type="submit" value="Po Fshije Produktin" name="deleteProduct">
            <a class="px-12 rounded-xl py-3 border inline-block cursor-pointer text-xl text-white font-extrabold bg-blue-700 hover:bg-blue-400 duration-150 hover:text-sis-white" href="jewelry_edit.php?pid=<?= $productID ?>">Jo Kthehu mbrapa</a>
        </form>
    </div>
    <script>

    </script>
</body>

</html>
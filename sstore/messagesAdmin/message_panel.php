<?php
session_start();
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../adm.php");
    exit();
}
?>
<body class="bg-sis-white">
    <main class="w-[80%] h-auto mb-6 pb-2">
        <div class="w-full">   
        <?php 
        include "../admInc/categoriesNavbar.php";
        ?>
        <div class="">
        <div class=" w-full text-center bg-gray-400 flex justify-between pl-6 py-2 text-sis-white text-2xl font-bold">
            <p>Admin Paneli i SIS store - Mesazhet, shiko dhe fshij mesazhet</p>
            </div>
        </div>
        <?php
        if (isset($_SESSION['error_mssg'])) {
            echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['error_mssg'] . "</p>";
            unset($_SESSION['error_mssg']);
        } else if (isset($_SESSION['success_mssg'])) {
            echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     right-8 bottom-8 w-1/2 h-[100px]'>" . $_SESSION['success_mssg'] . "</p>";
            unset($_SESSION['success_mssg']);
        }
        ?>
        <div class="w-full h-auto flex flex-col items-center">
            <table class="w-full border-none outline-none" style="border: none!important;">
                <thead class="">
                <tr class="w-full shadow-lg bg-gradient-to-r from-[#8e5ac3] via-[#8453b4] to-[#774ca2] text-sis-white text-xl">
                        <th class="bg-[#774ca2]">Username</th>
                        <th class="">Emaili</th>
                        <th class="bg-[#774ca2]">Mesazhet nga perdoruesit</th>
                        <th class="">Fshije</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'mssgControllers/fetchMessages.php';
                    ?>
                </tbody>
            </table>
        </div>
        </div>
        <?php include "../admInc/messagesMenu.php"?>

    </main>
    <script>
     
    document.addEventListener("DOMContentLoaded", function () {
        // Get the categoryForm and initial height
        var categoryForm = document.getElementById("categoryForm");
        var initialHeight = categoryForm.offsetHeight;

        // Initially hide the form
        gsap.set(categoryForm, { height: 0, overflow: "hidden" });

        // Toggle the form on link click
        document.querySelector("a[href='add_product.php']").addEventListener("click", function (event) {
            event.preventDefault(); // Prevent the default link behavior

            // Toggle the form's height
            gsap.to(categoryForm, { height: categoryForm.offsetHeight === 0 ? initialHeight : 0, duration: 0.3 });
        });
    });

        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 5000); // 5000 milliseconds = 5 seconds
        });
    </script>
</body>

</html>
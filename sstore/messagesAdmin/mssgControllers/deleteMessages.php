<?php
session_start();
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteMessage'])) {
    include "../../../public/Classes/Dbh.php";
    include "../../../public/Classes/MessageHandler.php";

    try {

        $messageHandler = new MessageHandler(new Dbh);
        $messageId = $_POST['messageId'];
        $result = $messageHandler->deleteMessage($messageId);
        if ($result) {
            $_SESSION['success_mssg'] = 'Mesazhi u fshi me sukses';
            header("Location:../message_panel.php");
            exit(); 
        } else {
            $_SESSION['error_mssg'] = 'Mesazhi nuk mundi te fshihet, provo prap';
            header("Location:../message_panel.php");
            exit();
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        // You might want to handle this error more gracefully, such as logging it
    }
}
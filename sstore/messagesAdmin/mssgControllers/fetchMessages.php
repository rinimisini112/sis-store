<?php 
include "../../public/Classes/MessageHandler.php";

$messages = new MessageHandler(new Dbh);
$readMessages = $messages->readMessage();
foreach ($readMessages as $index => $row) {
    $username = $row['username'];
    $email = $row['user_email']; 
    $message = $row['user_message']; 
    $messageId = $row['message_id'];

    $bgColorClass = ($index % 2 == 0) ? 'bg-slate-400' : 'bg-gray-300';

    echo '<tr class="h-auto w-full ' . $bgColorClass . '">';
    echo '<td class="border-4 py-2 px-3 text-xl font-bold border-sis-white text-center">' . $username . '</td>';
    echo '<td class="border-4 py-2 px-3 text-xl font-bold border-sis-white text-center">' . $email . '</td>';
    echo '<td class="border-4 py-2 px-3 text-xl font-bold border-sis-white text-center">' . $message . '</td>'; // Display the product count
    echo '<td class="border-4 border-sis-white text-center underline underline-offset-1 text-2xl font-bold bg-red-600 text-sis-white hover:bg-red-500 duration-200 "><a href="delete_messages.php?mid=' . $messageId . '">Fshije</td>';
    echo '</tr>';
}
?>
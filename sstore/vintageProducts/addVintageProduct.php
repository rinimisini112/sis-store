<?php
    session_start();
    include "../admInc/categoriesHead.php";
    include "../../public/Classes/Dbh.php";
    include "../../public/Classes/ProductHandler.php";
    include "../auth/Auth.php";

    $auth = new Auth(new Dbh());

    // Check if the authentication token is present
    if (!$auth->isAuthTokenValid()) {
        header("Location: adm.php");
        exit();
    }
    ?>

    <body class="bg-gray-400">

        <main class="w-[80%] h-auto bg-sis-white  flex  mb-6">
            <div class="w-full">
            <?php
            include '../admInc/vintageNavbar.php';
            ?>
            <div class=" w-full text-center bg-gray-400 flex items-center justify-between pl-6 text-sis-white text-2xl font-bold">
                <p>Admin Paneli i SIS store - Shto Produkt</p>
                <a href="vintage_panel.php" class="h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl text-sis-grey font-bold">Shko mbrapa</a>
            </div>
            <form action="vntgCrud/vintageProductAdd.php" id="productForm" method="post" enctype="multipart/form-data" class="w-full flex bg-gray-300">
                <div class="w-1/4 flex flex-col gap-y-6 border-r-2 border-r-sis-grey">
                    <p class="bg-gray-600 text-sis-white text-center text-2xl font-bold">Fotot</p>
                    <label for="main_image" class="px-4 pt-2">Fotoja Kryesore kjo foto del ne collections kur shfaqen krejt produktet.</label>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="main_image" id="main_image">
                    <label for="image1" class="px-4 pt-2">Fotoja 2 kjo foto del si fotoja e dyt kur kalojm me maus ne produkt te collections.</label>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="image1" id="image1">
                    <label for="image2" class="px-4 pt-2">Fotoja 3 nuk osht obligative mu qit</label>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="image2" id="image2">
                    <label for="image3" class="px-4 pt-2">Fotoja 4 nuk osht obligative mu qit</label>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="image3" id="image3">
                    <label for="image4" class="px-4 pt-2">Fotoja 5 nuk osht obligative mu qit</label>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="image4" id="image4">
                    <label for="image5" class="px-4 pt-2">Fotoja 6 nuk osht obligative mu qit</label>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="image5" id="image5">
                    <label for="image6" class="px-4 pt-2">Fotoja 7 nuk osht obligative mu qit</label>
                    <input type="file" class="px-4  pt-2" name="image6" id="image6">
                </div>
                <div class="w-3/4 ml-auto flex flex-col gap-y-8">
                    <p class="bg-gray-600 text-sis-white text-center text-2xl font-bold">Te dhenat e produktit qe ka mu fut ne website</p>
                    <?php
                    if (isset($_SESSION['error_mssg'])) {
                        echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                    border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                    fixed right-8 bottom-8 w-1/2 h-[100px] z-50'>" . $_SESSION['error_mssg'] . "</p>";
                        unset($_SESSION['error_mssg']);
                    }
                    ?>
                    <div class="flex flex-col w-[60%] mx-auto gap-y-4">
                        <label for="productName">Emri i Produktit</label>
                        <input type="text" class="py-3 text-xl pl-3" name="productName" id="productName">
                        <label for="productDesc">Pershkrimi i Produktit</label>
                        <input type="text" class="py-3 text-xl pl-3" name="productDesc" id="productDesc">
                        <label for="productPrice">Qmimi</label>
                        <input type="text" class="py-3 text-xl pl-3" name="productPrice" id="productPrice">
                        <label for="">A eshte in stock</label>
                        <div>
                            <label for="yes">Po</label>
                            <input type="checkbox" name="yes" id="yes" class="checkbox" data-checkbox-group="checkGroup">
                            <label for="no">Jo</label>
                            <input type="checkbox" name="no" id="no" class="checkbox" data-checkbox-group="checkGroup">
                        </div>
                        <?php
                        include "vntgCrud/vntgCategoryDrop.php";
                        include 'vntgCrud/vintageSizeList.php';
                        ?>
                        <input type="submit" name="shtoProduktin" id="shtoProduktin" value='Shto Produktin' class="hover:bg-vintage-black hover:text-vintage-white duration-150 border mx-auto border-sis-grey px-8 w-1/2 py-2 cursor-pointer">

                    </div>
                </div>
            </form>
            </div>
        <?php 
        include "../admInc/vintageMenu.php";
        ?>   
        </main>
        <div class="w-[90%] mx-auto flex flex-wrap gap-4">
            <div id="mainImg" class="w-[31%] h-[400px]"></div>
            <div id="img1" class="w-[31%] h-[400px]"></div>
            <div id="img2" class="w-[31%] h-[400px]"></div>
            <div id="img3" class="w-[31%] h-[400px]"></div>
            <div id="img4" class="w-[31%] h-[400px]"></div>
            <div id="img5" class="w-[31%] h-[400px]"></div>
            <div id="img6" class="w-[31%] h-[400px]"></div>
        </div>

        <script src="../../dist/javascript/admVal.js"></script>
        <script>   
        </script>
    </body>

    </html>
<?php
session_start();
include "../admInc/categoriesHead.php";
include "../../public/Classes/Dbh.php";
include "../auth/Auth.php";
include "../auth/VintageMultiHandler.php";
include '../../public/Classes/ProductHandler.php';

$auth = new Auth(new Dbh());

// Check if the authentication token is present
if (!$auth->isAuthTokenValid()) {
    header("Location: ../adm.php");
    exit();
}
if (isset($_GET['pid'])) {
    $productID = $_GET['pid'];
    $editHandler = new VintageMultiHandler(new Dbh());

    $productData =  $editHandler->getProductData($productID);
}
?>

<body class="bg-sis-white">
    <main class="w-[80%] h-auto flex gap-4 mb-6">
        <div class="w-full">  
        <?php
        include '../admInc/vintageNavbar.php';
        ?>
        <div class=" w-full text-center bg-gray-400 flex items-center justify-between pl-6 text-sis-white text-2xl font-bold">
            <p>Admin Paneli i SIS store - Nrysho te dhenat e Produktit</p>
            <a href="vintage_panel.php" class="h-full px-8 py-4 inline-block bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600 text-2xl text-sis-grey font-bold">Shko mbrapa</a>
        </div>
        <form action="vntgCrud/vintageProductEdit.php" id="editProductForm" method="post" enctype="multipart/form-data" class="w-full bg-gray-300 flex">
            <input type="hidden" name="productID" value="<?= $productID ?>">
            <div class="w-[30%] flex flex-col gap-y-6 border-r-2 border-r-sis-grey">
                <p class="bg-gray-600 text-sis-white text-center text-2xl font-bold">Fotot aktuale te produkteve</p>
                <label for="main_image" class="px-4 pt-2">Fotoja Kryesore kjo foto del ne collections kur shfaqen krejt produktet.</label>
                <div id="mainImg" class="w-[95%] px-4 h-[300px]">
                    <img src="../../resources/images/<?= $productData['vmain_image'] ?>" alt="" class="h-full w-full object-cover">
                </div>
                <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="main_image" id="vmain_image" value="<?= $productData['vmain_image'] ?>">
                <?php
                $imageFields = ['vimage1', 'vimage2', 'vimage3', 'vimage4', 'vimage5', 'vimage6'];

                foreach ($imageFields as $field) :
                ?>
                    <label for="<?= $field ?>" class="px-4 pt-2">Fotoja <?= substr($field, -1) + 0 ?> nuk osht obligative mu qit</label>
                    <div id="<?= 'img' . substr($field, -1) ?>" class="w-[95%] h-[300px]  px-4">
                        <?php
                        if (isset($productData[$field])) :
                        ?>
                            <img src="../../resources/images/<?= $productData[$field] ?? '' ?>" alt="" class="h-full w-full object-cover">
                        <?php
                        endif;
                        ?>
                    </div>
                    <p class="flex items-center justify-center">Fshije foton <?= $field ?> <input type="checkbox" name="delete_image1"></p>
                    <input type="file" class="px-4 border-b-2 border-sis-grey pb-4 pt-2" name="<?= $field ?>" id="<?= $field ?>">
                <?php
                endforeach;
                ?>
            </div>
            <div class="w-[70%] flex flex-col gap-y-8">
                <p class="bg-gray-600 text-sis-white text-center flex items-center text-2xl font-bold justify-between pl-2">Shikimi, ndryshimi dhe fshirja
                    <a class="bg-gradient-to-r from-red-500 via-red-600 to-red-700 px-4 flex items-center justify-center hover:bg-red-600 border border-red-700 py-2 duration-150 text-sis-grey h-full hover:text-sis-white" href="deleteVintage.php?pid=<?= $productID ?>">Fshij Produktin</a>
                </p>
                <?php
                if (isset($_SESSION['error_mssg'])) {
                    echo "<p id='messageNotif' class='bg-red-600 bg-opacity-70 flex items-center justify-center
                border-red-600 border rounded-xl text-sis-white text-center text-2xl font-bold 
                fixed left-8 bottom-8 w-1/2 h-[100px] z-50'>" . $_SESSION['error_mssg'] . "</p>";
                    unset($_SESSION['error_mssg']);
                } else if (isset($_SESSION['success_mssg'])) {
                    echo "<p id='messageNotif' class='bg-green-600 bg-opacity-70 flex items-center justify-center
                    border-green-600 border rounded-xl text-sis-white text-center text-2xl font-bold fixed
                     left-8 bottom-8 w-1/2 h-[100px] z-50'>" . $_SESSION['success_mssg'] . "</p>";
                    unset($_SESSION['success_mssg']);
                }
                ?>
                <div class="flex flex-col w-[60%] mx-auto gap-y-4">
                    <label for="productName">Emri i Produktit</label>
                    <input type="text" class="py-3 text-xl pl-3" name="productName" id="productName" value="<?php echo $productData['v_name'] ?? ''; ?>">
                    <label for="productDesc">Pershkrimi i Produktit</label>
                    <input type="text" class="py-3 text-xl pl-3" name="productDesc" id="productDesc" value="<?php echo $productData['v_description'] ?? ''; ?>">
                    <label for="productPrice">Qmimi</label>
                    <input type="text" class="py-3 text-xl pl-3" name="productPrice" id="productPrice" value="<?php echo $productData['v_price'] ?? ''; ?>">
                    <label for="" class="text-xl">A eshte in stock</label>
                    <div>
                        <div>
                            <label class="text-xl" for="yes">Po</label>
                            <input class="w-5 h-5 accent-black" type="checkbox" name="yes" id="yes" <?php echo ($productData['in_stock'] == 1) ? 'checked' : ''; ?>>
                            <label class="text-xl" for="no">Jo</label>
                            <input class="w-5 h-5 accent-black" type="checkbox" name="no" id="no" <?php echo ($productData['in_stock'] == 0) ? 'checked' : ''; ?>>
                        </div>
                    </div>
                    <?php
                    include "vntgCrud/vintageProductDrpd.php";
                    include 'vntgCrud/vintageSelectedSizes.php';
                    ?>
                    <input type="submit" name="editProduct" id="editProduct" value='Ndrysho Te dhenat' class=" hover:bg-blue-700 bg-blue-300 hover:text-sis-white duration-100 text-xl font-bold border mx-auto border-blue-500 px-8 w-1/2 py-2 cursor-pointer">

                </div>
            </div>
        </form>
        </div>
     <?php 
     include '../admInc/vintageMenu.php';
     ?>
    </main>
    <script>
        
const vblogPictureInput = document.getElementById('vmain_image');
const vimage1Input = document.getElementById('vimage1');
const vimage2Input = document.getElementById('vimage2');
const vimage3Input = document.getElementById('vimage3');
const vimage4Input = document.getElementById('vimage4');
const vimage5Input = document.getElementById('vimage5');
const vimage6Input = document.getElementById('vimage6');
const vimagePreview = document.getElementById('mainImg');
const vimg1 = document.getElementById('img1');
const vimg2 = document.getElementById('img2');
const vimg3 = document.getElementById('img3');
const vimg4 = document.getElementById('img4');
const vimg5 = document.getElementById('img5');
const vimg6 = document.getElementById('img6');

function showImg(input, previewContainer) {
    input.addEventListener('change', function(event) {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                const image = document.createElement('img');
                image.src = e.target.result;
                previewContainer.innerHTML = '';
                image.className = 'w-full h-full object-cover';
                previewContainer.appendChild(image);
            };
            reader.readAsDataURL(file);
        } else {
            previewContainer.innerHTML = '';
        }
    });
}
showImg(vblogPictureInput, vimagePreview);
showImg(vimage1Input, vimg1);
showImg(vimage2Input, vimg2);
showImg(vimage3Input, vimg3);
showImg(vimage4Input, vimg4);
showImg(vimage5Input, vimg5);
showImg(vimage6Input, vimg6);
        $(document).ready(function() {
    // Wait for the document to be ready
    setTimeout(function() {
        $('#messageNotif').fadeOut('slow'); // Hide the element with a fade-out effect
    }, 5000); // 5000 milliseconds = 5 seconds
});
$(document).ready(function() {
    $.validator.addMethod("alphabetsOnly", function(value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");

    $("#editProductForm").validate({
        rules: {
            productName: {
                required: true,
            },
            productDesc: {
                required: true,
            },
            productPrice: {
                required: true,
            },
        },
        messages: {
            productName: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            productDesc: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },
            productPrice: {
                required: "Fusha nuk duhet te jet e zbrazt",
            },

        },
        errorPlacement: function(error, element) {
            // Place the error message below the input element
            error.insertAfter(element);

            // Add a class to the input when there is an error
            element.addClass("error_effect");
        },
        success: function(label, element) {
            // Remove the error class when the input is valid
            $(element).removeClass("error_effect");
        }
    });
});

    </script>
</body>

</html>
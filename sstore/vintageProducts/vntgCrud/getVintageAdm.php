<?php
include "../auth/VintageOperations.php";

$productsOperation = new VintageOperations(new Dbh());
$productsTable = $productsOperation->fetchProductsAdm();

foreach ($productsTable as $index => $row) {
    $mainImage = $row['vmain_image'];
    $productName = $row['v_name'];
    $productDescription = $row['v_description'];
    $price = $row['v_price'];
    $categoryName = $row['category_name'];
    $productID = $row['vintage_id'];

    $bgColorClass = ($index % 2 == 0) ? 'bg-slate-400' : 'bg-gray-300';

    echo '<tr class="h-auto w-full ' . $bgColorClass . '">';
    echo '<td class="border-4 border-sis-white w-[20%]"><img src="../../resources/images/' . $mainImage . '" alt="" class="w-[50%] mx-auto"></td>';
    echo '<td class="border-4 text-xl font-bold border-sis-white text-center">' . $productName . '</td>';
    echo '<td class="border-4 text-xl font-bold border-sis-white pl-4">' . $productDescription . '</td>';
    echo '<td class="border-4 text-xl font-bold border-sis-white text-center">' . $price . '€</td>';
    echo '<td class="border-4 text-xl font-bold border-sis-white text-center">' . $categoryName . '</td>';
    echo '<td class="border-4 border-sis-white text-center underline underline-offset-1 text-2xl font-bold bg-sis-grey text-sis-white hover:bg-green-500 duration-200 "><a href="vintageEdit.php?pid=' . $productID . '">Edit</td>';
    echo '</tr>';
}
?>
<?php
include "../../public/Classes/CategoryHandler.php";

$categoryHandler = new CategoryHandler(new Dbh);
$categories = $categoryHandler->fetchCategories();
$productCategoryID = $productData['category_id'] ?? ''; // Assuming $productData contains the product data

?>
<p class="text-xl">Zgjedh Kategorin e produktit</p>
<select class="py-2 text-xl font-bold w-1/2 pl-4" name="category" id="category">
    <option value="#">Zgjidh kategorin</option>
    
    <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category['category_id']; ?>" <?php echo ($productCategoryID == $category['category_id']) ? 'selected' : ''; ?>>
            <?php echo $category['category_name']; ?>
        </option>
    <?php endforeach; ?>
</select>
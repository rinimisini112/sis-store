<?php
include "../../../public/Classes/Dbh.php";
session_start();

if ($_SERVER["REQUEST_METHOD"] === "POST") {
try {
    include "../../auth/VintageMultiUpdater.php";
    $productUpdater = new VintageMultiUpdater(new Dbh);

    $productID = isset($_POST['productID']) ? $_POST['productID'] : '';
    $productName = isset($_POST['productName']) ? $_POST['productName'] : '';
    $productDesc = isset($_POST['productDesc']) ? $_POST['productDesc'] : '';
    $productPrice = isset($_POST['productPrice']) ? $_POST['productPrice'] : '';
    $inStock = isset($_POST['yes']) ? 1 : 0;
    $categoryID = isset($_POST['category']) ? $_POST['category'] : '';

    $editHandler = new VintageMultiHandler(new Dbh());
    $productData = $editHandler->getProductData($productID);

    $mainImage = isset($_FILES['main_image']['name']) && $_FILES['main_image']['size'] > 0 ? $_FILES['main_image']['name'] : null;
    $vimage1 = isset($_FILES['vimage1']['name']) && $_FILES['vimage1']['size'] > 0 ? $_FILES['vimage1']['name'] : $product['vimage1'];
    $vimage2 = isset($_FILES['vimage2']['name']) && $_FILES['vimage2']['size'] > 0 ? $_FILES['vimage2']['name'] : $product['vimage2'];
    $vimage3 = isset($_FILES['vimage3']['name']) && $_FILES['vimage3']['size'] > 0 ? $_FILES['vimage3']['name'] : $product['vimage3'];
    $vimage4 = isset($_FILES['vimage4']['name']) && $_FILES['vimage4']['size'] > 0 ? $_FILES['vimage4']['name'] : $product['vimage4'];
    $vimage5 = isset($_FILES['vimage5']['name']) && $_FILES['vimage5']['size'] > 0 ? $_FILES['vimage5']['name'] : $product['vimage5'];
    $vimage6 = isset($_FILES['vimage6']['name']) && $_FILES['vimage6']['size'] > 0 ? $_FILES['vimage6']['name'] : $product['vimage6'];

    function moveUploadedFile($tempFilePath, $destinationPath)
    {
        if (move_uploaded_file($tempFilePath, $destinationPath)) {
            return true;
        } else {
            throw new Exception('Failed to move uploaded file');
        }
    }
   // Move main image
$destinationMainImage = "../../../resources/images/" . $_FILES['main_image']['name'];
if (!empty($_FILES['main_image']['tmp_name'])) {
    $tempMainImage = $_FILES['main_image']['tmp_name'];
    moveUploadedFile($tempMainImage, $destinationMainImage);
}

// Move additional images
$additionalImages = ['vimage1', 'vimage2', 'vimage3', 'vimage4', 'vimage5', 'vimage6'];

foreach ($additionalImages as $imageInputName) {
    if (!empty($_FILES[$imageInputName]['tmp_name'])) {
        $tempImage = $_FILES[$imageInputName]['tmp_name'];
        $destinationImage = "../../../resources/images/" . $_FILES[$imageInputName]['name'];
        moveUploadedFile($tempImage, $destinationImage);
    }
}

    if ($_FILES['main_image']['error'] == 4) {
        $mainImage = $productData['vmain_image']; // Use the existing image if no new file is uploaded
    } else {
        $mainImage = $_FILES['main_image']['name'];
    }

    $imageFields = ['vimage1', 'vimage2', 'vimage3', 'vimage4', 'vimage5', 'vimage6'];

    foreach ($imageFields as $field) {
    // Check if the delete checkbox is checked
    $deleteCheckbox = 'delete_' . $field;
    $deleteImage = isset($_POST[$deleteCheckbox]) && $_POST[$deleteCheckbox] === 'on';

    if ($_FILES[$field]['error'] == 4 && !$deleteImage) {
        // No new file uploaded, use the existing image if delete checkbox is not checked
        $$field = $productData[$field];
    } elseif ($_FILES[$field]['error'] == 4 && $deleteImage) {
        // No new file uploaded, and delete checkbox is checked, set the image field to null
        $$field = null;
    } else {
        // New file uploaded
        $$field = $_FILES[$field]['name'];
    }
}

    $selectedSizes = [];
    foreach ($_POST as $key => $value) {
        if (strpos($key, 'size_') === 0 && $value == 'on') {
            $sizeID = substr($key, 5);
            $selectedSizes[] = $sizeID;
        }
    }
    if(empty($selectedSizes)) {
        throw new Exception('Ju lutem mos i leni te zbrazta madhsit per produkt');
    }


        $updateResult = $productUpdater->updateProductAndRelatedData(
            $productID,
            $productName,
            $productDesc,
            $productPrice,
            $inStock,
            $categoryID,
            $mainImage,
            $vimage1,
            $vimage2,
            $vimage3,
            $vimage4,
            $vimage5,
            $vimage6,
            $selectedSizes
        );

        if ($updateResult) {
            $_SESSION['success_mssg'] = 'Produkti u ndryshua me sukses';
            header("Location:../vintageEdit.php?pid=" . $productID);
            exit();
        } else {
            throw new Exception('Produkti nuk mund te ndryshohet, provo prap');
            exit();
        }
    } catch (Exception $e) {
        $_SESSION['error_mssg'] = $e->getMessage();
        header("Location:../vintageEdit.php?pid=" . $productID);

        // You might want to handle this error more gracefully, such as logging it
    }
}

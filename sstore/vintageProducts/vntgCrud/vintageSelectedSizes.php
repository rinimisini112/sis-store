<?php 
// Assuming you have the product ID from the URL
$productID = $_GET['pid'];

// Fetch all sizes for the product
$sizesByType = $editHandler->fetchSizesByType('all');
$letterSizes = $sizesByType['letterSizes'];
$numberSizes = $sizesByType['numberSizes'];
// Fetch selected sizes for the product
$selectedSizes = $editHandler->getSelectedSizesForProduct($productID);
?>

<!-- HTML for letter sizes -->
<div>
    <p class="text-xl font-bold">Zgjidhi nese produkti i ka kto numra ne Etikete</p>
    <?php foreach ($letterSizes as $size): ?>
        <label class="text-2xl" for="<?php echo $size['SizeName']; ?>">
            <?php echo $size['SizeName']; ?>
        </label>
        <input type="checkbox" class="accent-black w-5 h-5 pr-2" name="size_<?php echo $size['SizeID']; ?>" id="<?php echo $size['SizeName']; ?>" <?php echo (in_array($size, $selectedSizes)) ? 'checked' : ''; ?>>
    <?php endforeach; ?>
</div>

<!-- HTML for number sizes -->
<div>
    <p class="text-xl font-bold">Zgjidhi nese produkti i ka kto numra ne Etikete</p>
    <?php foreach ($numberSizes as $size): ?>
        <label class="text-2xl" for="<?php echo $size['SizeName']; ?>">
            <?php echo $size['SizeName']; ?>
        </label>
        <input type="checkbox" class="accent-black w-5 h-5 pr-2" name="size_<?php echo $size['SizeID']; ?>" id="<?php echo $size['SizeName']; ?>" <?php echo (in_array($size, $selectedSizes)) ? 'checked' : ''; ?>>
    <?php endforeach; ?>
</div>
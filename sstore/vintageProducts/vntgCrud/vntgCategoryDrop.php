<?php
include "../../public/Classes/CategoryHandler.php";

$categoryHandler = new CategoryHandler(new Dbh);
$categories = $categoryHandler->fetchCategories();
?>

<select name="category" id="category">
    <option value="0">Zgjidh kategorin</option>
    
    <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category['category_id']; ?>">
            <?php echo $category['category_name']; ?>
        </option>
    <?php endforeach; ?>
</select>
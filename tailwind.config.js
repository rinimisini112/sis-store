/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js,php}"],
    theme: {
      screens: {
        'sm': '640px',
        'md': '768px',
        'lg': '1034px',  // Change this value to your desired large breakpoint
        'xl': '1280px',
      },
    extend: {
      colors: {
        'sis-lemon': '#D5DFE5',
        'sis-yellow': '#C9B1BD',
        'sis-sandy': '#B49594',
        'sis-violet': '#37123C',
        'sis-blue': '#051923',
        'vintage-black': '#212121',
        'vintage-white': '#d1c8b6',
        'sis-grey': '#1e1e1e',
        'sis-white': '#E3E3E3ff',
      },
      // Add your custom classes here
      // For example, let's add a class called 'custom-bg' with a background color
      backgroundColor: {
        'sis-grey': '#1e1e1e',
        'sis-white': '#E3E3E3ff'
      },
      textColor: {
        'sis-grey': '#1e1e1e',
        'sis-white': '#E3E3E3ff'
      },
      shadow : {
        'sis-grey' : '#1e1e1e',
      },

      dropShadow: {
        'lg-white': '0 35px 35px rgba(255, 255, 255, 0.25)',
      },
      borderColor: {
        'sis-grey': '#1e1e1e', // Replace with your custom color code
      },
      // You can add more custom classes as needed
    },
  },
  plugins: [],
}

